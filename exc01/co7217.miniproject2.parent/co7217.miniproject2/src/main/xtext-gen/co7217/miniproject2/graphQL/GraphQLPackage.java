/**
 * generated by Xtext 2.18.0.M3
 */
package co7217.miniproject2.graphQL;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see co7217.miniproject2.graphQL.GraphQLFactory
 * @model kind="package"
 * @generated
 */
public interface GraphQLPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "graphQL";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.miniproject2.co7217/GraphQL";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "graphQL";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  GraphQLPackage eINSTANCE = co7217.miniproject2.graphQL.impl.GraphQLPackageImpl.init();

  /**
   * The meta object id for the '{@link co7217.miniproject2.graphQL.impl.GraphQLImpl <em>Graph QL</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co7217.miniproject2.graphQL.impl.GraphQLImpl
   * @see co7217.miniproject2.graphQL.impl.GraphQLPackageImpl#getGraphQL()
   * @generated
   */
  int GRAPH_QL = 0;

  /**
   * The feature id for the '<em><b>Queries</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GRAPH_QL__QUERIES = 0;

  /**
   * The number of structural features of the '<em>Graph QL</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GRAPH_QL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link co7217.miniproject2.graphQL.impl.QueryImpl <em>Query</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co7217.miniproject2.graphQL.impl.QueryImpl
   * @see co7217.miniproject2.graphQL.impl.GraphQLPackageImpl#getQuery()
   * @generated
   */
  int QUERY = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY__NAME = 0;

  /**
   * The number of structural features of the '<em>Query</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link co7217.miniproject2.graphQL.impl.DataTypeImpl <em>Data Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co7217.miniproject2.graphQL.impl.DataTypeImpl
   * @see co7217.miniproject2.graphQL.impl.GraphQLPackageImpl#getDataType()
   * @generated
   */
  int DATA_TYPE = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_TYPE__NAME = QUERY__NAME;

  /**
   * The number of structural features of the '<em>Data Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_TYPE_FEATURE_COUNT = QUERY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link co7217.miniproject2.graphQL.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co7217.miniproject2.graphQL.impl.TypeImpl
   * @see co7217.miniproject2.graphQL.impl.GraphQLPackageImpl#getType()
   * @generated
   */
  int TYPE = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__NAME = QUERY__NAME;

  /**
   * The feature id for the '<em><b>Query</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__QUERY = QUERY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = QUERY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co7217.miniproject2.graphQL.impl.QueryNodeImpl <em>Query Node</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co7217.miniproject2.graphQL.impl.QueryNodeImpl
   * @see co7217.miniproject2.graphQL.impl.GraphQLPackageImpl#getQueryNode()
   * @generated
   */
  int QUERY_NODE = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_NODE__NAME = 0;

  /**
   * The feature id for the '<em><b>Query Node</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_NODE__QUERY_NODE = 1;

  /**
   * The feature id for the '<em><b>Next Node</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_NODE__NEXT_NODE = 2;

  /**
   * The number of structural features of the '<em>Query Node</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_NODE_FEATURE_COUNT = 3;


  /**
   * Returns the meta object for class '{@link co7217.miniproject2.graphQL.GraphQL <em>Graph QL</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Graph QL</em>'.
   * @see co7217.miniproject2.graphQL.GraphQL
   * @generated
   */
  EClass getGraphQL();

  /**
   * Returns the meta object for the containment reference list '{@link co7217.miniproject2.graphQL.GraphQL#getQueries <em>Queries</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Queries</em>'.
   * @see co7217.miniproject2.graphQL.GraphQL#getQueries()
   * @see #getGraphQL()
   * @generated
   */
  EReference getGraphQL_Queries();

  /**
   * Returns the meta object for class '{@link co7217.miniproject2.graphQL.Query <em>Query</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Query</em>'.
   * @see co7217.miniproject2.graphQL.Query
   * @generated
   */
  EClass getQuery();

  /**
   * Returns the meta object for the attribute '{@link co7217.miniproject2.graphQL.Query#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see co7217.miniproject2.graphQL.Query#getName()
   * @see #getQuery()
   * @generated
   */
  EAttribute getQuery_Name();

  /**
   * Returns the meta object for class '{@link co7217.miniproject2.graphQL.DataType <em>Data Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Data Type</em>'.
   * @see co7217.miniproject2.graphQL.DataType
   * @generated
   */
  EClass getDataType();

  /**
   * Returns the meta object for class '{@link co7217.miniproject2.graphQL.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see co7217.miniproject2.graphQL.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for the containment reference list '{@link co7217.miniproject2.graphQL.Type#getQuery <em>Query</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Query</em>'.
   * @see co7217.miniproject2.graphQL.Type#getQuery()
   * @see #getType()
   * @generated
   */
  EReference getType_Query();

  /**
   * Returns the meta object for class '{@link co7217.miniproject2.graphQL.QueryNode <em>Query Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Query Node</em>'.
   * @see co7217.miniproject2.graphQL.QueryNode
   * @generated
   */
  EClass getQueryNode();

  /**
   * Returns the meta object for the attribute '{@link co7217.miniproject2.graphQL.QueryNode#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see co7217.miniproject2.graphQL.QueryNode#getName()
   * @see #getQueryNode()
   * @generated
   */
  EAttribute getQueryNode_Name();

  /**
   * Returns the meta object for the attribute list '{@link co7217.miniproject2.graphQL.QueryNode#getQueryNode <em>Query Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Query Node</em>'.
   * @see co7217.miniproject2.graphQL.QueryNode#getQueryNode()
   * @see #getQueryNode()
   * @generated
   */
  EAttribute getQueryNode_QueryNode();

  /**
   * Returns the meta object for the containment reference list '{@link co7217.miniproject2.graphQL.QueryNode#getNextNode <em>Next Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Next Node</em>'.
   * @see co7217.miniproject2.graphQL.QueryNode#getNextNode()
   * @see #getQueryNode()
   * @generated
   */
  EReference getQueryNode_NextNode();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  GraphQLFactory getGraphQLFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link co7217.miniproject2.graphQL.impl.GraphQLImpl <em>Graph QL</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co7217.miniproject2.graphQL.impl.GraphQLImpl
     * @see co7217.miniproject2.graphQL.impl.GraphQLPackageImpl#getGraphQL()
     * @generated
     */
    EClass GRAPH_QL = eINSTANCE.getGraphQL();

    /**
     * The meta object literal for the '<em><b>Queries</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GRAPH_QL__QUERIES = eINSTANCE.getGraphQL_Queries();

    /**
     * The meta object literal for the '{@link co7217.miniproject2.graphQL.impl.QueryImpl <em>Query</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co7217.miniproject2.graphQL.impl.QueryImpl
     * @see co7217.miniproject2.graphQL.impl.GraphQLPackageImpl#getQuery()
     * @generated
     */
    EClass QUERY = eINSTANCE.getQuery();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QUERY__NAME = eINSTANCE.getQuery_Name();

    /**
     * The meta object literal for the '{@link co7217.miniproject2.graphQL.impl.DataTypeImpl <em>Data Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co7217.miniproject2.graphQL.impl.DataTypeImpl
     * @see co7217.miniproject2.graphQL.impl.GraphQLPackageImpl#getDataType()
     * @generated
     */
    EClass DATA_TYPE = eINSTANCE.getDataType();

    /**
     * The meta object literal for the '{@link co7217.miniproject2.graphQL.impl.TypeImpl <em>Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co7217.miniproject2.graphQL.impl.TypeImpl
     * @see co7217.miniproject2.graphQL.impl.GraphQLPackageImpl#getType()
     * @generated
     */
    EClass TYPE = eINSTANCE.getType();

    /**
     * The meta object literal for the '<em><b>Query</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__QUERY = eINSTANCE.getType_Query();

    /**
     * The meta object literal for the '{@link co7217.miniproject2.graphQL.impl.QueryNodeImpl <em>Query Node</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co7217.miniproject2.graphQL.impl.QueryNodeImpl
     * @see co7217.miniproject2.graphQL.impl.GraphQLPackageImpl#getQueryNode()
     * @generated
     */
    EClass QUERY_NODE = eINSTANCE.getQueryNode();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QUERY_NODE__NAME = eINSTANCE.getQueryNode_Name();

    /**
     * The meta object literal for the '<em><b>Query Node</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QUERY_NODE__QUERY_NODE = eINSTANCE.getQueryNode_QueryNode();

    /**
     * The meta object literal for the '<em><b>Next Node</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUERY_NODE__NEXT_NODE = eINSTANCE.getQueryNode_NextNode();

  }

} //GraphQLPackage
