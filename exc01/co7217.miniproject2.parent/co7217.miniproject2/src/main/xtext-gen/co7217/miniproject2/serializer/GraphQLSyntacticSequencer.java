/*
 * generated by Xtext 2.18.0.M3
 */
package co7217.miniproject2.serializer;

import co7217.miniproject2.services.GraphQLGrammarAccess;
import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;

@SuppressWarnings("all")
public class GraphQLSyntacticSequencer extends AbstractSyntacticSequencer {

	protected GraphQLGrammarAccess grammarAccess;
	protected AbstractElementAlias match_QueryNode_ExclamationMarkKeyword_2_2_q;
	protected AbstractElementAlias match_QueryNode_ExclamationMarkKeyword_2_4_q;
	protected AbstractElementAlias match_QueryNode_LeftSquareBracketKeyword_2_0_q;
	protected AbstractElementAlias match_QueryNode_RightSquareBracketKeyword_2_3_q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (GraphQLGrammarAccess) access;
		match_QueryNode_ExclamationMarkKeyword_2_2_q = new TokenAlias(false, true, grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_2());
		match_QueryNode_ExclamationMarkKeyword_2_4_q = new TokenAlias(false, true, grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_4());
		match_QueryNode_LeftSquareBracketKeyword_2_0_q = new TokenAlias(false, true, grammarAccess.getQueryNodeAccess().getLeftSquareBracketKeyword_2_0());
		match_QueryNode_RightSquareBracketKeyword_2_3_q = new TokenAlias(false, true, grammarAccess.getQueryNodeAccess().getRightSquareBracketKeyword_2_3());
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if (match_QueryNode_ExclamationMarkKeyword_2_2_q.equals(syntax))
				emit_QueryNode_ExclamationMarkKeyword_2_2_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_QueryNode_ExclamationMarkKeyword_2_4_q.equals(syntax))
				emit_QueryNode_ExclamationMarkKeyword_2_4_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_QueryNode_LeftSquareBracketKeyword_2_0_q.equals(syntax))
				emit_QueryNode_LeftSquareBracketKeyword_2_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_QueryNode_RightSquareBracketKeyword_2_3_q.equals(syntax))
				emit_QueryNode_RightSquareBracketKeyword_2_3_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Ambiguous syntax:
	 *     '!'?
	 *
	 * This ambiguous syntax occurs at:
	 *     QueryNode+=DataID (ambiguity) ']'? '!'? '{' nextNode+=QueryNode
	 *     QueryNode+=DataID (ambiguity) ']'? '!'? (rule end)
	 *     QueryNode+=TypeID (ambiguity) ']'? '!'? '{' nextNode+=QueryNode
	 *     QueryNode+=TypeID (ambiguity) ']'? '!'? (rule end)
	 *     name=ID ':' '['? (ambiguity) ']'? '!'? '{' nextNode+=QueryNode
	 *     name=ID ':' '['? (ambiguity) ']'? '!'? (rule end)
	 */
	protected void emit_QueryNode_ExclamationMarkKeyword_2_2_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '!'?
	 *
	 * This ambiguous syntax occurs at:
	 *     QueryNode+=DataID '!'? ']'? (ambiguity) '{' nextNode+=QueryNode
	 *     QueryNode+=DataID '!'? ']'? (ambiguity) (rule end)
	 *     QueryNode+=TypeID '!'? ']'? (ambiguity) '{' nextNode+=QueryNode
	 *     QueryNode+=TypeID '!'? ']'? (ambiguity) (rule end)
	 *     name=ID ':' '['? '!'? ']'? (ambiguity) '{' nextNode+=QueryNode
	 *     name=ID ':' '['? '!'? ']'? (ambiguity) (rule end)
	 */
	protected void emit_QueryNode_ExclamationMarkKeyword_2_4_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '['?
	 *
	 * This ambiguous syntax occurs at:
	 *     name=ID ':' (ambiguity) '!'? ']'? '!'? '{' nextNode+=QueryNode
	 *     name=ID ':' (ambiguity) '!'? ']'? '!'? (rule end)
	 *     name=ID ':' (ambiguity) QueryNode+=DataID
	 *     name=ID ':' (ambiguity) QueryNode+=TypeID
	 */
	protected void emit_QueryNode_LeftSquareBracketKeyword_2_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ']'?
	 *
	 * This ambiguous syntax occurs at:
	 *     QueryNode+=DataID '!'? (ambiguity) '!'? '{' nextNode+=QueryNode
	 *     QueryNode+=DataID '!'? (ambiguity) '!'? (rule end)
	 *     QueryNode+=TypeID '!'? (ambiguity) '!'? '{' nextNode+=QueryNode
	 *     QueryNode+=TypeID '!'? (ambiguity) '!'? (rule end)
	 *     name=ID ':' '['? '!'? (ambiguity) '!'? '{' nextNode+=QueryNode
	 *     name=ID ':' '['? '!'? (ambiguity) '!'? (rule end)
	 */
	protected void emit_QueryNode_RightSquareBracketKeyword_2_3_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
