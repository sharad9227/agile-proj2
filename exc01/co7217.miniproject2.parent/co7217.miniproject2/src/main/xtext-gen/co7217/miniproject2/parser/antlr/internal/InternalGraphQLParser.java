package co7217.miniproject2.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import co7217.miniproject2.services.GraphQLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGraphQLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'scalar'", "'type'", "'{'", "'}'", "':'", "'['", "'!'", "']'", "'String'", "'Integer'", "'User'", "'Tweet'", "'Entity'", "'HashTag'", "'UserMention'", "'Url'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalGraphQLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGraphQLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGraphQLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGraphQL.g"; }



     	private GraphQLGrammarAccess grammarAccess;

        public InternalGraphQLParser(TokenStream input, GraphQLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "GraphQL";
       	}

       	@Override
       	protected GraphQLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleGraphQL"
    // InternalGraphQL.g:64:1: entryRuleGraphQL returns [EObject current=null] : iv_ruleGraphQL= ruleGraphQL EOF ;
    public final EObject entryRuleGraphQL() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGraphQL = null;


        try {
            // InternalGraphQL.g:64:48: (iv_ruleGraphQL= ruleGraphQL EOF )
            // InternalGraphQL.g:65:2: iv_ruleGraphQL= ruleGraphQL EOF
            {
             newCompositeNode(grammarAccess.getGraphQLRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGraphQL=ruleGraphQL();

            state._fsp--;

             current =iv_ruleGraphQL; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGraphQL"


    // $ANTLR start "ruleGraphQL"
    // InternalGraphQL.g:71:1: ruleGraphQL returns [EObject current=null] : ( (lv_queries_0_0= ruleQuery ) )* ;
    public final EObject ruleGraphQL() throws RecognitionException {
        EObject current = null;

        EObject lv_queries_0_0 = null;



        	enterRule();

        try {
            // InternalGraphQL.g:77:2: ( ( (lv_queries_0_0= ruleQuery ) )* )
            // InternalGraphQL.g:78:2: ( (lv_queries_0_0= ruleQuery ) )*
            {
            // InternalGraphQL.g:78:2: ( (lv_queries_0_0= ruleQuery ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=11 && LA1_0<=12)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalGraphQL.g:79:3: (lv_queries_0_0= ruleQuery )
            	    {
            	    // InternalGraphQL.g:79:3: (lv_queries_0_0= ruleQuery )
            	    // InternalGraphQL.g:80:4: lv_queries_0_0= ruleQuery
            	    {

            	    				newCompositeNode(grammarAccess.getGraphQLAccess().getQueriesQueryParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_queries_0_0=ruleQuery();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getGraphQLRule());
            	    				}
            	    				add(
            	    					current,
            	    					"queries",
            	    					lv_queries_0_0,
            	    					"co7217.miniproject2.GraphQL.Query");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGraphQL"


    // $ANTLR start "entryRuleQuery"
    // InternalGraphQL.g:100:1: entryRuleQuery returns [EObject current=null] : iv_ruleQuery= ruleQuery EOF ;
    public final EObject entryRuleQuery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuery = null;


        try {
            // InternalGraphQL.g:100:46: (iv_ruleQuery= ruleQuery EOF )
            // InternalGraphQL.g:101:2: iv_ruleQuery= ruleQuery EOF
            {
             newCompositeNode(grammarAccess.getQueryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuery=ruleQuery();

            state._fsp--;

             current =iv_ruleQuery; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuery"


    // $ANTLR start "ruleQuery"
    // InternalGraphQL.g:107:1: ruleQuery returns [EObject current=null] : (this_DataType_0= ruleDataType | this_Type_1= ruleType ) ;
    public final EObject ruleQuery() throws RecognitionException {
        EObject current = null;

        EObject this_DataType_0 = null;

        EObject this_Type_1 = null;



        	enterRule();

        try {
            // InternalGraphQL.g:113:2: ( (this_DataType_0= ruleDataType | this_Type_1= ruleType ) )
            // InternalGraphQL.g:114:2: (this_DataType_0= ruleDataType | this_Type_1= ruleType )
            {
            // InternalGraphQL.g:114:2: (this_DataType_0= ruleDataType | this_Type_1= ruleType )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalGraphQL.g:115:3: this_DataType_0= ruleDataType
                    {

                    			newCompositeNode(grammarAccess.getQueryAccess().getDataTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_DataType_0=ruleDataType();

                    state._fsp--;


                    			current = this_DataType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:124:3: this_Type_1= ruleType
                    {

                    			newCompositeNode(grammarAccess.getQueryAccess().getTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Type_1=ruleType();

                    state._fsp--;


                    			current = this_Type_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuery"


    // $ANTLR start "entryRuleDataType"
    // InternalGraphQL.g:136:1: entryRuleDataType returns [EObject current=null] : iv_ruleDataType= ruleDataType EOF ;
    public final EObject entryRuleDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataType = null;


        try {
            // InternalGraphQL.g:136:49: (iv_ruleDataType= ruleDataType EOF )
            // InternalGraphQL.g:137:2: iv_ruleDataType= ruleDataType EOF
            {
             newCompositeNode(grammarAccess.getDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataType=ruleDataType();

            state._fsp--;

             current =iv_ruleDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // InternalGraphQL.g:143:1: ruleDataType returns [EObject current=null] : (otherlv_0= 'scalar' ( (lv_name_1_0= ruleDataID ) ) ) ;
    public final EObject ruleDataType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalGraphQL.g:149:2: ( (otherlv_0= 'scalar' ( (lv_name_1_0= ruleDataID ) ) ) )
            // InternalGraphQL.g:150:2: (otherlv_0= 'scalar' ( (lv_name_1_0= ruleDataID ) ) )
            {
            // InternalGraphQL.g:150:2: (otherlv_0= 'scalar' ( (lv_name_1_0= ruleDataID ) ) )
            // InternalGraphQL.g:151:3: otherlv_0= 'scalar' ( (lv_name_1_0= ruleDataID ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getDataTypeAccess().getScalarKeyword_0());
            		
            // InternalGraphQL.g:155:3: ( (lv_name_1_0= ruleDataID ) )
            // InternalGraphQL.g:156:4: (lv_name_1_0= ruleDataID )
            {
            // InternalGraphQL.g:156:4: (lv_name_1_0= ruleDataID )
            // InternalGraphQL.g:157:5: lv_name_1_0= ruleDataID
            {

            					newCompositeNode(grammarAccess.getDataTypeAccess().getNameDataIDParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleDataID();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDataTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"co7217.miniproject2.GraphQL.DataID");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleType"
    // InternalGraphQL.g:178:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalGraphQL.g:178:45: (iv_ruleType= ruleType EOF )
            // InternalGraphQL.g:179:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalGraphQL.g:185:1: ruleType returns [EObject current=null] : (otherlv_0= 'type' ( (lv_name_1_0= ruleTypeID ) ) otherlv_2= '{' ( (lv_query_3_0= ruleQueryNode ) )+ otherlv_4= '}' ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_query_3_0 = null;



        	enterRule();

        try {
            // InternalGraphQL.g:191:2: ( (otherlv_0= 'type' ( (lv_name_1_0= ruleTypeID ) ) otherlv_2= '{' ( (lv_query_3_0= ruleQueryNode ) )+ otherlv_4= '}' ) )
            // InternalGraphQL.g:192:2: (otherlv_0= 'type' ( (lv_name_1_0= ruleTypeID ) ) otherlv_2= '{' ( (lv_query_3_0= ruleQueryNode ) )+ otherlv_4= '}' )
            {
            // InternalGraphQL.g:192:2: (otherlv_0= 'type' ( (lv_name_1_0= ruleTypeID ) ) otherlv_2= '{' ( (lv_query_3_0= ruleQueryNode ) )+ otherlv_4= '}' )
            // InternalGraphQL.g:193:3: otherlv_0= 'type' ( (lv_name_1_0= ruleTypeID ) ) otherlv_2= '{' ( (lv_query_3_0= ruleQueryNode ) )+ otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getTypeAccess().getTypeKeyword_0());
            		
            // InternalGraphQL.g:197:3: ( (lv_name_1_0= ruleTypeID ) )
            // InternalGraphQL.g:198:4: (lv_name_1_0= ruleTypeID )
            {
            // InternalGraphQL.g:198:4: (lv_name_1_0= ruleTypeID )
            // InternalGraphQL.g:199:5: lv_name_1_0= ruleTypeID
            {

            					newCompositeNode(grammarAccess.getTypeAccess().getNameTypeIDParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_1_0=ruleTypeID();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"co7217.miniproject2.GraphQL.TypeID");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getTypeAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalGraphQL.g:220:3: ( (lv_query_3_0= ruleQueryNode ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalGraphQL.g:221:4: (lv_query_3_0= ruleQueryNode )
            	    {
            	    // InternalGraphQL.g:221:4: (lv_query_3_0= ruleQueryNode )
            	    // InternalGraphQL.g:222:5: lv_query_3_0= ruleQueryNode
            	    {

            	    					newCompositeNode(grammarAccess.getTypeAccess().getQueryQueryNodeParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_query_3_0=ruleQueryNode();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTypeRule());
            	    					}
            	    					add(
            	    						current,
            	    						"query",
            	    						lv_query_3_0,
            	    						"co7217.miniproject2.GraphQL.QueryNode");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);

            otherlv_4=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getTypeAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleQueryNode"
    // InternalGraphQL.g:247:1: entryRuleQueryNode returns [EObject current=null] : iv_ruleQueryNode= ruleQueryNode EOF ;
    public final EObject entryRuleQueryNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQueryNode = null;


        try {
            // InternalGraphQL.g:247:50: (iv_ruleQueryNode= ruleQueryNode EOF )
            // InternalGraphQL.g:248:2: iv_ruleQueryNode= ruleQueryNode EOF
            {
             newCompositeNode(grammarAccess.getQueryNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQueryNode=ruleQueryNode();

            state._fsp--;

             current =iv_ruleQueryNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQueryNode"


    // $ANTLR start "ruleQueryNode"
    // InternalGraphQL.g:254:1: ruleQueryNode returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? ) ) ;
    public final EObject ruleQueryNode() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_QueryNode_3_0 = null;

        AntlrDatatypeRuleToken lv_QueryNode_4_0 = null;

        EObject lv_nextNode_9_0 = null;



        	enterRule();

        try {
            // InternalGraphQL.g:260:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? ) ) )
            // InternalGraphQL.g:261:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? ) )
            {
            // InternalGraphQL.g:261:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? ) )
            // InternalGraphQL.g:262:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? )
            {
            // InternalGraphQL.g:262:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGraphQL.g:263:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGraphQL.g:263:4: (lv_name_0_0= RULE_ID )
            // InternalGraphQL.g:264:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_0_0, grammarAccess.getQueryNodeAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQueryNodeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,15,FOLLOW_10); 

            			newLeafNode(otherlv_1, grammarAccess.getQueryNodeAccess().getColonKeyword_1());
            		
            // InternalGraphQL.g:284:3: ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? )
            // InternalGraphQL.g:285:4: (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )?
            {
            // InternalGraphQL.g:285:4: (otherlv_2= '[' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==16) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalGraphQL.g:286:5: otherlv_2= '['
                    {
                    otherlv_2=(Token)match(input,16,FOLLOW_11); 

                    					newLeafNode(otherlv_2, grammarAccess.getQueryNodeAccess().getLeftSquareBracketKeyword_2_0());
                    				

                    }
                    break;

            }

            // InternalGraphQL.g:291:4: ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? )
            // InternalGraphQL.g:292:5: ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )?
            {
            // InternalGraphQL.g:292:5: ( (lv_QueryNode_3_0= ruleDataID ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( ((LA5_0>=19 && LA5_0<=20)) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalGraphQL.g:293:6: (lv_QueryNode_3_0= ruleDataID )
                    {
                    // InternalGraphQL.g:293:6: (lv_QueryNode_3_0= ruleDataID )
                    // InternalGraphQL.g:294:7: lv_QueryNode_3_0= ruleDataID
                    {

                    							newCompositeNode(grammarAccess.getQueryNodeAccess().getQueryNodeDataIDParserRuleCall_2_1_0_0());
                    						
                    pushFollow(FOLLOW_12);
                    lv_QueryNode_3_0=ruleDataID();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getQueryNodeRule());
                    							}
                    							add(
                    								current,
                    								"QueryNode",
                    								lv_QueryNode_3_0,
                    								"co7217.miniproject2.GraphQL.DataID");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }
                    break;

            }

            // InternalGraphQL.g:311:5: ( (lv_QueryNode_4_0= ruleTypeID ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( ((LA6_0>=21 && LA6_0<=26)) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalGraphQL.g:312:6: (lv_QueryNode_4_0= ruleTypeID )
                    {
                    // InternalGraphQL.g:312:6: (lv_QueryNode_4_0= ruleTypeID )
                    // InternalGraphQL.g:313:7: lv_QueryNode_4_0= ruleTypeID
                    {

                    							newCompositeNode(grammarAccess.getQueryNodeAccess().getQueryNodeTypeIDParserRuleCall_2_1_1_0());
                    						
                    pushFollow(FOLLOW_13);
                    lv_QueryNode_4_0=ruleTypeID();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getQueryNodeRule());
                    							}
                    							add(
                    								current,
                    								"QueryNode",
                    								lv_QueryNode_4_0,
                    								"co7217.miniproject2.GraphQL.TypeID");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }
                    break;

            }


            }

            // InternalGraphQL.g:331:4: (otherlv_5= '!' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==17) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalGraphQL.g:332:5: otherlv_5= '!'
                    {
                    otherlv_5=(Token)match(input,17,FOLLOW_13); 

                    					newLeafNode(otherlv_5, grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_2());
                    				

                    }
                    break;

            }

            // InternalGraphQL.g:337:4: (otherlv_6= ']' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==18) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalGraphQL.g:338:5: otherlv_6= ']'
                    {
                    otherlv_6=(Token)match(input,18,FOLLOW_14); 

                    					newLeafNode(otherlv_6, grammarAccess.getQueryNodeAccess().getRightSquareBracketKeyword_2_3());
                    				

                    }
                    break;

            }

            // InternalGraphQL.g:343:4: (otherlv_7= '!' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==17) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalGraphQL.g:344:5: otherlv_7= '!'
                    {
                    otherlv_7=(Token)match(input,17,FOLLOW_15); 

                    					newLeafNode(otherlv_7, grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_4());
                    				

                    }
                    break;

            }

            // InternalGraphQL.g:349:4: (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==13) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalGraphQL.g:350:5: otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}'
                    {
                    otherlv_8=(Token)match(input,13,FOLLOW_7); 

                    					newLeafNode(otherlv_8, grammarAccess.getQueryNodeAccess().getLeftCurlyBracketKeyword_2_5_0());
                    				
                    // InternalGraphQL.g:354:5: ( (lv_nextNode_9_0= ruleQueryNode ) )
                    // InternalGraphQL.g:355:6: (lv_nextNode_9_0= ruleQueryNode )
                    {
                    // InternalGraphQL.g:355:6: (lv_nextNode_9_0= ruleQueryNode )
                    // InternalGraphQL.g:356:7: lv_nextNode_9_0= ruleQueryNode
                    {

                    							newCompositeNode(grammarAccess.getQueryNodeAccess().getNextNodeQueryNodeParserRuleCall_2_5_1_0());
                    						
                    pushFollow(FOLLOW_16);
                    lv_nextNode_9_0=ruleQueryNode();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getQueryNodeRule());
                    							}
                    							add(
                    								current,
                    								"nextNode",
                    								lv_nextNode_9_0,
                    								"co7217.miniproject2.GraphQL.QueryNode");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    otherlv_10=(Token)match(input,14,FOLLOW_2); 

                    					newLeafNode(otherlv_10, grammarAccess.getQueryNodeAccess().getRightCurlyBracketKeyword_2_5_2());
                    				

                    }
                    break;

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQueryNode"


    // $ANTLR start "entryRuleDataID"
    // InternalGraphQL.g:383:1: entryRuleDataID returns [String current=null] : iv_ruleDataID= ruleDataID EOF ;
    public final String entryRuleDataID() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDataID = null;


        try {
            // InternalGraphQL.g:383:46: (iv_ruleDataID= ruleDataID EOF )
            // InternalGraphQL.g:384:2: iv_ruleDataID= ruleDataID EOF
            {
             newCompositeNode(grammarAccess.getDataIDRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataID=ruleDataID();

            state._fsp--;

             current =iv_ruleDataID.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataID"


    // $ANTLR start "ruleDataID"
    // InternalGraphQL.g:390:1: ruleDataID returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'String' | kw= 'Integer' ) ;
    public final AntlrDatatypeRuleToken ruleDataID() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalGraphQL.g:396:2: ( (kw= 'String' | kw= 'Integer' ) )
            // InternalGraphQL.g:397:2: (kw= 'String' | kw= 'Integer' )
            {
            // InternalGraphQL.g:397:2: (kw= 'String' | kw= 'Integer' )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==19) ) {
                alt11=1;
            }
            else if ( (LA11_0==20) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalGraphQL.g:398:3: kw= 'String'
                    {
                    kw=(Token)match(input,19,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDataIDAccess().getStringKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:404:3: kw= 'Integer'
                    {
                    kw=(Token)match(input,20,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDataIDAccess().getIntegerKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataID"


    // $ANTLR start "entryRuleTypeID"
    // InternalGraphQL.g:413:1: entryRuleTypeID returns [String current=null] : iv_ruleTypeID= ruleTypeID EOF ;
    public final String entryRuleTypeID() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTypeID = null;


        try {
            // InternalGraphQL.g:413:46: (iv_ruleTypeID= ruleTypeID EOF )
            // InternalGraphQL.g:414:2: iv_ruleTypeID= ruleTypeID EOF
            {
             newCompositeNode(grammarAccess.getTypeIDRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeID=ruleTypeID();

            state._fsp--;

             current =iv_ruleTypeID.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeID"


    // $ANTLR start "ruleTypeID"
    // InternalGraphQL.g:420:1: ruleTypeID returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'User' | kw= 'Tweet' | kw= 'Entity' | kw= 'HashTag' | kw= 'UserMention' | kw= 'Url' ) ;
    public final AntlrDatatypeRuleToken ruleTypeID() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalGraphQL.g:426:2: ( (kw= 'User' | kw= 'Tweet' | kw= 'Entity' | kw= 'HashTag' | kw= 'UserMention' | kw= 'Url' ) )
            // InternalGraphQL.g:427:2: (kw= 'User' | kw= 'Tweet' | kw= 'Entity' | kw= 'HashTag' | kw= 'UserMention' | kw= 'Url' )
            {
            // InternalGraphQL.g:427:2: (kw= 'User' | kw= 'Tweet' | kw= 'Entity' | kw= 'HashTag' | kw= 'UserMention' | kw= 'Url' )
            int alt12=6;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt12=1;
                }
                break;
            case 22:
                {
                alt12=2;
                }
                break;
            case 23:
                {
                alt12=3;
                }
                break;
            case 24:
                {
                alt12=4;
                }
                break;
            case 25:
                {
                alt12=5;
                }
                break;
            case 26:
                {
                alt12=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalGraphQL.g:428:3: kw= 'User'
                    {
                    kw=(Token)match(input,21,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getUserKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:434:3: kw= 'Tweet'
                    {
                    kw=(Token)match(input,22,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getTweetKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalGraphQL.g:440:3: kw= 'Entity'
                    {
                    kw=(Token)match(input,23,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getEntityKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalGraphQL.g:446:3: kw= 'HashTag'
                    {
                    kw=(Token)match(input,24,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getHashTagKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalGraphQL.g:452:3: kw= 'UserMention'
                    {
                    kw=(Token)match(input,25,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getUserMentionKeyword_4());
                    		

                    }
                    break;
                case 6 :
                    // InternalGraphQL.g:458:3: kw= 'Url'
                    {
                    kw=(Token)match(input,26,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getUrlKeyword_5());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeID"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000007E00000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000007FF2002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000007FE2002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000007E62002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000062002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000022002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000004000L});

}