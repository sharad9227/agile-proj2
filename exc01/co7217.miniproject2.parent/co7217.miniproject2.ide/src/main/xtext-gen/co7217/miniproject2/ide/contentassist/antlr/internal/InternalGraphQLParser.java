package co7217.miniproject2.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import co7217.miniproject2.services.GraphQLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGraphQLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'String'", "'Integer'", "'User'", "'Tweet'", "'Entity'", "'HashTag'", "'UserMention'", "'Url'", "'scalar'", "'type'", "'{'", "'}'", "':'", "'['", "'!'", "']'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalGraphQLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGraphQLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGraphQLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGraphQL.g"; }


    	private GraphQLGrammarAccess grammarAccess;

    	public void setGrammarAccess(GraphQLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleGraphQL"
    // InternalGraphQL.g:53:1: entryRuleGraphQL : ruleGraphQL EOF ;
    public final void entryRuleGraphQL() throws RecognitionException {
        try {
            // InternalGraphQL.g:54:1: ( ruleGraphQL EOF )
            // InternalGraphQL.g:55:1: ruleGraphQL EOF
            {
             before(grammarAccess.getGraphQLRule()); 
            pushFollow(FOLLOW_1);
            ruleGraphQL();

            state._fsp--;

             after(grammarAccess.getGraphQLRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGraphQL"


    // $ANTLR start "ruleGraphQL"
    // InternalGraphQL.g:62:1: ruleGraphQL : ( ( rule__GraphQL__QueriesAssignment )* ) ;
    public final void ruleGraphQL() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:66:2: ( ( ( rule__GraphQL__QueriesAssignment )* ) )
            // InternalGraphQL.g:67:2: ( ( rule__GraphQL__QueriesAssignment )* )
            {
            // InternalGraphQL.g:67:2: ( ( rule__GraphQL__QueriesAssignment )* )
            // InternalGraphQL.g:68:3: ( rule__GraphQL__QueriesAssignment )*
            {
             before(grammarAccess.getGraphQLAccess().getQueriesAssignment()); 
            // InternalGraphQL.g:69:3: ( rule__GraphQL__QueriesAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=19 && LA1_0<=20)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalGraphQL.g:69:4: rule__GraphQL__QueriesAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__GraphQL__QueriesAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getGraphQLAccess().getQueriesAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGraphQL"


    // $ANTLR start "entryRuleQuery"
    // InternalGraphQL.g:78:1: entryRuleQuery : ruleQuery EOF ;
    public final void entryRuleQuery() throws RecognitionException {
        try {
            // InternalGraphQL.g:79:1: ( ruleQuery EOF )
            // InternalGraphQL.g:80:1: ruleQuery EOF
            {
             before(grammarAccess.getQueryRule()); 
            pushFollow(FOLLOW_1);
            ruleQuery();

            state._fsp--;

             after(grammarAccess.getQueryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuery"


    // $ANTLR start "ruleQuery"
    // InternalGraphQL.g:87:1: ruleQuery : ( ( rule__Query__Alternatives ) ) ;
    public final void ruleQuery() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:91:2: ( ( ( rule__Query__Alternatives ) ) )
            // InternalGraphQL.g:92:2: ( ( rule__Query__Alternatives ) )
            {
            // InternalGraphQL.g:92:2: ( ( rule__Query__Alternatives ) )
            // InternalGraphQL.g:93:3: ( rule__Query__Alternatives )
            {
             before(grammarAccess.getQueryAccess().getAlternatives()); 
            // InternalGraphQL.g:94:3: ( rule__Query__Alternatives )
            // InternalGraphQL.g:94:4: rule__Query__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Query__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getQueryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuery"


    // $ANTLR start "entryRuleDataType"
    // InternalGraphQL.g:103:1: entryRuleDataType : ruleDataType EOF ;
    public final void entryRuleDataType() throws RecognitionException {
        try {
            // InternalGraphQL.g:104:1: ( ruleDataType EOF )
            // InternalGraphQL.g:105:1: ruleDataType EOF
            {
             before(grammarAccess.getDataTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleDataType();

            state._fsp--;

             after(grammarAccess.getDataTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // InternalGraphQL.g:112:1: ruleDataType : ( ( rule__DataType__Group__0 ) ) ;
    public final void ruleDataType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:116:2: ( ( ( rule__DataType__Group__0 ) ) )
            // InternalGraphQL.g:117:2: ( ( rule__DataType__Group__0 ) )
            {
            // InternalGraphQL.g:117:2: ( ( rule__DataType__Group__0 ) )
            // InternalGraphQL.g:118:3: ( rule__DataType__Group__0 )
            {
             before(grammarAccess.getDataTypeAccess().getGroup()); 
            // InternalGraphQL.g:119:3: ( rule__DataType__Group__0 )
            // InternalGraphQL.g:119:4: rule__DataType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DataType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleType"
    // InternalGraphQL.g:128:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // InternalGraphQL.g:129:1: ( ruleType EOF )
            // InternalGraphQL.g:130:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalGraphQL.g:137:1: ruleType : ( ( rule__Type__Group__0 ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:141:2: ( ( ( rule__Type__Group__0 ) ) )
            // InternalGraphQL.g:142:2: ( ( rule__Type__Group__0 ) )
            {
            // InternalGraphQL.g:142:2: ( ( rule__Type__Group__0 ) )
            // InternalGraphQL.g:143:3: ( rule__Type__Group__0 )
            {
             before(grammarAccess.getTypeAccess().getGroup()); 
            // InternalGraphQL.g:144:3: ( rule__Type__Group__0 )
            // InternalGraphQL.g:144:4: rule__Type__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Type__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleQueryNode"
    // InternalGraphQL.g:153:1: entryRuleQueryNode : ruleQueryNode EOF ;
    public final void entryRuleQueryNode() throws RecognitionException {
        try {
            // InternalGraphQL.g:154:1: ( ruleQueryNode EOF )
            // InternalGraphQL.g:155:1: ruleQueryNode EOF
            {
             before(grammarAccess.getQueryNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleQueryNode();

            state._fsp--;

             after(grammarAccess.getQueryNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQueryNode"


    // $ANTLR start "ruleQueryNode"
    // InternalGraphQL.g:162:1: ruleQueryNode : ( ( rule__QueryNode__Group__0 ) ) ;
    public final void ruleQueryNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:166:2: ( ( ( rule__QueryNode__Group__0 ) ) )
            // InternalGraphQL.g:167:2: ( ( rule__QueryNode__Group__0 ) )
            {
            // InternalGraphQL.g:167:2: ( ( rule__QueryNode__Group__0 ) )
            // InternalGraphQL.g:168:3: ( rule__QueryNode__Group__0 )
            {
             before(grammarAccess.getQueryNodeAccess().getGroup()); 
            // InternalGraphQL.g:169:3: ( rule__QueryNode__Group__0 )
            // InternalGraphQL.g:169:4: rule__QueryNode__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryNodeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQueryNode"


    // $ANTLR start "entryRuleDataID"
    // InternalGraphQL.g:178:1: entryRuleDataID : ruleDataID EOF ;
    public final void entryRuleDataID() throws RecognitionException {
        try {
            // InternalGraphQL.g:179:1: ( ruleDataID EOF )
            // InternalGraphQL.g:180:1: ruleDataID EOF
            {
             before(grammarAccess.getDataIDRule()); 
            pushFollow(FOLLOW_1);
            ruleDataID();

            state._fsp--;

             after(grammarAccess.getDataIDRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataID"


    // $ANTLR start "ruleDataID"
    // InternalGraphQL.g:187:1: ruleDataID : ( ( rule__DataID__Alternatives ) ) ;
    public final void ruleDataID() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:191:2: ( ( ( rule__DataID__Alternatives ) ) )
            // InternalGraphQL.g:192:2: ( ( rule__DataID__Alternatives ) )
            {
            // InternalGraphQL.g:192:2: ( ( rule__DataID__Alternatives ) )
            // InternalGraphQL.g:193:3: ( rule__DataID__Alternatives )
            {
             before(grammarAccess.getDataIDAccess().getAlternatives()); 
            // InternalGraphQL.g:194:3: ( rule__DataID__Alternatives )
            // InternalGraphQL.g:194:4: rule__DataID__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DataID__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDataIDAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataID"


    // $ANTLR start "entryRuleTypeID"
    // InternalGraphQL.g:203:1: entryRuleTypeID : ruleTypeID EOF ;
    public final void entryRuleTypeID() throws RecognitionException {
        try {
            // InternalGraphQL.g:204:1: ( ruleTypeID EOF )
            // InternalGraphQL.g:205:1: ruleTypeID EOF
            {
             before(grammarAccess.getTypeIDRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeID();

            state._fsp--;

             after(grammarAccess.getTypeIDRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeID"


    // $ANTLR start "ruleTypeID"
    // InternalGraphQL.g:212:1: ruleTypeID : ( ( rule__TypeID__Alternatives ) ) ;
    public final void ruleTypeID() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:216:2: ( ( ( rule__TypeID__Alternatives ) ) )
            // InternalGraphQL.g:217:2: ( ( rule__TypeID__Alternatives ) )
            {
            // InternalGraphQL.g:217:2: ( ( rule__TypeID__Alternatives ) )
            // InternalGraphQL.g:218:3: ( rule__TypeID__Alternatives )
            {
             before(grammarAccess.getTypeIDAccess().getAlternatives()); 
            // InternalGraphQL.g:219:3: ( rule__TypeID__Alternatives )
            // InternalGraphQL.g:219:4: rule__TypeID__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TypeID__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeIDAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeID"


    // $ANTLR start "rule__Query__Alternatives"
    // InternalGraphQL.g:227:1: rule__Query__Alternatives : ( ( ruleDataType ) | ( ruleType ) );
    public final void rule__Query__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:231:1: ( ( ruleDataType ) | ( ruleType ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==19) ) {
                alt2=1;
            }
            else if ( (LA2_0==20) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalGraphQL.g:232:2: ( ruleDataType )
                    {
                    // InternalGraphQL.g:232:2: ( ruleDataType )
                    // InternalGraphQL.g:233:3: ruleDataType
                    {
                     before(grammarAccess.getQueryAccess().getDataTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleDataType();

                    state._fsp--;

                     after(grammarAccess.getQueryAccess().getDataTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:238:2: ( ruleType )
                    {
                    // InternalGraphQL.g:238:2: ( ruleType )
                    // InternalGraphQL.g:239:3: ruleType
                    {
                     before(grammarAccess.getQueryAccess().getTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleType();

                    state._fsp--;

                     after(grammarAccess.getQueryAccess().getTypeParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Alternatives"


    // $ANTLR start "rule__DataID__Alternatives"
    // InternalGraphQL.g:248:1: rule__DataID__Alternatives : ( ( 'String' ) | ( 'Integer' ) );
    public final void rule__DataID__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:252:1: ( ( 'String' ) | ( 'Integer' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==11) ) {
                alt3=1;
            }
            else if ( (LA3_0==12) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalGraphQL.g:253:2: ( 'String' )
                    {
                    // InternalGraphQL.g:253:2: ( 'String' )
                    // InternalGraphQL.g:254:3: 'String'
                    {
                     before(grammarAccess.getDataIDAccess().getStringKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getDataIDAccess().getStringKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:259:2: ( 'Integer' )
                    {
                    // InternalGraphQL.g:259:2: ( 'Integer' )
                    // InternalGraphQL.g:260:3: 'Integer'
                    {
                     before(grammarAccess.getDataIDAccess().getIntegerKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getDataIDAccess().getIntegerKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataID__Alternatives"


    // $ANTLR start "rule__TypeID__Alternatives"
    // InternalGraphQL.g:269:1: rule__TypeID__Alternatives : ( ( 'User' ) | ( 'Tweet' ) | ( 'Entity' ) | ( 'HashTag' ) | ( 'UserMention' ) | ( 'Url' ) );
    public final void rule__TypeID__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:273:1: ( ( 'User' ) | ( 'Tweet' ) | ( 'Entity' ) | ( 'HashTag' ) | ( 'UserMention' ) | ( 'Url' ) )
            int alt4=6;
            switch ( input.LA(1) ) {
            case 13:
                {
                alt4=1;
                }
                break;
            case 14:
                {
                alt4=2;
                }
                break;
            case 15:
                {
                alt4=3;
                }
                break;
            case 16:
                {
                alt4=4;
                }
                break;
            case 17:
                {
                alt4=5;
                }
                break;
            case 18:
                {
                alt4=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalGraphQL.g:274:2: ( 'User' )
                    {
                    // InternalGraphQL.g:274:2: ( 'User' )
                    // InternalGraphQL.g:275:3: 'User'
                    {
                     before(grammarAccess.getTypeIDAccess().getUserKeyword_0()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getUserKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:280:2: ( 'Tweet' )
                    {
                    // InternalGraphQL.g:280:2: ( 'Tweet' )
                    // InternalGraphQL.g:281:3: 'Tweet'
                    {
                     before(grammarAccess.getTypeIDAccess().getTweetKeyword_1()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getTweetKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGraphQL.g:286:2: ( 'Entity' )
                    {
                    // InternalGraphQL.g:286:2: ( 'Entity' )
                    // InternalGraphQL.g:287:3: 'Entity'
                    {
                     before(grammarAccess.getTypeIDAccess().getEntityKeyword_2()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getEntityKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGraphQL.g:292:2: ( 'HashTag' )
                    {
                    // InternalGraphQL.g:292:2: ( 'HashTag' )
                    // InternalGraphQL.g:293:3: 'HashTag'
                    {
                     before(grammarAccess.getTypeIDAccess().getHashTagKeyword_3()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getHashTagKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalGraphQL.g:298:2: ( 'UserMention' )
                    {
                    // InternalGraphQL.g:298:2: ( 'UserMention' )
                    // InternalGraphQL.g:299:3: 'UserMention'
                    {
                     before(grammarAccess.getTypeIDAccess().getUserMentionKeyword_4()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getUserMentionKeyword_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalGraphQL.g:304:2: ( 'Url' )
                    {
                    // InternalGraphQL.g:304:2: ( 'Url' )
                    // InternalGraphQL.g:305:3: 'Url'
                    {
                     before(grammarAccess.getTypeIDAccess().getUrlKeyword_5()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getUrlKeyword_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeID__Alternatives"


    // $ANTLR start "rule__DataType__Group__0"
    // InternalGraphQL.g:314:1: rule__DataType__Group__0 : rule__DataType__Group__0__Impl rule__DataType__Group__1 ;
    public final void rule__DataType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:318:1: ( rule__DataType__Group__0__Impl rule__DataType__Group__1 )
            // InternalGraphQL.g:319:2: rule__DataType__Group__0__Impl rule__DataType__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__DataType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__0"


    // $ANTLR start "rule__DataType__Group__0__Impl"
    // InternalGraphQL.g:326:1: rule__DataType__Group__0__Impl : ( 'scalar' ) ;
    public final void rule__DataType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:330:1: ( ( 'scalar' ) )
            // InternalGraphQL.g:331:1: ( 'scalar' )
            {
            // InternalGraphQL.g:331:1: ( 'scalar' )
            // InternalGraphQL.g:332:2: 'scalar'
            {
             before(grammarAccess.getDataTypeAccess().getScalarKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getDataTypeAccess().getScalarKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__0__Impl"


    // $ANTLR start "rule__DataType__Group__1"
    // InternalGraphQL.g:341:1: rule__DataType__Group__1 : rule__DataType__Group__1__Impl ;
    public final void rule__DataType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:345:1: ( rule__DataType__Group__1__Impl )
            // InternalGraphQL.g:346:2: rule__DataType__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__1"


    // $ANTLR start "rule__DataType__Group__1__Impl"
    // InternalGraphQL.g:352:1: rule__DataType__Group__1__Impl : ( ( rule__DataType__NameAssignment_1 ) ) ;
    public final void rule__DataType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:356:1: ( ( ( rule__DataType__NameAssignment_1 ) ) )
            // InternalGraphQL.g:357:1: ( ( rule__DataType__NameAssignment_1 ) )
            {
            // InternalGraphQL.g:357:1: ( ( rule__DataType__NameAssignment_1 ) )
            // InternalGraphQL.g:358:2: ( rule__DataType__NameAssignment_1 )
            {
             before(grammarAccess.getDataTypeAccess().getNameAssignment_1()); 
            // InternalGraphQL.g:359:2: ( rule__DataType__NameAssignment_1 )
            // InternalGraphQL.g:359:3: rule__DataType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DataType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDataTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__1__Impl"


    // $ANTLR start "rule__Type__Group__0"
    // InternalGraphQL.g:368:1: rule__Type__Group__0 : rule__Type__Group__0__Impl rule__Type__Group__1 ;
    public final void rule__Type__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:372:1: ( rule__Type__Group__0__Impl rule__Type__Group__1 )
            // InternalGraphQL.g:373:2: rule__Type__Group__0__Impl rule__Type__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Type__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__0"


    // $ANTLR start "rule__Type__Group__0__Impl"
    // InternalGraphQL.g:380:1: rule__Type__Group__0__Impl : ( 'type' ) ;
    public final void rule__Type__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:384:1: ( ( 'type' ) )
            // InternalGraphQL.g:385:1: ( 'type' )
            {
            // InternalGraphQL.g:385:1: ( 'type' )
            // InternalGraphQL.g:386:2: 'type'
            {
             before(grammarAccess.getTypeAccess().getTypeKeyword_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getTypeAccess().getTypeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__0__Impl"


    // $ANTLR start "rule__Type__Group__1"
    // InternalGraphQL.g:395:1: rule__Type__Group__1 : rule__Type__Group__1__Impl rule__Type__Group__2 ;
    public final void rule__Type__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:399:1: ( rule__Type__Group__1__Impl rule__Type__Group__2 )
            // InternalGraphQL.g:400:2: rule__Type__Group__1__Impl rule__Type__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Type__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__1"


    // $ANTLR start "rule__Type__Group__1__Impl"
    // InternalGraphQL.g:407:1: rule__Type__Group__1__Impl : ( ( rule__Type__NameAssignment_1 ) ) ;
    public final void rule__Type__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:411:1: ( ( ( rule__Type__NameAssignment_1 ) ) )
            // InternalGraphQL.g:412:1: ( ( rule__Type__NameAssignment_1 ) )
            {
            // InternalGraphQL.g:412:1: ( ( rule__Type__NameAssignment_1 ) )
            // InternalGraphQL.g:413:2: ( rule__Type__NameAssignment_1 )
            {
             before(grammarAccess.getTypeAccess().getNameAssignment_1()); 
            // InternalGraphQL.g:414:2: ( rule__Type__NameAssignment_1 )
            // InternalGraphQL.g:414:3: rule__Type__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Type__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__1__Impl"


    // $ANTLR start "rule__Type__Group__2"
    // InternalGraphQL.g:422:1: rule__Type__Group__2 : rule__Type__Group__2__Impl rule__Type__Group__3 ;
    public final void rule__Type__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:426:1: ( rule__Type__Group__2__Impl rule__Type__Group__3 )
            // InternalGraphQL.g:427:2: rule__Type__Group__2__Impl rule__Type__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Type__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__2"


    // $ANTLR start "rule__Type__Group__2__Impl"
    // InternalGraphQL.g:434:1: rule__Type__Group__2__Impl : ( '{' ) ;
    public final void rule__Type__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:438:1: ( ( '{' ) )
            // InternalGraphQL.g:439:1: ( '{' )
            {
            // InternalGraphQL.g:439:1: ( '{' )
            // InternalGraphQL.g:440:2: '{'
            {
             before(grammarAccess.getTypeAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getTypeAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__2__Impl"


    // $ANTLR start "rule__Type__Group__3"
    // InternalGraphQL.g:449:1: rule__Type__Group__3 : rule__Type__Group__3__Impl rule__Type__Group__4 ;
    public final void rule__Type__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:453:1: ( rule__Type__Group__3__Impl rule__Type__Group__4 )
            // InternalGraphQL.g:454:2: rule__Type__Group__3__Impl rule__Type__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Type__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__3"


    // $ANTLR start "rule__Type__Group__3__Impl"
    // InternalGraphQL.g:461:1: rule__Type__Group__3__Impl : ( ( ( rule__Type__QueryAssignment_3 ) ) ( ( rule__Type__QueryAssignment_3 )* ) ) ;
    public final void rule__Type__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:465:1: ( ( ( ( rule__Type__QueryAssignment_3 ) ) ( ( rule__Type__QueryAssignment_3 )* ) ) )
            // InternalGraphQL.g:466:1: ( ( ( rule__Type__QueryAssignment_3 ) ) ( ( rule__Type__QueryAssignment_3 )* ) )
            {
            // InternalGraphQL.g:466:1: ( ( ( rule__Type__QueryAssignment_3 ) ) ( ( rule__Type__QueryAssignment_3 )* ) )
            // InternalGraphQL.g:467:2: ( ( rule__Type__QueryAssignment_3 ) ) ( ( rule__Type__QueryAssignment_3 )* )
            {
            // InternalGraphQL.g:467:2: ( ( rule__Type__QueryAssignment_3 ) )
            // InternalGraphQL.g:468:3: ( rule__Type__QueryAssignment_3 )
            {
             before(grammarAccess.getTypeAccess().getQueryAssignment_3()); 
            // InternalGraphQL.g:469:3: ( rule__Type__QueryAssignment_3 )
            // InternalGraphQL.g:469:4: rule__Type__QueryAssignment_3
            {
            pushFollow(FOLLOW_9);
            rule__Type__QueryAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getQueryAssignment_3()); 

            }

            // InternalGraphQL.g:472:2: ( ( rule__Type__QueryAssignment_3 )* )
            // InternalGraphQL.g:473:3: ( rule__Type__QueryAssignment_3 )*
            {
             before(grammarAccess.getTypeAccess().getQueryAssignment_3()); 
            // InternalGraphQL.g:474:3: ( rule__Type__QueryAssignment_3 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalGraphQL.g:474:4: rule__Type__QueryAssignment_3
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Type__QueryAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getTypeAccess().getQueryAssignment_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__3__Impl"


    // $ANTLR start "rule__Type__Group__4"
    // InternalGraphQL.g:483:1: rule__Type__Group__4 : rule__Type__Group__4__Impl ;
    public final void rule__Type__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:487:1: ( rule__Type__Group__4__Impl )
            // InternalGraphQL.g:488:2: rule__Type__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Type__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__4"


    // $ANTLR start "rule__Type__Group__4__Impl"
    // InternalGraphQL.g:494:1: rule__Type__Group__4__Impl : ( '}' ) ;
    public final void rule__Type__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:498:1: ( ( '}' ) )
            // InternalGraphQL.g:499:1: ( '}' )
            {
            // InternalGraphQL.g:499:1: ( '}' )
            // InternalGraphQL.g:500:2: '}'
            {
             before(grammarAccess.getTypeAccess().getRightCurlyBracketKeyword_4()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getTypeAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__4__Impl"


    // $ANTLR start "rule__QueryNode__Group__0"
    // InternalGraphQL.g:510:1: rule__QueryNode__Group__0 : rule__QueryNode__Group__0__Impl rule__QueryNode__Group__1 ;
    public final void rule__QueryNode__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:514:1: ( rule__QueryNode__Group__0__Impl rule__QueryNode__Group__1 )
            // InternalGraphQL.g:515:2: rule__QueryNode__Group__0__Impl rule__QueryNode__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__QueryNode__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__0"


    // $ANTLR start "rule__QueryNode__Group__0__Impl"
    // InternalGraphQL.g:522:1: rule__QueryNode__Group__0__Impl : ( ( rule__QueryNode__NameAssignment_0 ) ) ;
    public final void rule__QueryNode__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:526:1: ( ( ( rule__QueryNode__NameAssignment_0 ) ) )
            // InternalGraphQL.g:527:1: ( ( rule__QueryNode__NameAssignment_0 ) )
            {
            // InternalGraphQL.g:527:1: ( ( rule__QueryNode__NameAssignment_0 ) )
            // InternalGraphQL.g:528:2: ( rule__QueryNode__NameAssignment_0 )
            {
             before(grammarAccess.getQueryNodeAccess().getNameAssignment_0()); 
            // InternalGraphQL.g:529:2: ( rule__QueryNode__NameAssignment_0 )
            // InternalGraphQL.g:529:3: rule__QueryNode__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getQueryNodeAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__0__Impl"


    // $ANTLR start "rule__QueryNode__Group__1"
    // InternalGraphQL.g:537:1: rule__QueryNode__Group__1 : rule__QueryNode__Group__1__Impl rule__QueryNode__Group__2 ;
    public final void rule__QueryNode__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:541:1: ( rule__QueryNode__Group__1__Impl rule__QueryNode__Group__2 )
            // InternalGraphQL.g:542:2: rule__QueryNode__Group__1__Impl rule__QueryNode__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__QueryNode__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__1"


    // $ANTLR start "rule__QueryNode__Group__1__Impl"
    // InternalGraphQL.g:549:1: rule__QueryNode__Group__1__Impl : ( ':' ) ;
    public final void rule__QueryNode__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:553:1: ( ( ':' ) )
            // InternalGraphQL.g:554:1: ( ':' )
            {
            // InternalGraphQL.g:554:1: ( ':' )
            // InternalGraphQL.g:555:2: ':'
            {
             before(grammarAccess.getQueryNodeAccess().getColonKeyword_1()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getQueryNodeAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__1__Impl"


    // $ANTLR start "rule__QueryNode__Group__2"
    // InternalGraphQL.g:564:1: rule__QueryNode__Group__2 : rule__QueryNode__Group__2__Impl ;
    public final void rule__QueryNode__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:568:1: ( rule__QueryNode__Group__2__Impl )
            // InternalGraphQL.g:569:2: rule__QueryNode__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__2"


    // $ANTLR start "rule__QueryNode__Group__2__Impl"
    // InternalGraphQL.g:575:1: rule__QueryNode__Group__2__Impl : ( ( rule__QueryNode__Group_2__0 ) ) ;
    public final void rule__QueryNode__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:579:1: ( ( ( rule__QueryNode__Group_2__0 ) ) )
            // InternalGraphQL.g:580:1: ( ( rule__QueryNode__Group_2__0 ) )
            {
            // InternalGraphQL.g:580:1: ( ( rule__QueryNode__Group_2__0 ) )
            // InternalGraphQL.g:581:2: ( rule__QueryNode__Group_2__0 )
            {
             before(grammarAccess.getQueryNodeAccess().getGroup_2()); 
            // InternalGraphQL.g:582:2: ( rule__QueryNode__Group_2__0 )
            // InternalGraphQL.g:582:3: rule__QueryNode__Group_2__0
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryNodeAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__2__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__0"
    // InternalGraphQL.g:591:1: rule__QueryNode__Group_2__0 : rule__QueryNode__Group_2__0__Impl rule__QueryNode__Group_2__1 ;
    public final void rule__QueryNode__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:595:1: ( rule__QueryNode__Group_2__0__Impl rule__QueryNode__Group_2__1 )
            // InternalGraphQL.g:596:2: rule__QueryNode__Group_2__0__Impl rule__QueryNode__Group_2__1
            {
            pushFollow(FOLLOW_11);
            rule__QueryNode__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__0"


    // $ANTLR start "rule__QueryNode__Group_2__0__Impl"
    // InternalGraphQL.g:603:1: rule__QueryNode__Group_2__0__Impl : ( ( '[' )? ) ;
    public final void rule__QueryNode__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:607:1: ( ( ( '[' )? ) )
            // InternalGraphQL.g:608:1: ( ( '[' )? )
            {
            // InternalGraphQL.g:608:1: ( ( '[' )? )
            // InternalGraphQL.g:609:2: ( '[' )?
            {
             before(grammarAccess.getQueryNodeAccess().getLeftSquareBracketKeyword_2_0()); 
            // InternalGraphQL.g:610:2: ( '[' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==24) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalGraphQL.g:610:3: '['
                    {
                    match(input,24,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getLeftSquareBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__0__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__1"
    // InternalGraphQL.g:618:1: rule__QueryNode__Group_2__1 : rule__QueryNode__Group_2__1__Impl rule__QueryNode__Group_2__2 ;
    public final void rule__QueryNode__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:622:1: ( rule__QueryNode__Group_2__1__Impl rule__QueryNode__Group_2__2 )
            // InternalGraphQL.g:623:2: rule__QueryNode__Group_2__1__Impl rule__QueryNode__Group_2__2
            {
            pushFollow(FOLLOW_12);
            rule__QueryNode__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__1"


    // $ANTLR start "rule__QueryNode__Group_2__1__Impl"
    // InternalGraphQL.g:630:1: rule__QueryNode__Group_2__1__Impl : ( ( rule__QueryNode__Group_2_1__0 ) ) ;
    public final void rule__QueryNode__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:634:1: ( ( ( rule__QueryNode__Group_2_1__0 ) ) )
            // InternalGraphQL.g:635:1: ( ( rule__QueryNode__Group_2_1__0 ) )
            {
            // InternalGraphQL.g:635:1: ( ( rule__QueryNode__Group_2_1__0 ) )
            // InternalGraphQL.g:636:2: ( rule__QueryNode__Group_2_1__0 )
            {
             before(grammarAccess.getQueryNodeAccess().getGroup_2_1()); 
            // InternalGraphQL.g:637:2: ( rule__QueryNode__Group_2_1__0 )
            // InternalGraphQL.g:637:3: rule__QueryNode__Group_2_1__0
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_1__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryNodeAccess().getGroup_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__1__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__2"
    // InternalGraphQL.g:645:1: rule__QueryNode__Group_2__2 : rule__QueryNode__Group_2__2__Impl rule__QueryNode__Group_2__3 ;
    public final void rule__QueryNode__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:649:1: ( rule__QueryNode__Group_2__2__Impl rule__QueryNode__Group_2__3 )
            // InternalGraphQL.g:650:2: rule__QueryNode__Group_2__2__Impl rule__QueryNode__Group_2__3
            {
            pushFollow(FOLLOW_12);
            rule__QueryNode__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__2"


    // $ANTLR start "rule__QueryNode__Group_2__2__Impl"
    // InternalGraphQL.g:657:1: rule__QueryNode__Group_2__2__Impl : ( ( '!' )? ) ;
    public final void rule__QueryNode__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:661:1: ( ( ( '!' )? ) )
            // InternalGraphQL.g:662:1: ( ( '!' )? )
            {
            // InternalGraphQL.g:662:1: ( ( '!' )? )
            // InternalGraphQL.g:663:2: ( '!' )?
            {
             before(grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_2()); 
            // InternalGraphQL.g:664:2: ( '!' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==25) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalGraphQL.g:664:3: '!'
                    {
                    match(input,25,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__2__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__3"
    // InternalGraphQL.g:672:1: rule__QueryNode__Group_2__3 : rule__QueryNode__Group_2__3__Impl rule__QueryNode__Group_2__4 ;
    public final void rule__QueryNode__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:676:1: ( rule__QueryNode__Group_2__3__Impl rule__QueryNode__Group_2__4 )
            // InternalGraphQL.g:677:2: rule__QueryNode__Group_2__3__Impl rule__QueryNode__Group_2__4
            {
            pushFollow(FOLLOW_12);
            rule__QueryNode__Group_2__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__3"


    // $ANTLR start "rule__QueryNode__Group_2__3__Impl"
    // InternalGraphQL.g:684:1: rule__QueryNode__Group_2__3__Impl : ( ( ']' )? ) ;
    public final void rule__QueryNode__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:688:1: ( ( ( ']' )? ) )
            // InternalGraphQL.g:689:1: ( ( ']' )? )
            {
            // InternalGraphQL.g:689:1: ( ( ']' )? )
            // InternalGraphQL.g:690:2: ( ']' )?
            {
             before(grammarAccess.getQueryNodeAccess().getRightSquareBracketKeyword_2_3()); 
            // InternalGraphQL.g:691:2: ( ']' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==26) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalGraphQL.g:691:3: ']'
                    {
                    match(input,26,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getRightSquareBracketKeyword_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__3__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__4"
    // InternalGraphQL.g:699:1: rule__QueryNode__Group_2__4 : rule__QueryNode__Group_2__4__Impl rule__QueryNode__Group_2__5 ;
    public final void rule__QueryNode__Group_2__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:703:1: ( rule__QueryNode__Group_2__4__Impl rule__QueryNode__Group_2__5 )
            // InternalGraphQL.g:704:2: rule__QueryNode__Group_2__4__Impl rule__QueryNode__Group_2__5
            {
            pushFollow(FOLLOW_12);
            rule__QueryNode__Group_2__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__4"


    // $ANTLR start "rule__QueryNode__Group_2__4__Impl"
    // InternalGraphQL.g:711:1: rule__QueryNode__Group_2__4__Impl : ( ( '!' )? ) ;
    public final void rule__QueryNode__Group_2__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:715:1: ( ( ( '!' )? ) )
            // InternalGraphQL.g:716:1: ( ( '!' )? )
            {
            // InternalGraphQL.g:716:1: ( ( '!' )? )
            // InternalGraphQL.g:717:2: ( '!' )?
            {
             before(grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_4()); 
            // InternalGraphQL.g:718:2: ( '!' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==25) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalGraphQL.g:718:3: '!'
                    {
                    match(input,25,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__4__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__5"
    // InternalGraphQL.g:726:1: rule__QueryNode__Group_2__5 : rule__QueryNode__Group_2__5__Impl ;
    public final void rule__QueryNode__Group_2__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:730:1: ( rule__QueryNode__Group_2__5__Impl )
            // InternalGraphQL.g:731:2: rule__QueryNode__Group_2__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__5"


    // $ANTLR start "rule__QueryNode__Group_2__5__Impl"
    // InternalGraphQL.g:737:1: rule__QueryNode__Group_2__5__Impl : ( ( rule__QueryNode__Group_2_5__0 )? ) ;
    public final void rule__QueryNode__Group_2__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:741:1: ( ( ( rule__QueryNode__Group_2_5__0 )? ) )
            // InternalGraphQL.g:742:1: ( ( rule__QueryNode__Group_2_5__0 )? )
            {
            // InternalGraphQL.g:742:1: ( ( rule__QueryNode__Group_2_5__0 )? )
            // InternalGraphQL.g:743:2: ( rule__QueryNode__Group_2_5__0 )?
            {
             before(grammarAccess.getQueryNodeAccess().getGroup_2_5()); 
            // InternalGraphQL.g:744:2: ( rule__QueryNode__Group_2_5__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==21) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalGraphQL.g:744:3: rule__QueryNode__Group_2_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__QueryNode__Group_2_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getGroup_2_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__5__Impl"


    // $ANTLR start "rule__QueryNode__Group_2_1__0"
    // InternalGraphQL.g:753:1: rule__QueryNode__Group_2_1__0 : rule__QueryNode__Group_2_1__0__Impl rule__QueryNode__Group_2_1__1 ;
    public final void rule__QueryNode__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:757:1: ( rule__QueryNode__Group_2_1__0__Impl rule__QueryNode__Group_2_1__1 )
            // InternalGraphQL.g:758:2: rule__QueryNode__Group_2_1__0__Impl rule__QueryNode__Group_2_1__1
            {
            pushFollow(FOLLOW_11);
            rule__QueryNode__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_1__0"


    // $ANTLR start "rule__QueryNode__Group_2_1__0__Impl"
    // InternalGraphQL.g:765:1: rule__QueryNode__Group_2_1__0__Impl : ( ( rule__QueryNode__QueryNodeAssignment_2_1_0 )? ) ;
    public final void rule__QueryNode__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:769:1: ( ( ( rule__QueryNode__QueryNodeAssignment_2_1_0 )? ) )
            // InternalGraphQL.g:770:1: ( ( rule__QueryNode__QueryNodeAssignment_2_1_0 )? )
            {
            // InternalGraphQL.g:770:1: ( ( rule__QueryNode__QueryNodeAssignment_2_1_0 )? )
            // InternalGraphQL.g:771:2: ( rule__QueryNode__QueryNodeAssignment_2_1_0 )?
            {
             before(grammarAccess.getQueryNodeAccess().getQueryNodeAssignment_2_1_0()); 
            // InternalGraphQL.g:772:2: ( rule__QueryNode__QueryNodeAssignment_2_1_0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( ((LA11_0>=11 && LA11_0<=12)) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalGraphQL.g:772:3: rule__QueryNode__QueryNodeAssignment_2_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__QueryNode__QueryNodeAssignment_2_1_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getQueryNodeAssignment_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_1__0__Impl"


    // $ANTLR start "rule__QueryNode__Group_2_1__1"
    // InternalGraphQL.g:780:1: rule__QueryNode__Group_2_1__1 : rule__QueryNode__Group_2_1__1__Impl ;
    public final void rule__QueryNode__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:784:1: ( rule__QueryNode__Group_2_1__1__Impl )
            // InternalGraphQL.g:785:2: rule__QueryNode__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_1__1"


    // $ANTLR start "rule__QueryNode__Group_2_1__1__Impl"
    // InternalGraphQL.g:791:1: rule__QueryNode__Group_2_1__1__Impl : ( ( rule__QueryNode__QueryNodeAssignment_2_1_1 )? ) ;
    public final void rule__QueryNode__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:795:1: ( ( ( rule__QueryNode__QueryNodeAssignment_2_1_1 )? ) )
            // InternalGraphQL.g:796:1: ( ( rule__QueryNode__QueryNodeAssignment_2_1_1 )? )
            {
            // InternalGraphQL.g:796:1: ( ( rule__QueryNode__QueryNodeAssignment_2_1_1 )? )
            // InternalGraphQL.g:797:2: ( rule__QueryNode__QueryNodeAssignment_2_1_1 )?
            {
             before(grammarAccess.getQueryNodeAccess().getQueryNodeAssignment_2_1_1()); 
            // InternalGraphQL.g:798:2: ( rule__QueryNode__QueryNodeAssignment_2_1_1 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=13 && LA12_0<=18)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalGraphQL.g:798:3: rule__QueryNode__QueryNodeAssignment_2_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__QueryNode__QueryNodeAssignment_2_1_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getQueryNodeAssignment_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_1__1__Impl"


    // $ANTLR start "rule__QueryNode__Group_2_5__0"
    // InternalGraphQL.g:807:1: rule__QueryNode__Group_2_5__0 : rule__QueryNode__Group_2_5__0__Impl rule__QueryNode__Group_2_5__1 ;
    public final void rule__QueryNode__Group_2_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:811:1: ( rule__QueryNode__Group_2_5__0__Impl rule__QueryNode__Group_2_5__1 )
            // InternalGraphQL.g:812:2: rule__QueryNode__Group_2_5__0__Impl rule__QueryNode__Group_2_5__1
            {
            pushFollow(FOLLOW_7);
            rule__QueryNode__Group_2_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__0"


    // $ANTLR start "rule__QueryNode__Group_2_5__0__Impl"
    // InternalGraphQL.g:819:1: rule__QueryNode__Group_2_5__0__Impl : ( '{' ) ;
    public final void rule__QueryNode__Group_2_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:823:1: ( ( '{' ) )
            // InternalGraphQL.g:824:1: ( '{' )
            {
            // InternalGraphQL.g:824:1: ( '{' )
            // InternalGraphQL.g:825:2: '{'
            {
             before(grammarAccess.getQueryNodeAccess().getLeftCurlyBracketKeyword_2_5_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getQueryNodeAccess().getLeftCurlyBracketKeyword_2_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__0__Impl"


    // $ANTLR start "rule__QueryNode__Group_2_5__1"
    // InternalGraphQL.g:834:1: rule__QueryNode__Group_2_5__1 : rule__QueryNode__Group_2_5__1__Impl rule__QueryNode__Group_2_5__2 ;
    public final void rule__QueryNode__Group_2_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:838:1: ( rule__QueryNode__Group_2_5__1__Impl rule__QueryNode__Group_2_5__2 )
            // InternalGraphQL.g:839:2: rule__QueryNode__Group_2_5__1__Impl rule__QueryNode__Group_2_5__2
            {
            pushFollow(FOLLOW_8);
            rule__QueryNode__Group_2_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__1"


    // $ANTLR start "rule__QueryNode__Group_2_5__1__Impl"
    // InternalGraphQL.g:846:1: rule__QueryNode__Group_2_5__1__Impl : ( ( rule__QueryNode__NextNodeAssignment_2_5_1 ) ) ;
    public final void rule__QueryNode__Group_2_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:850:1: ( ( ( rule__QueryNode__NextNodeAssignment_2_5_1 ) ) )
            // InternalGraphQL.g:851:1: ( ( rule__QueryNode__NextNodeAssignment_2_5_1 ) )
            {
            // InternalGraphQL.g:851:1: ( ( rule__QueryNode__NextNodeAssignment_2_5_1 ) )
            // InternalGraphQL.g:852:2: ( rule__QueryNode__NextNodeAssignment_2_5_1 )
            {
             before(grammarAccess.getQueryNodeAccess().getNextNodeAssignment_2_5_1()); 
            // InternalGraphQL.g:853:2: ( rule__QueryNode__NextNodeAssignment_2_5_1 )
            // InternalGraphQL.g:853:3: rule__QueryNode__NextNodeAssignment_2_5_1
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__NextNodeAssignment_2_5_1();

            state._fsp--;


            }

             after(grammarAccess.getQueryNodeAccess().getNextNodeAssignment_2_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__1__Impl"


    // $ANTLR start "rule__QueryNode__Group_2_5__2"
    // InternalGraphQL.g:861:1: rule__QueryNode__Group_2_5__2 : rule__QueryNode__Group_2_5__2__Impl ;
    public final void rule__QueryNode__Group_2_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:865:1: ( rule__QueryNode__Group_2_5__2__Impl )
            // InternalGraphQL.g:866:2: rule__QueryNode__Group_2_5__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_5__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__2"


    // $ANTLR start "rule__QueryNode__Group_2_5__2__Impl"
    // InternalGraphQL.g:872:1: rule__QueryNode__Group_2_5__2__Impl : ( '}' ) ;
    public final void rule__QueryNode__Group_2_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:876:1: ( ( '}' ) )
            // InternalGraphQL.g:877:1: ( '}' )
            {
            // InternalGraphQL.g:877:1: ( '}' )
            // InternalGraphQL.g:878:2: '}'
            {
             before(grammarAccess.getQueryNodeAccess().getRightCurlyBracketKeyword_2_5_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getQueryNodeAccess().getRightCurlyBracketKeyword_2_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__2__Impl"


    // $ANTLR start "rule__GraphQL__QueriesAssignment"
    // InternalGraphQL.g:888:1: rule__GraphQL__QueriesAssignment : ( ruleQuery ) ;
    public final void rule__GraphQL__QueriesAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:892:1: ( ( ruleQuery ) )
            // InternalGraphQL.g:893:2: ( ruleQuery )
            {
            // InternalGraphQL.g:893:2: ( ruleQuery )
            // InternalGraphQL.g:894:3: ruleQuery
            {
             before(grammarAccess.getGraphQLAccess().getQueriesQueryParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleQuery();

            state._fsp--;

             after(grammarAccess.getGraphQLAccess().getQueriesQueryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GraphQL__QueriesAssignment"


    // $ANTLR start "rule__DataType__NameAssignment_1"
    // InternalGraphQL.g:903:1: rule__DataType__NameAssignment_1 : ( ruleDataID ) ;
    public final void rule__DataType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:907:1: ( ( ruleDataID ) )
            // InternalGraphQL.g:908:2: ( ruleDataID )
            {
            // InternalGraphQL.g:908:2: ( ruleDataID )
            // InternalGraphQL.g:909:3: ruleDataID
            {
             before(grammarAccess.getDataTypeAccess().getNameDataIDParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDataID();

            state._fsp--;

             after(grammarAccess.getDataTypeAccess().getNameDataIDParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__NameAssignment_1"


    // $ANTLR start "rule__Type__NameAssignment_1"
    // InternalGraphQL.g:918:1: rule__Type__NameAssignment_1 : ( ruleTypeID ) ;
    public final void rule__Type__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:922:1: ( ( ruleTypeID ) )
            // InternalGraphQL.g:923:2: ( ruleTypeID )
            {
            // InternalGraphQL.g:923:2: ( ruleTypeID )
            // InternalGraphQL.g:924:3: ruleTypeID
            {
             before(grammarAccess.getTypeAccess().getNameTypeIDParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeID();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getNameTypeIDParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__NameAssignment_1"


    // $ANTLR start "rule__Type__QueryAssignment_3"
    // InternalGraphQL.g:933:1: rule__Type__QueryAssignment_3 : ( ruleQueryNode ) ;
    public final void rule__Type__QueryAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:937:1: ( ( ruleQueryNode ) )
            // InternalGraphQL.g:938:2: ( ruleQueryNode )
            {
            // InternalGraphQL.g:938:2: ( ruleQueryNode )
            // InternalGraphQL.g:939:3: ruleQueryNode
            {
             before(grammarAccess.getTypeAccess().getQueryQueryNodeParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleQueryNode();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getQueryQueryNodeParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__QueryAssignment_3"


    // $ANTLR start "rule__QueryNode__NameAssignment_0"
    // InternalGraphQL.g:948:1: rule__QueryNode__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__QueryNode__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:952:1: ( ( RULE_ID ) )
            // InternalGraphQL.g:953:2: ( RULE_ID )
            {
            // InternalGraphQL.g:953:2: ( RULE_ID )
            // InternalGraphQL.g:954:3: RULE_ID
            {
             before(grammarAccess.getQueryNodeAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQueryNodeAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__NameAssignment_0"


    // $ANTLR start "rule__QueryNode__QueryNodeAssignment_2_1_0"
    // InternalGraphQL.g:963:1: rule__QueryNode__QueryNodeAssignment_2_1_0 : ( ruleDataID ) ;
    public final void rule__QueryNode__QueryNodeAssignment_2_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:967:1: ( ( ruleDataID ) )
            // InternalGraphQL.g:968:2: ( ruleDataID )
            {
            // InternalGraphQL.g:968:2: ( ruleDataID )
            // InternalGraphQL.g:969:3: ruleDataID
            {
             before(grammarAccess.getQueryNodeAccess().getQueryNodeDataIDParserRuleCall_2_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDataID();

            state._fsp--;

             after(grammarAccess.getQueryNodeAccess().getQueryNodeDataIDParserRuleCall_2_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__QueryNodeAssignment_2_1_0"


    // $ANTLR start "rule__QueryNode__QueryNodeAssignment_2_1_1"
    // InternalGraphQL.g:978:1: rule__QueryNode__QueryNodeAssignment_2_1_1 : ( ruleTypeID ) ;
    public final void rule__QueryNode__QueryNodeAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:982:1: ( ( ruleTypeID ) )
            // InternalGraphQL.g:983:2: ( ruleTypeID )
            {
            // InternalGraphQL.g:983:2: ( ruleTypeID )
            // InternalGraphQL.g:984:3: ruleTypeID
            {
             before(grammarAccess.getQueryNodeAccess().getQueryNodeTypeIDParserRuleCall_2_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeID();

            state._fsp--;

             after(grammarAccess.getQueryNodeAccess().getQueryNodeTypeIDParserRuleCall_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__QueryNodeAssignment_2_1_1"


    // $ANTLR start "rule__QueryNode__NextNodeAssignment_2_5_1"
    // InternalGraphQL.g:993:1: rule__QueryNode__NextNodeAssignment_2_5_1 : ( ruleQueryNode ) ;
    public final void rule__QueryNode__NextNodeAssignment_2_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:997:1: ( ( ruleQueryNode ) )
            // InternalGraphQL.g:998:2: ( ruleQueryNode )
            {
            // InternalGraphQL.g:998:2: ( ruleQueryNode )
            // InternalGraphQL.g:999:3: ruleQueryNode
            {
             before(grammarAccess.getQueryNodeAccess().getNextNodeQueryNodeParserRuleCall_2_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQueryNode();

            state._fsp--;

             after(grammarAccess.getQueryNodeAccess().getNextNodeQueryNodeParserRuleCall_2_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__NextNodeAssignment_2_5_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000180002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000007E000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x000000000107F800L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000006200000L});

}