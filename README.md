Miniproject 2 - Agile Cloud Automation (CO4217/CO7217/CO7517)

Cloud integration: GraphQL
In this worksheet, we are going to build on what we learnt, in part 1 on NoSQL and part 2 on MDE, to analyse the architecture of Facebook's GraphQL library. In the programming exercises, the techniques and technology that have been used to build DSLs have to be applied to model part of both the GraphQL language and its semantics. In the essay, the software architecture of Facebook's GraphQL has to be discussed from a DSL perspective.
Table of contents:

Exercise 1: Text-to-Model
Exercise 2: Model-to-Text
Exercise 3: Open solution
Essay: GraphQL

Programming exercises 

Exercise 1: Text-to-Model
In this first exercise, the goal is to model an excerpt of GraphQL, focussing on the Schema Definition Language. An initial Xtext grammar is provided, which will allow you to implement the second exercise, even if this exercise is not solved.

Configuration of Xtext Project


Create an Xtext project using the following information:

project name: co7217.miniproject2

language name: co7217.miniproject2.GraphQL

extensions: graphql




Copy this Xtext grammar for defining queries in GraphQL:
 grammar co7217.miniproject2.GraphQL with org.eclipse.xtext.common.Terminals
 
 generate graphQL "http://www.miniproject2.co7217/GraphQL"
 
 GraphQL:
 	queries+=Query*;
 
 Query:
 	'query' name=ID '(' ')' '{'
 		(queryField+=QueryField)+
 	'}'
 ;
 
 QueryField:
 	name=ID ('{' (nestedQuery+=QueryField)+ '}')?
 ;



Exercise
Augment the previous Xtext grammar, in the same file, to model the excerpt of the GraphQL syntax that refers to the Schema Definition Language.
The examples below have to be parseable by your grammar:


Mandatory Field [1.5 marks]
 scalar String
 type Tweet {
     text : String !
 }  


Optional Field [1.5 marks]
 scalar String
 type Tweet {
     text : String 
 }


Mandatory String list [1.5 marks]
 scalar String
 type User {
     entities : [ String ] !
 }


Non-empty mandatory String list [1.5 marks]
 scalar String
 type User {
     entities : [ String ! ] !
 }


Optional String list [1.5 marks]
 scalar String
 type User {
     entities : [ String ] 
 }


Mandatory reference [1.5 marks]
 scalar String
 scalar Integer
 type Tweet {
     user : User !
 }
 type User {
 	 id : Integer !
 }


Optional reference [1.5 marks]
 scalar String
 scalar Integer
 type Tweet {
     user : User 
 }
 type User {
 	 id : Integer !
 }


Mandatory reference list [1.5 marks]
 scalar String
 scalar Integer
 type Tweet {
     user : [ User ] !
 }
 type User {
 	 id : Integer !
 }


Optional reference list [1.5 marks]
 scalar String
 scalar Integer
 type Tweet {
     user : [ User ] 
 }
 type User {
 	 id : Integer !
 }


Non-empty optional reference list [1.5 marks]
scalar String
scalar Integer
type Tweet {
    user : [ User ! ] 
}
type User {
	 id : Integer !
}


Model [5 marks]
scalar String
scalar Integer
type Tweet {
    text : String !
    user : User !
}
type User {
    id : Integer !
    name : String !
    screen_name : String !
    location : String 
    entities : [ Entity ] !
}
type Entity {
	hastags : [ HashTag ]
	user_mentions : [UserMention]
	urls : [Url]
}
type HashTag {
	text : String ! 
	indices : [ Integer ]
}
type UserMention {
	screen_name : String !
	name : String !
	id : Integer !
	indices : [ Integer ]
}
type Url { 
	url : String !
	expanded_url : String !
	display_url : String !
	indices : [ Integer ]
}


Take into account the following considerations:

Types of fields should be references to other types declared in the program (either scalar types or normal types). If a field is declared with a type name that does not correspond to a declared type, the parser should spot the error.
Terminals [, ] and ! carry important semantics (normally boolean features) that needs to be captured in the abstract syntax graph. This should allow you to generate code correctly in exercise 3. Check the GraphQL documentation for understanding what these are used for.
The examples provided should be parseable in the web editor that is generated from your Xtext grammar.
If there are compilation errors or the web editor cannot be executed for other technical reasons, a mark of 0 (zero) will be awarded.



Exercise 2: Model-to-Text [20 marks]
In this second exercise, we are going to implement the semantics of GraphQL queries using a model compiler. The model compiler will map GraphQL queries to Groovy scripts that use the Mongo API, used in part 1, to query JSON files from a MongoDB database on MongoDB Atlas as a normal GraphQL query would do. Our goal is twofold:

to learn how to develop a model compiler for defining the semantics of a DSL;
to combine what we learnt in part I and part II to develop a simplified software architecture for a Cloud system providing support for GraphQL, which should provide insight into the main components of the software architecture behind GraphQL.

The queries are going to be run against the Twitter data used in miniproject 1.
The model compiler must be implemented in the class co7217.miniproject2.generator.GraphQLGenerator.xtend.
The following table provides several examples that should be supported. For each row:

the column Case is a descriptor for that example;
the column GraphQL query shows the the query that should be used as input program;
the column Groovy script shows the Groovy script that should be generated by your model compiler using the Gradle task compileModel;
the column Output query shows the output that you should get after executing the Groovy script;
the column Marks is the weight of that example in the marking scheme.




Case
GraphQL query
Groovy script
 Output query
Marks




TweetText
query
Groovy
output
5 marks


TweetHashTags
query
Groovy
output
5 marks


TweetUsers
query
Groovy
output
5 marks


TweetTextAndUsers
query
Groovy
output
10 marks



Take into account the following considerations:

The Groovy script to be generated does not need to be identical to the ones provided but they must produce an identical JSON output when executed, thus preserving the semantics of GraphQL queries.
The task compileModel must execute without compilation errors and it should generate the corresponding Groovy script, which can then be executed manually to check if it works.
The execution of the model compiler for the example TweetText.graphql must generate a file TweetText.groovy in folder src/main/xtend-gen of the project co7217.miniproject2. Likewise for the other examples.   
The model compiler should work for all cases and it should not be implemented in ad-hoc way for each case.
If there are compilation errors or this task above cannot be executed for other technical reasons, a mark of 0 (zero) will be awarded for the corresponding example.


Exercise 3 
This is an open-ended completion of the two exercises above. The goal is to cover as much as possible both GraphQL's syntax and the model compiler, in order to work for any example and not just for the ones provided.
The key decisions here are:

A systematic enumeration both of GraphQL querying primitives that  are covered by the grammar and of those that are left out. The characterization of the language accepted by the grammar should be given using examples and references to the GraphQL documentation, together with a well documented Xtext grammar. Please do not provide more than 5 interesting cases.
A systematic mapping from examples using each of those querying primitives (and of their combination) to code using Groovy and MongoDB as in the examples above. The mapping should be illustrated providing examples as in the table above and a commented code generator. Please do not provide more than 5 interesting cases.

If possible, the solution should come equipped with a number of tests for the parser and for the code generator. You may get some ideas from Testing Xtext and Xtend.

Essay: GraphQL 
Similarities and differences between Groovy and Xtend for code generation [max. 500 words (excl. images and references), 15 marks]

Analyse and compare key features of the language (main advantages w.r.t. plain Java)
Analyse and compare facilities for querying data sources
Analyse and compare facilities for code generation


Discuss the software architecture of GraphQL from an DSL point of view and in a self-contained text. [max. 500 words (excl. images and references), 25 marks]

Text-to-model:

Where is a parser needed and how is it used?
In what component of the architecture is the parser?
How are text-to-model transformations applied?


Model-to-text

How is code generation used?
How could model-to-text transformations be applied?
Explain similarities/differences between the approach followed in the exercises and the GraphQL approach for code generation.





Consider the following references:

Text-to-model:

Introduction to hints to where parser is (app layer protocol)
GraphQL-parser


Model-to-text:

Code generation
GraphQL code generation
Code generator for a GraphQL schema that provides query builders and response classes. (Shopify)



Other references that may be helpful are:

Introduction to GraphQL

How to GraphQL tutorial

Core concepts



DataLoader

Data loader examples
Creating a data loader per request



Note that you are invited to do some research of your own.