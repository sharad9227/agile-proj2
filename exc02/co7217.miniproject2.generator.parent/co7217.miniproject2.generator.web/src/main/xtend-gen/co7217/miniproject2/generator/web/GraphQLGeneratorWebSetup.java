/**
 * generated by Xtext 2.18.0.M3
 */
package co7217.miniproject2.generator.web;

import co7217.miniproject2.generator.GraphQLGeneratorRuntimeModule;
import co7217.miniproject2.generator.GraphQLGeneratorStandaloneSetup;
import co7217.miniproject2.generator.ide.GraphQLGeneratorIdeModule;
import co7217.miniproject2.generator.web.GraphQLGeneratorWebModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages in web applications.
 */
@SuppressWarnings("all")
public class GraphQLGeneratorWebSetup extends GraphQLGeneratorStandaloneSetup {
  @Override
  public Injector createInjector() {
    GraphQLGeneratorRuntimeModule _graphQLGeneratorRuntimeModule = new GraphQLGeneratorRuntimeModule();
    GraphQLGeneratorIdeModule _graphQLGeneratorIdeModule = new GraphQLGeneratorIdeModule();
    GraphQLGeneratorWebModule _graphQLGeneratorWebModule = new GraphQLGeneratorWebModule();
    return Guice.createInjector(Modules2.mixin(_graphQLGeneratorRuntimeModule, _graphQLGeneratorIdeModule, _graphQLGeneratorWebModule));
  }
}
