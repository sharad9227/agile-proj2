/*
 * generated by Xtext 2.18.0.M3
 */
package co7217.miniproject2.generator.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import co7217.miniproject2.generator.graphQLGenerator.GraphQL
import co7217.miniproject2.generator.graphQLGenerator.Query
import co7217.miniproject2.generator.graphQLGenerator.QueryField

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class GraphQLGeneratorGenerator extends AbstractGenerator {

	def compile(GraphQL query) '''
package miniproject2

import com.mongodb.BasicDBObject
import com.mongodb.client.MongoClients

public class TweetText {
def static void main(String[] args) {
def mongoClient = MongoClients.create("mongodb+srv://ab373:pa89sdfua@cluster0-aocks.mongodb.net/test?retryWrites=true&w=majority");
def db = mongoClient.getDatabase('co7217-test');
def col = db.getCollection("tweets")

// let's groovy the syntax for defining filters and updates
def filter = { key, val ->
def filterAux = new BasicDBObject()
filterAux.append(key, val)
filterAux
}
def set = { key, val ->
def updateObject = new BasicDBObject()
updateObject.append('$set', new BasicDBObject().append(key,val))
updateObject
}



// query
def list = col.find().limit(10)

def result1 = ''
def notFirst = false
list.each{
if (notFirst) result1 += ","
result1 += """


     «FOR res : query.queries SEPARATOR ','»

  «res.compile»
     «ENDFOR»
'''


def compile(Query queryfld)'''
     «FOR res1 : queryfld.queryField SEPARATOR ',' »
{
  «res1.compile»
  }
     «ENDFOR»
 '''
def compile (QueryField qf)'''
«FOR b : qf.nestedQuery SEPARATOR ',' »
{
\"«b.name»\": \"${it.get("«b.name»")}\"
}"""
notFirst=true
}
def result="""
{
«qf.name»:[
$result1
]
}"""

print result


}

«ENDFOR»
'''
override doGenerate(Resource input, IFileSystemAccess2 fsa, IGeneratorContext context) {
  for (modeloutput : input.allContents.toIterable.filter(GraphQL)) {
         fsa.generateFile(
             "resultfile.groovy",
             modeloutput.compile)
     }
 }
	
}
