/*
 * generated by Xtext 2.18.0.M3
 */
grammar InternalGraphQLGenerator;

options {
	superClass=AbstractInternalAntlrParser;
}

@lexer::header {
package co7217.miniproject2.generator.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package co7217.miniproject2.generator.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import co7217.miniproject2.generator.services.GraphQLGeneratorGrammarAccess;

}

@parser::members {

 	private GraphQLGeneratorGrammarAccess grammarAccess;

    public InternalGraphQLGeneratorParser(TokenStream input, GraphQLGeneratorGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }

    @Override
    protected String getFirstRuleName() {
    	return "GraphQL";
   	}

   	@Override
   	protected GraphQLGeneratorGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}

}

@rulecatch {
    catch (RecognitionException re) {
        recover(input,re);
        appendSkippedTokens();
    }
}

// Entry rule entryRuleGraphQL
entryRuleGraphQL returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getGraphQLRule()); }
	iv_ruleGraphQL=ruleGraphQL
	{ $current=$iv_ruleGraphQL.current; }
	EOF;

// Rule GraphQL
ruleGraphQL returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				newCompositeNode(grammarAccess.getGraphQLAccess().getQueriesQueryParserRuleCall_0());
			}
			lv_queries_0_0=ruleQuery
			{
				if ($current==null) {
					$current = createModelElementForParent(grammarAccess.getGraphQLRule());
				}
				add(
					$current,
					"queries",
					lv_queries_0_0,
					"co7217.miniproject2.generator.GraphQLGenerator.Query");
				afterParserOrEnumRuleCall();
			}
		)
	)*
;

// Entry rule entryRuleQuery
entryRuleQuery returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getQueryRule()); }
	iv_ruleQuery=ruleQuery
	{ $current=$iv_ruleQuery.current; }
	EOF;

// Rule Query
ruleQuery returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='query'
		{
			newLeafNode(otherlv_0, grammarAccess.getQueryAccess().getQueryKeyword_0());
		}
		(
			(
				lv_name_1_0=RULE_ID
				{
					newLeafNode(lv_name_1_0, grammarAccess.getQueryAccess().getNameIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getQueryRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		otherlv_2='('
		{
			newLeafNode(otherlv_2, grammarAccess.getQueryAccess().getLeftParenthesisKeyword_2());
		}
		otherlv_3=')'
		{
			newLeafNode(otherlv_3, grammarAccess.getQueryAccess().getRightParenthesisKeyword_3());
		}
		otherlv_4='{'
		{
			newLeafNode(otherlv_4, grammarAccess.getQueryAccess().getLeftCurlyBracketKeyword_4());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getQueryAccess().getQueryFieldQueryFieldParserRuleCall_5_0());
				}
				lv_queryField_5_0=ruleQueryField
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getQueryRule());
					}
					add(
						$current,
						"queryField",
						lv_queryField_5_0,
						"co7217.miniproject2.generator.GraphQLGenerator.QueryField");
					afterParserOrEnumRuleCall();
				}
			)
		)+
		otherlv_6='}'
		{
			newLeafNode(otherlv_6, grammarAccess.getQueryAccess().getRightCurlyBracketKeyword_6());
		}
	)
;

// Entry rule entryRuleQueryField
entryRuleQueryField returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getQueryFieldRule()); }
	iv_ruleQueryField=ruleQueryField
	{ $current=$iv_ruleQueryField.current; }
	EOF;

// Rule QueryField
ruleQueryField returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			(
				lv_name_0_0=RULE_ID
				{
					newLeafNode(lv_name_0_0, grammarAccess.getQueryFieldAccess().getNameIDTerminalRuleCall_0_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getQueryFieldRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_0_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		(
			otherlv_1='{'
			{
				newLeafNode(otherlv_1, grammarAccess.getQueryFieldAccess().getLeftCurlyBracketKeyword_1_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getQueryFieldAccess().getNestedQueryQueryFieldParserRuleCall_1_1_0());
					}
					lv_nestedQuery_2_0=ruleQueryField
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getQueryFieldRule());
						}
						add(
							$current,
							"nestedQuery",
							lv_nestedQuery_2_0,
							"co7217.miniproject2.generator.GraphQLGenerator.QueryField");
						afterParserOrEnumRuleCall();
					}
				)
			)+
			otherlv_3='}'
			{
				newLeafNode(otherlv_3, grammarAccess.getQueryFieldAccess().getRightCurlyBracketKeyword_1_2());
			}
		)?
	)
;

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
