package co7217.miniproject2.generator.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import co7217.miniproject2.generator.services.GraphQLGeneratorGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGraphQLGeneratorParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'query'", "'('", "')'", "'{'", "'}'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int RULE_INT=5;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalGraphQLGeneratorParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGraphQLGeneratorParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGraphQLGeneratorParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGraphQLGenerator.g"; }



     	private GraphQLGeneratorGrammarAccess grammarAccess;

        public InternalGraphQLGeneratorParser(TokenStream input, GraphQLGeneratorGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "GraphQL";
       	}

       	@Override
       	protected GraphQLGeneratorGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleGraphQL"
    // InternalGraphQLGenerator.g:64:1: entryRuleGraphQL returns [EObject current=null] : iv_ruleGraphQL= ruleGraphQL EOF ;
    public final EObject entryRuleGraphQL() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGraphQL = null;


        try {
            // InternalGraphQLGenerator.g:64:48: (iv_ruleGraphQL= ruleGraphQL EOF )
            // InternalGraphQLGenerator.g:65:2: iv_ruleGraphQL= ruleGraphQL EOF
            {
             newCompositeNode(grammarAccess.getGraphQLRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGraphQL=ruleGraphQL();

            state._fsp--;

             current =iv_ruleGraphQL; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGraphQL"


    // $ANTLR start "ruleGraphQL"
    // InternalGraphQLGenerator.g:71:1: ruleGraphQL returns [EObject current=null] : ( (lv_queries_0_0= ruleQuery ) )* ;
    public final EObject ruleGraphQL() throws RecognitionException {
        EObject current = null;

        EObject lv_queries_0_0 = null;



        	enterRule();

        try {
            // InternalGraphQLGenerator.g:77:2: ( ( (lv_queries_0_0= ruleQuery ) )* )
            // InternalGraphQLGenerator.g:78:2: ( (lv_queries_0_0= ruleQuery ) )*
            {
            // InternalGraphQLGenerator.g:78:2: ( (lv_queries_0_0= ruleQuery ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalGraphQLGenerator.g:79:3: (lv_queries_0_0= ruleQuery )
            	    {
            	    // InternalGraphQLGenerator.g:79:3: (lv_queries_0_0= ruleQuery )
            	    // InternalGraphQLGenerator.g:80:4: lv_queries_0_0= ruleQuery
            	    {

            	    				newCompositeNode(grammarAccess.getGraphQLAccess().getQueriesQueryParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_queries_0_0=ruleQuery();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getGraphQLRule());
            	    				}
            	    				add(
            	    					current,
            	    					"queries",
            	    					lv_queries_0_0,
            	    					"co7217.miniproject2.generator.GraphQLGenerator.Query");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGraphQL"


    // $ANTLR start "entryRuleQuery"
    // InternalGraphQLGenerator.g:100:1: entryRuleQuery returns [EObject current=null] : iv_ruleQuery= ruleQuery EOF ;
    public final EObject entryRuleQuery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuery = null;


        try {
            // InternalGraphQLGenerator.g:100:46: (iv_ruleQuery= ruleQuery EOF )
            // InternalGraphQLGenerator.g:101:2: iv_ruleQuery= ruleQuery EOF
            {
             newCompositeNode(grammarAccess.getQueryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuery=ruleQuery();

            state._fsp--;

             current =iv_ruleQuery; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuery"


    // $ANTLR start "ruleQuery"
    // InternalGraphQLGenerator.g:107:1: ruleQuery returns [EObject current=null] : (otherlv_0= 'query' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_queryField_5_0= ruleQueryField ) )+ otherlv_6= '}' ) ;
    public final EObject ruleQuery() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_queryField_5_0 = null;



        	enterRule();

        try {
            // InternalGraphQLGenerator.g:113:2: ( (otherlv_0= 'query' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_queryField_5_0= ruleQueryField ) )+ otherlv_6= '}' ) )
            // InternalGraphQLGenerator.g:114:2: (otherlv_0= 'query' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_queryField_5_0= ruleQueryField ) )+ otherlv_6= '}' )
            {
            // InternalGraphQLGenerator.g:114:2: (otherlv_0= 'query' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_queryField_5_0= ruleQueryField ) )+ otherlv_6= '}' )
            // InternalGraphQLGenerator.g:115:3: otherlv_0= 'query' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_queryField_5_0= ruleQueryField ) )+ otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getQueryAccess().getQueryKeyword_0());
            		
            // InternalGraphQLGenerator.g:119:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalGraphQLGenerator.g:120:4: (lv_name_1_0= RULE_ID )
            {
            // InternalGraphQLGenerator.g:120:4: (lv_name_1_0= RULE_ID )
            // InternalGraphQLGenerator.g:121:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_1_0, grammarAccess.getQueryAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQueryRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getQueryAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_3, grammarAccess.getQueryAccess().getRightParenthesisKeyword_3());
            		
            otherlv_4=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_4, grammarAccess.getQueryAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalGraphQLGenerator.g:149:3: ( (lv_queryField_5_0= ruleQueryField ) )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalGraphQLGenerator.g:150:4: (lv_queryField_5_0= ruleQueryField )
            	    {
            	    // InternalGraphQLGenerator.g:150:4: (lv_queryField_5_0= ruleQueryField )
            	    // InternalGraphQLGenerator.g:151:5: lv_queryField_5_0= ruleQueryField
            	    {

            	    					newCompositeNode(grammarAccess.getQueryAccess().getQueryFieldQueryFieldParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_queryField_5_0=ruleQueryField();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getQueryRule());
            	    					}
            	    					add(
            	    						current,
            	    						"queryField",
            	    						lv_queryField_5_0,
            	    						"co7217.miniproject2.generator.GraphQLGenerator.QueryField");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);

            otherlv_6=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getQueryAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuery"


    // $ANTLR start "entryRuleQueryField"
    // InternalGraphQLGenerator.g:176:1: entryRuleQueryField returns [EObject current=null] : iv_ruleQueryField= ruleQueryField EOF ;
    public final EObject entryRuleQueryField() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQueryField = null;


        try {
            // InternalGraphQLGenerator.g:176:51: (iv_ruleQueryField= ruleQueryField EOF )
            // InternalGraphQLGenerator.g:177:2: iv_ruleQueryField= ruleQueryField EOF
            {
             newCompositeNode(grammarAccess.getQueryFieldRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQueryField=ruleQueryField();

            state._fsp--;

             current =iv_ruleQueryField; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQueryField"


    // $ANTLR start "ruleQueryField"
    // InternalGraphQLGenerator.g:183:1: ruleQueryField returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '{' ( (lv_nestedQuery_2_0= ruleQueryField ) )+ otherlv_3= '}' )? ) ;
    public final EObject ruleQueryField() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_nestedQuery_2_0 = null;



        	enterRule();

        try {
            // InternalGraphQLGenerator.g:189:2: ( ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '{' ( (lv_nestedQuery_2_0= ruleQueryField ) )+ otherlv_3= '}' )? ) )
            // InternalGraphQLGenerator.g:190:2: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '{' ( (lv_nestedQuery_2_0= ruleQueryField ) )+ otherlv_3= '}' )? )
            {
            // InternalGraphQLGenerator.g:190:2: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '{' ( (lv_nestedQuery_2_0= ruleQueryField ) )+ otherlv_3= '}' )? )
            // InternalGraphQLGenerator.g:191:3: ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '{' ( (lv_nestedQuery_2_0= ruleQueryField ) )+ otherlv_3= '}' )?
            {
            // InternalGraphQLGenerator.g:191:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGraphQLGenerator.g:192:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGraphQLGenerator.g:192:4: (lv_name_0_0= RULE_ID )
            // InternalGraphQLGenerator.g:193:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_0_0, grammarAccess.getQueryFieldAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQueryFieldRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalGraphQLGenerator.g:209:3: (otherlv_1= '{' ( (lv_nestedQuery_2_0= ruleQueryField ) )+ otherlv_3= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==14) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalGraphQLGenerator.g:210:4: otherlv_1= '{' ( (lv_nestedQuery_2_0= ruleQueryField ) )+ otherlv_3= '}'
                    {
                    otherlv_1=(Token)match(input,14,FOLLOW_4); 

                    				newLeafNode(otherlv_1, grammarAccess.getQueryFieldAccess().getLeftCurlyBracketKeyword_1_0());
                    			
                    // InternalGraphQLGenerator.g:214:4: ( (lv_nestedQuery_2_0= ruleQueryField ) )+
                    int cnt3=0;
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==RULE_ID) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalGraphQLGenerator.g:215:5: (lv_nestedQuery_2_0= ruleQueryField )
                    	    {
                    	    // InternalGraphQLGenerator.g:215:5: (lv_nestedQuery_2_0= ruleQueryField )
                    	    // InternalGraphQLGenerator.g:216:6: lv_nestedQuery_2_0= ruleQueryField
                    	    {

                    	    						newCompositeNode(grammarAccess.getQueryFieldAccess().getNestedQueryQueryFieldParserRuleCall_1_1_0());
                    	    					
                    	    pushFollow(FOLLOW_8);
                    	    lv_nestedQuery_2_0=ruleQueryField();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getQueryFieldRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"nestedQuery",
                    	    							lv_nestedQuery_2_0,
                    	    							"co7217.miniproject2.generator.GraphQLGenerator.QueryField");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt3 >= 1 ) break loop3;
                                EarlyExitException eee =
                                    new EarlyExitException(3, input);
                                throw eee;
                        }
                        cnt3++;
                    } while (true);

                    otherlv_3=(Token)match(input,15,FOLLOW_2); 

                    				newLeafNode(otherlv_3, grammarAccess.getQueryFieldAccess().getRightCurlyBracketKeyword_1_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQueryField"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004002L});

}