/**
 * generated by Xtext 2.18.0.M3
 */
package co7217.miniproject2.generator.graphQLGenerator;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co7217.miniproject2.generator.graphQLGenerator.QueryField#getName <em>Name</em>}</li>
 *   <li>{@link co7217.miniproject2.generator.graphQLGenerator.QueryField#getNestedQuery <em>Nested Query</em>}</li>
 * </ul>
 *
 * @see co7217.miniproject2.generator.graphQLGenerator.GraphQLGeneratorPackage#getQueryField()
 * @model
 * @generated
 */
public interface QueryField extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see co7217.miniproject2.generator.graphQLGenerator.GraphQLGeneratorPackage#getQueryField_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link co7217.miniproject2.generator.graphQLGenerator.QueryField#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Nested Query</b></em>' containment reference list.
   * The list contents are of type {@link co7217.miniproject2.generator.graphQLGenerator.QueryField}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nested Query</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nested Query</em>' containment reference list.
   * @see co7217.miniproject2.generator.graphQLGenerator.GraphQLGeneratorPackage#getQueryField_NestedQuery()
   * @model containment="true"
   * @generated
   */
  EList<QueryField> getNestedQuery();

} // QueryField
