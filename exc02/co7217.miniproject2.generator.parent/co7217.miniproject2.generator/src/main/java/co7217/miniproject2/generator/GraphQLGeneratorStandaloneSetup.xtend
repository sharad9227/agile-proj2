/*
 * generated by Xtext 2.18.0.M3
 */
package co7217.miniproject2.generator


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class GraphQLGeneratorStandaloneSetup extends GraphQLGeneratorStandaloneSetupGenerated {

	def static void doSetup() {
		new GraphQLGeneratorStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
