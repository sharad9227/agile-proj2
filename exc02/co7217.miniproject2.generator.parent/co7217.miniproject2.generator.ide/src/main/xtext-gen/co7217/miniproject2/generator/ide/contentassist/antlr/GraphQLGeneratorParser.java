/*
 * generated by Xtext 2.18.0.M3
 */
package co7217.miniproject2.generator.ide.contentassist.antlr;

import co7217.miniproject2.generator.ide.contentassist.antlr.internal.InternalGraphQLGeneratorParser;
import co7217.miniproject2.generator.services.GraphQLGeneratorGrammarAccess;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Map;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;

public class GraphQLGeneratorParser extends AbstractContentAssistParser {

	@Singleton
	public static final class NameMappings {
		
		private final Map<AbstractElement, String> mappings;
		
		@Inject
		public NameMappings(GraphQLGeneratorGrammarAccess grammarAccess) {
			ImmutableMap.Builder<AbstractElement, String> builder = ImmutableMap.builder();
			init(builder, grammarAccess);
			this.mappings = builder.build();
		}
		
		public String getRuleName(AbstractElement element) {
			return mappings.get(element);
		}
		
		private static void init(ImmutableMap.Builder<AbstractElement, String> builder, GraphQLGeneratorGrammarAccess grammarAccess) {
			builder.put(grammarAccess.getQueryAccess().getGroup(), "rule__Query__Group__0");
			builder.put(grammarAccess.getQueryFieldAccess().getGroup(), "rule__QueryField__Group__0");
			builder.put(grammarAccess.getQueryFieldAccess().getGroup_1(), "rule__QueryField__Group_1__0");
			builder.put(grammarAccess.getGraphQLAccess().getQueriesAssignment(), "rule__GraphQL__QueriesAssignment");
			builder.put(grammarAccess.getQueryAccess().getNameAssignment_1(), "rule__Query__NameAssignment_1");
			builder.put(grammarAccess.getQueryAccess().getQueryFieldAssignment_5(), "rule__Query__QueryFieldAssignment_5");
			builder.put(grammarAccess.getQueryFieldAccess().getNameAssignment_0(), "rule__QueryField__NameAssignment_0");
			builder.put(grammarAccess.getQueryFieldAccess().getNestedQueryAssignment_1_1(), "rule__QueryField__NestedQueryAssignment_1_1");
		}
	}
	
	@Inject
	private NameMappings nameMappings;

	@Inject
	private GraphQLGeneratorGrammarAccess grammarAccess;

	@Override
	protected InternalGraphQLGeneratorParser createParser() {
		InternalGraphQLGeneratorParser result = new InternalGraphQLGeneratorParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		return nameMappings.getRuleName(element);
	}

	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public GraphQLGeneratorGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(GraphQLGeneratorGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
	public NameMappings getNameMappings() {
		return nameMappings;
	}
	
	public void setNameMappings(NameMappings nameMappings) {
		this.nameMappings = nameMappings;
	}
}
