package co7217.miniproject2.generator.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import co7217.miniproject2.generator.services.GraphQLGeneratorGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGraphQLGeneratorParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'query'", "'('", "')'", "'{'", "'}'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int RULE_INT=5;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalGraphQLGeneratorParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGraphQLGeneratorParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGraphQLGeneratorParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGraphQLGenerator.g"; }


    	private GraphQLGeneratorGrammarAccess grammarAccess;

    	public void setGrammarAccess(GraphQLGeneratorGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleGraphQL"
    // InternalGraphQLGenerator.g:53:1: entryRuleGraphQL : ruleGraphQL EOF ;
    public final void entryRuleGraphQL() throws RecognitionException {
        try {
            // InternalGraphQLGenerator.g:54:1: ( ruleGraphQL EOF )
            // InternalGraphQLGenerator.g:55:1: ruleGraphQL EOF
            {
             before(grammarAccess.getGraphQLRule()); 
            pushFollow(FOLLOW_1);
            ruleGraphQL();

            state._fsp--;

             after(grammarAccess.getGraphQLRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGraphQL"


    // $ANTLR start "ruleGraphQL"
    // InternalGraphQLGenerator.g:62:1: ruleGraphQL : ( ( rule__GraphQL__QueriesAssignment )* ) ;
    public final void ruleGraphQL() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:66:2: ( ( ( rule__GraphQL__QueriesAssignment )* ) )
            // InternalGraphQLGenerator.g:67:2: ( ( rule__GraphQL__QueriesAssignment )* )
            {
            // InternalGraphQLGenerator.g:67:2: ( ( rule__GraphQL__QueriesAssignment )* )
            // InternalGraphQLGenerator.g:68:3: ( rule__GraphQL__QueriesAssignment )*
            {
             before(grammarAccess.getGraphQLAccess().getQueriesAssignment()); 
            // InternalGraphQLGenerator.g:69:3: ( rule__GraphQL__QueriesAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalGraphQLGenerator.g:69:4: rule__GraphQL__QueriesAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__GraphQL__QueriesAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getGraphQLAccess().getQueriesAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGraphQL"


    // $ANTLR start "entryRuleQuery"
    // InternalGraphQLGenerator.g:78:1: entryRuleQuery : ruleQuery EOF ;
    public final void entryRuleQuery() throws RecognitionException {
        try {
            // InternalGraphQLGenerator.g:79:1: ( ruleQuery EOF )
            // InternalGraphQLGenerator.g:80:1: ruleQuery EOF
            {
             before(grammarAccess.getQueryRule()); 
            pushFollow(FOLLOW_1);
            ruleQuery();

            state._fsp--;

             after(grammarAccess.getQueryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuery"


    // $ANTLR start "ruleQuery"
    // InternalGraphQLGenerator.g:87:1: ruleQuery : ( ( rule__Query__Group__0 ) ) ;
    public final void ruleQuery() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:91:2: ( ( ( rule__Query__Group__0 ) ) )
            // InternalGraphQLGenerator.g:92:2: ( ( rule__Query__Group__0 ) )
            {
            // InternalGraphQLGenerator.g:92:2: ( ( rule__Query__Group__0 ) )
            // InternalGraphQLGenerator.g:93:3: ( rule__Query__Group__0 )
            {
             before(grammarAccess.getQueryAccess().getGroup()); 
            // InternalGraphQLGenerator.g:94:3: ( rule__Query__Group__0 )
            // InternalGraphQLGenerator.g:94:4: rule__Query__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Query__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuery"


    // $ANTLR start "entryRuleQueryField"
    // InternalGraphQLGenerator.g:103:1: entryRuleQueryField : ruleQueryField EOF ;
    public final void entryRuleQueryField() throws RecognitionException {
        try {
            // InternalGraphQLGenerator.g:104:1: ( ruleQueryField EOF )
            // InternalGraphQLGenerator.g:105:1: ruleQueryField EOF
            {
             before(grammarAccess.getQueryFieldRule()); 
            pushFollow(FOLLOW_1);
            ruleQueryField();

            state._fsp--;

             after(grammarAccess.getQueryFieldRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQueryField"


    // $ANTLR start "ruleQueryField"
    // InternalGraphQLGenerator.g:112:1: ruleQueryField : ( ( rule__QueryField__Group__0 ) ) ;
    public final void ruleQueryField() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:116:2: ( ( ( rule__QueryField__Group__0 ) ) )
            // InternalGraphQLGenerator.g:117:2: ( ( rule__QueryField__Group__0 ) )
            {
            // InternalGraphQLGenerator.g:117:2: ( ( rule__QueryField__Group__0 ) )
            // InternalGraphQLGenerator.g:118:3: ( rule__QueryField__Group__0 )
            {
             before(grammarAccess.getQueryFieldAccess().getGroup()); 
            // InternalGraphQLGenerator.g:119:3: ( rule__QueryField__Group__0 )
            // InternalGraphQLGenerator.g:119:4: rule__QueryField__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QueryField__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryFieldAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQueryField"


    // $ANTLR start "rule__Query__Group__0"
    // InternalGraphQLGenerator.g:127:1: rule__Query__Group__0 : rule__Query__Group__0__Impl rule__Query__Group__1 ;
    public final void rule__Query__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:131:1: ( rule__Query__Group__0__Impl rule__Query__Group__1 )
            // InternalGraphQLGenerator.g:132:2: rule__Query__Group__0__Impl rule__Query__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Query__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Query__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__0"


    // $ANTLR start "rule__Query__Group__0__Impl"
    // InternalGraphQLGenerator.g:139:1: rule__Query__Group__0__Impl : ( 'query' ) ;
    public final void rule__Query__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:143:1: ( ( 'query' ) )
            // InternalGraphQLGenerator.g:144:1: ( 'query' )
            {
            // InternalGraphQLGenerator.g:144:1: ( 'query' )
            // InternalGraphQLGenerator.g:145:2: 'query'
            {
             before(grammarAccess.getQueryAccess().getQueryKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getQueryAccess().getQueryKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__0__Impl"


    // $ANTLR start "rule__Query__Group__1"
    // InternalGraphQLGenerator.g:154:1: rule__Query__Group__1 : rule__Query__Group__1__Impl rule__Query__Group__2 ;
    public final void rule__Query__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:158:1: ( rule__Query__Group__1__Impl rule__Query__Group__2 )
            // InternalGraphQLGenerator.g:159:2: rule__Query__Group__1__Impl rule__Query__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Query__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Query__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__1"


    // $ANTLR start "rule__Query__Group__1__Impl"
    // InternalGraphQLGenerator.g:166:1: rule__Query__Group__1__Impl : ( ( rule__Query__NameAssignment_1 ) ) ;
    public final void rule__Query__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:170:1: ( ( ( rule__Query__NameAssignment_1 ) ) )
            // InternalGraphQLGenerator.g:171:1: ( ( rule__Query__NameAssignment_1 ) )
            {
            // InternalGraphQLGenerator.g:171:1: ( ( rule__Query__NameAssignment_1 ) )
            // InternalGraphQLGenerator.g:172:2: ( rule__Query__NameAssignment_1 )
            {
             before(grammarAccess.getQueryAccess().getNameAssignment_1()); 
            // InternalGraphQLGenerator.g:173:2: ( rule__Query__NameAssignment_1 )
            // InternalGraphQLGenerator.g:173:3: rule__Query__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Query__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQueryAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__1__Impl"


    // $ANTLR start "rule__Query__Group__2"
    // InternalGraphQLGenerator.g:181:1: rule__Query__Group__2 : rule__Query__Group__2__Impl rule__Query__Group__3 ;
    public final void rule__Query__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:185:1: ( rule__Query__Group__2__Impl rule__Query__Group__3 )
            // InternalGraphQLGenerator.g:186:2: rule__Query__Group__2__Impl rule__Query__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Query__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Query__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__2"


    // $ANTLR start "rule__Query__Group__2__Impl"
    // InternalGraphQLGenerator.g:193:1: rule__Query__Group__2__Impl : ( '(' ) ;
    public final void rule__Query__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:197:1: ( ( '(' ) )
            // InternalGraphQLGenerator.g:198:1: ( '(' )
            {
            // InternalGraphQLGenerator.g:198:1: ( '(' )
            // InternalGraphQLGenerator.g:199:2: '('
            {
             before(grammarAccess.getQueryAccess().getLeftParenthesisKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getQueryAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__2__Impl"


    // $ANTLR start "rule__Query__Group__3"
    // InternalGraphQLGenerator.g:208:1: rule__Query__Group__3 : rule__Query__Group__3__Impl rule__Query__Group__4 ;
    public final void rule__Query__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:212:1: ( rule__Query__Group__3__Impl rule__Query__Group__4 )
            // InternalGraphQLGenerator.g:213:2: rule__Query__Group__3__Impl rule__Query__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__Query__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Query__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__3"


    // $ANTLR start "rule__Query__Group__3__Impl"
    // InternalGraphQLGenerator.g:220:1: rule__Query__Group__3__Impl : ( ')' ) ;
    public final void rule__Query__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:224:1: ( ( ')' ) )
            // InternalGraphQLGenerator.g:225:1: ( ')' )
            {
            // InternalGraphQLGenerator.g:225:1: ( ')' )
            // InternalGraphQLGenerator.g:226:2: ')'
            {
             before(grammarAccess.getQueryAccess().getRightParenthesisKeyword_3()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getQueryAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__3__Impl"


    // $ANTLR start "rule__Query__Group__4"
    // InternalGraphQLGenerator.g:235:1: rule__Query__Group__4 : rule__Query__Group__4__Impl rule__Query__Group__5 ;
    public final void rule__Query__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:239:1: ( rule__Query__Group__4__Impl rule__Query__Group__5 )
            // InternalGraphQLGenerator.g:240:2: rule__Query__Group__4__Impl rule__Query__Group__5
            {
            pushFollow(FOLLOW_4);
            rule__Query__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Query__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__4"


    // $ANTLR start "rule__Query__Group__4__Impl"
    // InternalGraphQLGenerator.g:247:1: rule__Query__Group__4__Impl : ( '{' ) ;
    public final void rule__Query__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:251:1: ( ( '{' ) )
            // InternalGraphQLGenerator.g:252:1: ( '{' )
            {
            // InternalGraphQLGenerator.g:252:1: ( '{' )
            // InternalGraphQLGenerator.g:253:2: '{'
            {
             before(grammarAccess.getQueryAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getQueryAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__4__Impl"


    // $ANTLR start "rule__Query__Group__5"
    // InternalGraphQLGenerator.g:262:1: rule__Query__Group__5 : rule__Query__Group__5__Impl rule__Query__Group__6 ;
    public final void rule__Query__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:266:1: ( rule__Query__Group__5__Impl rule__Query__Group__6 )
            // InternalGraphQLGenerator.g:267:2: rule__Query__Group__5__Impl rule__Query__Group__6
            {
            pushFollow(FOLLOW_8);
            rule__Query__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Query__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__5"


    // $ANTLR start "rule__Query__Group__5__Impl"
    // InternalGraphQLGenerator.g:274:1: rule__Query__Group__5__Impl : ( ( ( rule__Query__QueryFieldAssignment_5 ) ) ( ( rule__Query__QueryFieldAssignment_5 )* ) ) ;
    public final void rule__Query__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:278:1: ( ( ( ( rule__Query__QueryFieldAssignment_5 ) ) ( ( rule__Query__QueryFieldAssignment_5 )* ) ) )
            // InternalGraphQLGenerator.g:279:1: ( ( ( rule__Query__QueryFieldAssignment_5 ) ) ( ( rule__Query__QueryFieldAssignment_5 )* ) )
            {
            // InternalGraphQLGenerator.g:279:1: ( ( ( rule__Query__QueryFieldAssignment_5 ) ) ( ( rule__Query__QueryFieldAssignment_5 )* ) )
            // InternalGraphQLGenerator.g:280:2: ( ( rule__Query__QueryFieldAssignment_5 ) ) ( ( rule__Query__QueryFieldAssignment_5 )* )
            {
            // InternalGraphQLGenerator.g:280:2: ( ( rule__Query__QueryFieldAssignment_5 ) )
            // InternalGraphQLGenerator.g:281:3: ( rule__Query__QueryFieldAssignment_5 )
            {
             before(grammarAccess.getQueryAccess().getQueryFieldAssignment_5()); 
            // InternalGraphQLGenerator.g:282:3: ( rule__Query__QueryFieldAssignment_5 )
            // InternalGraphQLGenerator.g:282:4: rule__Query__QueryFieldAssignment_5
            {
            pushFollow(FOLLOW_9);
            rule__Query__QueryFieldAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getQueryAccess().getQueryFieldAssignment_5()); 

            }

            // InternalGraphQLGenerator.g:285:2: ( ( rule__Query__QueryFieldAssignment_5 )* )
            // InternalGraphQLGenerator.g:286:3: ( rule__Query__QueryFieldAssignment_5 )*
            {
             before(grammarAccess.getQueryAccess().getQueryFieldAssignment_5()); 
            // InternalGraphQLGenerator.g:287:3: ( rule__Query__QueryFieldAssignment_5 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalGraphQLGenerator.g:287:4: rule__Query__QueryFieldAssignment_5
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Query__QueryFieldAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getQueryAccess().getQueryFieldAssignment_5()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__5__Impl"


    // $ANTLR start "rule__Query__Group__6"
    // InternalGraphQLGenerator.g:296:1: rule__Query__Group__6 : rule__Query__Group__6__Impl ;
    public final void rule__Query__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:300:1: ( rule__Query__Group__6__Impl )
            // InternalGraphQLGenerator.g:301:2: rule__Query__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Query__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__6"


    // $ANTLR start "rule__Query__Group__6__Impl"
    // InternalGraphQLGenerator.g:307:1: rule__Query__Group__6__Impl : ( '}' ) ;
    public final void rule__Query__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:311:1: ( ( '}' ) )
            // InternalGraphQLGenerator.g:312:1: ( '}' )
            {
            // InternalGraphQLGenerator.g:312:1: ( '}' )
            // InternalGraphQLGenerator.g:313:2: '}'
            {
             before(grammarAccess.getQueryAccess().getRightCurlyBracketKeyword_6()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getQueryAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Group__6__Impl"


    // $ANTLR start "rule__QueryField__Group__0"
    // InternalGraphQLGenerator.g:323:1: rule__QueryField__Group__0 : rule__QueryField__Group__0__Impl rule__QueryField__Group__1 ;
    public final void rule__QueryField__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:327:1: ( rule__QueryField__Group__0__Impl rule__QueryField__Group__1 )
            // InternalGraphQLGenerator.g:328:2: rule__QueryField__Group__0__Impl rule__QueryField__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__QueryField__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryField__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__Group__0"


    // $ANTLR start "rule__QueryField__Group__0__Impl"
    // InternalGraphQLGenerator.g:335:1: rule__QueryField__Group__0__Impl : ( ( rule__QueryField__NameAssignment_0 ) ) ;
    public final void rule__QueryField__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:339:1: ( ( ( rule__QueryField__NameAssignment_0 ) ) )
            // InternalGraphQLGenerator.g:340:1: ( ( rule__QueryField__NameAssignment_0 ) )
            {
            // InternalGraphQLGenerator.g:340:1: ( ( rule__QueryField__NameAssignment_0 ) )
            // InternalGraphQLGenerator.g:341:2: ( rule__QueryField__NameAssignment_0 )
            {
             before(grammarAccess.getQueryFieldAccess().getNameAssignment_0()); 
            // InternalGraphQLGenerator.g:342:2: ( rule__QueryField__NameAssignment_0 )
            // InternalGraphQLGenerator.g:342:3: rule__QueryField__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__QueryField__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getQueryFieldAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__Group__0__Impl"


    // $ANTLR start "rule__QueryField__Group__1"
    // InternalGraphQLGenerator.g:350:1: rule__QueryField__Group__1 : rule__QueryField__Group__1__Impl ;
    public final void rule__QueryField__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:354:1: ( rule__QueryField__Group__1__Impl )
            // InternalGraphQLGenerator.g:355:2: rule__QueryField__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryField__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__Group__1"


    // $ANTLR start "rule__QueryField__Group__1__Impl"
    // InternalGraphQLGenerator.g:361:1: rule__QueryField__Group__1__Impl : ( ( rule__QueryField__Group_1__0 )? ) ;
    public final void rule__QueryField__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:365:1: ( ( ( rule__QueryField__Group_1__0 )? ) )
            // InternalGraphQLGenerator.g:366:1: ( ( rule__QueryField__Group_1__0 )? )
            {
            // InternalGraphQLGenerator.g:366:1: ( ( rule__QueryField__Group_1__0 )? )
            // InternalGraphQLGenerator.g:367:2: ( rule__QueryField__Group_1__0 )?
            {
             before(grammarAccess.getQueryFieldAccess().getGroup_1()); 
            // InternalGraphQLGenerator.g:368:2: ( rule__QueryField__Group_1__0 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalGraphQLGenerator.g:368:3: rule__QueryField__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__QueryField__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQueryFieldAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__Group__1__Impl"


    // $ANTLR start "rule__QueryField__Group_1__0"
    // InternalGraphQLGenerator.g:377:1: rule__QueryField__Group_1__0 : rule__QueryField__Group_1__0__Impl rule__QueryField__Group_1__1 ;
    public final void rule__QueryField__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:381:1: ( rule__QueryField__Group_1__0__Impl rule__QueryField__Group_1__1 )
            // InternalGraphQLGenerator.g:382:2: rule__QueryField__Group_1__0__Impl rule__QueryField__Group_1__1
            {
            pushFollow(FOLLOW_4);
            rule__QueryField__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryField__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__Group_1__0"


    // $ANTLR start "rule__QueryField__Group_1__0__Impl"
    // InternalGraphQLGenerator.g:389:1: rule__QueryField__Group_1__0__Impl : ( '{' ) ;
    public final void rule__QueryField__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:393:1: ( ( '{' ) )
            // InternalGraphQLGenerator.g:394:1: ( '{' )
            {
            // InternalGraphQLGenerator.g:394:1: ( '{' )
            // InternalGraphQLGenerator.g:395:2: '{'
            {
             before(grammarAccess.getQueryFieldAccess().getLeftCurlyBracketKeyword_1_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getQueryFieldAccess().getLeftCurlyBracketKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__Group_1__0__Impl"


    // $ANTLR start "rule__QueryField__Group_1__1"
    // InternalGraphQLGenerator.g:404:1: rule__QueryField__Group_1__1 : rule__QueryField__Group_1__1__Impl rule__QueryField__Group_1__2 ;
    public final void rule__QueryField__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:408:1: ( rule__QueryField__Group_1__1__Impl rule__QueryField__Group_1__2 )
            // InternalGraphQLGenerator.g:409:2: rule__QueryField__Group_1__1__Impl rule__QueryField__Group_1__2
            {
            pushFollow(FOLLOW_8);
            rule__QueryField__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryField__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__Group_1__1"


    // $ANTLR start "rule__QueryField__Group_1__1__Impl"
    // InternalGraphQLGenerator.g:416:1: rule__QueryField__Group_1__1__Impl : ( ( ( rule__QueryField__NestedQueryAssignment_1_1 ) ) ( ( rule__QueryField__NestedQueryAssignment_1_1 )* ) ) ;
    public final void rule__QueryField__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:420:1: ( ( ( ( rule__QueryField__NestedQueryAssignment_1_1 ) ) ( ( rule__QueryField__NestedQueryAssignment_1_1 )* ) ) )
            // InternalGraphQLGenerator.g:421:1: ( ( ( rule__QueryField__NestedQueryAssignment_1_1 ) ) ( ( rule__QueryField__NestedQueryAssignment_1_1 )* ) )
            {
            // InternalGraphQLGenerator.g:421:1: ( ( ( rule__QueryField__NestedQueryAssignment_1_1 ) ) ( ( rule__QueryField__NestedQueryAssignment_1_1 )* ) )
            // InternalGraphQLGenerator.g:422:2: ( ( rule__QueryField__NestedQueryAssignment_1_1 ) ) ( ( rule__QueryField__NestedQueryAssignment_1_1 )* )
            {
            // InternalGraphQLGenerator.g:422:2: ( ( rule__QueryField__NestedQueryAssignment_1_1 ) )
            // InternalGraphQLGenerator.g:423:3: ( rule__QueryField__NestedQueryAssignment_1_1 )
            {
             before(grammarAccess.getQueryFieldAccess().getNestedQueryAssignment_1_1()); 
            // InternalGraphQLGenerator.g:424:3: ( rule__QueryField__NestedQueryAssignment_1_1 )
            // InternalGraphQLGenerator.g:424:4: rule__QueryField__NestedQueryAssignment_1_1
            {
            pushFollow(FOLLOW_9);
            rule__QueryField__NestedQueryAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getQueryFieldAccess().getNestedQueryAssignment_1_1()); 

            }

            // InternalGraphQLGenerator.g:427:2: ( ( rule__QueryField__NestedQueryAssignment_1_1 )* )
            // InternalGraphQLGenerator.g:428:3: ( rule__QueryField__NestedQueryAssignment_1_1 )*
            {
             before(grammarAccess.getQueryFieldAccess().getNestedQueryAssignment_1_1()); 
            // InternalGraphQLGenerator.g:429:3: ( rule__QueryField__NestedQueryAssignment_1_1 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalGraphQLGenerator.g:429:4: rule__QueryField__NestedQueryAssignment_1_1
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__QueryField__NestedQueryAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getQueryFieldAccess().getNestedQueryAssignment_1_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__Group_1__1__Impl"


    // $ANTLR start "rule__QueryField__Group_1__2"
    // InternalGraphQLGenerator.g:438:1: rule__QueryField__Group_1__2 : rule__QueryField__Group_1__2__Impl ;
    public final void rule__QueryField__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:442:1: ( rule__QueryField__Group_1__2__Impl )
            // InternalGraphQLGenerator.g:443:2: rule__QueryField__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryField__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__Group_1__2"


    // $ANTLR start "rule__QueryField__Group_1__2__Impl"
    // InternalGraphQLGenerator.g:449:1: rule__QueryField__Group_1__2__Impl : ( '}' ) ;
    public final void rule__QueryField__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:453:1: ( ( '}' ) )
            // InternalGraphQLGenerator.g:454:1: ( '}' )
            {
            // InternalGraphQLGenerator.g:454:1: ( '}' )
            // InternalGraphQLGenerator.g:455:2: '}'
            {
             before(grammarAccess.getQueryFieldAccess().getRightCurlyBracketKeyword_1_2()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getQueryFieldAccess().getRightCurlyBracketKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__Group_1__2__Impl"


    // $ANTLR start "rule__GraphQL__QueriesAssignment"
    // InternalGraphQLGenerator.g:465:1: rule__GraphQL__QueriesAssignment : ( ruleQuery ) ;
    public final void rule__GraphQL__QueriesAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:469:1: ( ( ruleQuery ) )
            // InternalGraphQLGenerator.g:470:2: ( ruleQuery )
            {
            // InternalGraphQLGenerator.g:470:2: ( ruleQuery )
            // InternalGraphQLGenerator.g:471:3: ruleQuery
            {
             before(grammarAccess.getGraphQLAccess().getQueriesQueryParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleQuery();

            state._fsp--;

             after(grammarAccess.getGraphQLAccess().getQueriesQueryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GraphQL__QueriesAssignment"


    // $ANTLR start "rule__Query__NameAssignment_1"
    // InternalGraphQLGenerator.g:480:1: rule__Query__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Query__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:484:1: ( ( RULE_ID ) )
            // InternalGraphQLGenerator.g:485:2: ( RULE_ID )
            {
            // InternalGraphQLGenerator.g:485:2: ( RULE_ID )
            // InternalGraphQLGenerator.g:486:3: RULE_ID
            {
             before(grammarAccess.getQueryAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQueryAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__NameAssignment_1"


    // $ANTLR start "rule__Query__QueryFieldAssignment_5"
    // InternalGraphQLGenerator.g:495:1: rule__Query__QueryFieldAssignment_5 : ( ruleQueryField ) ;
    public final void rule__Query__QueryFieldAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:499:1: ( ( ruleQueryField ) )
            // InternalGraphQLGenerator.g:500:2: ( ruleQueryField )
            {
            // InternalGraphQLGenerator.g:500:2: ( ruleQueryField )
            // InternalGraphQLGenerator.g:501:3: ruleQueryField
            {
             before(grammarAccess.getQueryAccess().getQueryFieldQueryFieldParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleQueryField();

            state._fsp--;

             after(grammarAccess.getQueryAccess().getQueryFieldQueryFieldParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__QueryFieldAssignment_5"


    // $ANTLR start "rule__QueryField__NameAssignment_0"
    // InternalGraphQLGenerator.g:510:1: rule__QueryField__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__QueryField__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:514:1: ( ( RULE_ID ) )
            // InternalGraphQLGenerator.g:515:2: ( RULE_ID )
            {
            // InternalGraphQLGenerator.g:515:2: ( RULE_ID )
            // InternalGraphQLGenerator.g:516:3: RULE_ID
            {
             before(grammarAccess.getQueryFieldAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQueryFieldAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__NameAssignment_0"


    // $ANTLR start "rule__QueryField__NestedQueryAssignment_1_1"
    // InternalGraphQLGenerator.g:525:1: rule__QueryField__NestedQueryAssignment_1_1 : ( ruleQueryField ) ;
    public final void rule__QueryField__NestedQueryAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQLGenerator.g:529:1: ( ( ruleQueryField ) )
            // InternalGraphQLGenerator.g:530:2: ( ruleQueryField )
            {
            // InternalGraphQLGenerator.g:530:2: ( ruleQueryField )
            // InternalGraphQLGenerator.g:531:3: ruleQueryField
            {
             before(grammarAccess.getQueryFieldAccess().getNestedQueryQueryFieldParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQueryField();

            state._fsp--;

             after(grammarAccess.getQueryFieldAccess().getNestedQueryQueryFieldParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryField__NestedQueryAssignment_1_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000012L});

}