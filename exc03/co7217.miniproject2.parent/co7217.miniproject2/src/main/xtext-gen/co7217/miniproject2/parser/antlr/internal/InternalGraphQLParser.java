package co7217.miniproject2.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import co7217.miniproject2.services.GraphQLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGraphQLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'scalar'", "'type'", "'('", "')'", "'{'", "'}'", "':'", "'['", "'!'", "']'", "'String'", "'Integer'", "'Int'", "'Float'", "'True'", "'False'", "'Date'", "'User'", "'Entity'", "'HashTag'", "'UserMention'", "'Url'", "'Tweet'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalGraphQLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGraphQLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGraphQLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGraphQL.g"; }



     	private GraphQLGrammarAccess grammarAccess;

        public InternalGraphQLParser(TokenStream input, GraphQLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "GraphQL";
       	}

       	@Override
       	protected GraphQLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleGraphQL"
    // InternalGraphQL.g:64:1: entryRuleGraphQL returns [EObject current=null] : iv_ruleGraphQL= ruleGraphQL EOF ;
    public final EObject entryRuleGraphQL() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGraphQL = null;


        try {
            // InternalGraphQL.g:64:48: (iv_ruleGraphQL= ruleGraphQL EOF )
            // InternalGraphQL.g:65:2: iv_ruleGraphQL= ruleGraphQL EOF
            {
             newCompositeNode(grammarAccess.getGraphQLRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGraphQL=ruleGraphQL();

            state._fsp--;

             current =iv_ruleGraphQL; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGraphQL"


    // $ANTLR start "ruleGraphQL"
    // InternalGraphQL.g:71:1: ruleGraphQL returns [EObject current=null] : ( (lv_queries_0_0= ruleQuery ) )* ;
    public final EObject ruleGraphQL() throws RecognitionException {
        EObject current = null;

        EObject lv_queries_0_0 = null;



        	enterRule();

        try {
            // InternalGraphQL.g:77:2: ( ( (lv_queries_0_0= ruleQuery ) )* )
            // InternalGraphQL.g:78:2: ( (lv_queries_0_0= ruleQuery ) )*
            {
            // InternalGraphQL.g:78:2: ( (lv_queries_0_0= ruleQuery ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=11 && LA1_0<=12)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalGraphQL.g:79:3: (lv_queries_0_0= ruleQuery )
            	    {
            	    // InternalGraphQL.g:79:3: (lv_queries_0_0= ruleQuery )
            	    // InternalGraphQL.g:80:4: lv_queries_0_0= ruleQuery
            	    {

            	    				newCompositeNode(grammarAccess.getGraphQLAccess().getQueriesQueryParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_queries_0_0=ruleQuery();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getGraphQLRule());
            	    				}
            	    				add(
            	    					current,
            	    					"queries",
            	    					lv_queries_0_0,
            	    					"co7217.miniproject2.GraphQL.Query");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGraphQL"


    // $ANTLR start "entryRuleQuery"
    // InternalGraphQL.g:100:1: entryRuleQuery returns [EObject current=null] : iv_ruleQuery= ruleQuery EOF ;
    public final EObject entryRuleQuery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuery = null;


        try {
            // InternalGraphQL.g:100:46: (iv_ruleQuery= ruleQuery EOF )
            // InternalGraphQL.g:101:2: iv_ruleQuery= ruleQuery EOF
            {
             newCompositeNode(grammarAccess.getQueryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuery=ruleQuery();

            state._fsp--;

             current =iv_ruleQuery; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuery"


    // $ANTLR start "ruleQuery"
    // InternalGraphQL.g:107:1: ruleQuery returns [EObject current=null] : (this_DataType_0= ruleDataType | this_Type_1= ruleType ) ;
    public final EObject ruleQuery() throws RecognitionException {
        EObject current = null;

        EObject this_DataType_0 = null;

        EObject this_Type_1 = null;



        	enterRule();

        try {
            // InternalGraphQL.g:113:2: ( (this_DataType_0= ruleDataType | this_Type_1= ruleType ) )
            // InternalGraphQL.g:114:2: (this_DataType_0= ruleDataType | this_Type_1= ruleType )
            {
            // InternalGraphQL.g:114:2: (this_DataType_0= ruleDataType | this_Type_1= ruleType )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalGraphQL.g:115:3: this_DataType_0= ruleDataType
                    {

                    			newCompositeNode(grammarAccess.getQueryAccess().getDataTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_DataType_0=ruleDataType();

                    state._fsp--;


                    			current = this_DataType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:124:3: this_Type_1= ruleType
                    {

                    			newCompositeNode(grammarAccess.getQueryAccess().getTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Type_1=ruleType();

                    state._fsp--;


                    			current = this_Type_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuery"


    // $ANTLR start "entryRuleDataType"
    // InternalGraphQL.g:136:1: entryRuleDataType returns [EObject current=null] : iv_ruleDataType= ruleDataType EOF ;
    public final EObject entryRuleDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataType = null;


        try {
            // InternalGraphQL.g:136:49: (iv_ruleDataType= ruleDataType EOF )
            // InternalGraphQL.g:137:2: iv_ruleDataType= ruleDataType EOF
            {
             newCompositeNode(grammarAccess.getDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataType=ruleDataType();

            state._fsp--;

             current =iv_ruleDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // InternalGraphQL.g:143:1: ruleDataType returns [EObject current=null] : (otherlv_0= 'scalar' ( (lv_name_1_0= ruleDataID ) ) ) ;
    public final EObject ruleDataType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalGraphQL.g:149:2: ( (otherlv_0= 'scalar' ( (lv_name_1_0= ruleDataID ) ) ) )
            // InternalGraphQL.g:150:2: (otherlv_0= 'scalar' ( (lv_name_1_0= ruleDataID ) ) )
            {
            // InternalGraphQL.g:150:2: (otherlv_0= 'scalar' ( (lv_name_1_0= ruleDataID ) ) )
            // InternalGraphQL.g:151:3: otherlv_0= 'scalar' ( (lv_name_1_0= ruleDataID ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getDataTypeAccess().getScalarKeyword_0());
            		
            // InternalGraphQL.g:155:3: ( (lv_name_1_0= ruleDataID ) )
            // InternalGraphQL.g:156:4: (lv_name_1_0= ruleDataID )
            {
            // InternalGraphQL.g:156:4: (lv_name_1_0= ruleDataID )
            // InternalGraphQL.g:157:5: lv_name_1_0= ruleDataID
            {

            					newCompositeNode(grammarAccess.getDataTypeAccess().getNameDataIDParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleDataID();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDataTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"co7217.miniproject2.GraphQL.DataID");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleType"
    // InternalGraphQL.g:178:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalGraphQL.g:178:45: (iv_ruleType= ruleType EOF )
            // InternalGraphQL.g:179:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalGraphQL.g:185:1: ruleType returns [EObject current=null] : ( (otherlv_0= 'type' )+ ( (lv_name_1_0= ruleTypeID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_query_5_0= ruleQueryNode ) )+ otherlv_6= '}' ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_query_5_0 = null;



        	enterRule();

        try {
            // InternalGraphQL.g:191:2: ( ( (otherlv_0= 'type' )+ ( (lv_name_1_0= ruleTypeID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_query_5_0= ruleQueryNode ) )+ otherlv_6= '}' ) )
            // InternalGraphQL.g:192:2: ( (otherlv_0= 'type' )+ ( (lv_name_1_0= ruleTypeID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_query_5_0= ruleQueryNode ) )+ otherlv_6= '}' )
            {
            // InternalGraphQL.g:192:2: ( (otherlv_0= 'type' )+ ( (lv_name_1_0= ruleTypeID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_query_5_0= ruleQueryNode ) )+ otherlv_6= '}' )
            // InternalGraphQL.g:193:3: (otherlv_0= 'type' )+ ( (lv_name_1_0= ruleTypeID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_query_5_0= ruleQueryNode ) )+ otherlv_6= '}'
            {
            // InternalGraphQL.g:193:3: (otherlv_0= 'type' )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==12) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalGraphQL.g:194:4: otherlv_0= 'type'
            	    {
            	    otherlv_0=(Token)match(input,12,FOLLOW_5); 

            	    				newLeafNode(otherlv_0, grammarAccess.getTypeAccess().getTypeKeyword_0());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);

            // InternalGraphQL.g:199:3: ( (lv_name_1_0= ruleTypeID ) )
            // InternalGraphQL.g:200:4: (lv_name_1_0= ruleTypeID )
            {
            // InternalGraphQL.g:200:4: (lv_name_1_0= ruleTypeID )
            // InternalGraphQL.g:201:5: lv_name_1_0= ruleTypeID
            {

            					newCompositeNode(grammarAccess.getTypeAccess().getNameTypeIDParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_1_0=ruleTypeID();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"co7217.miniproject2.GraphQL.TypeID");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getTypeAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_3, grammarAccess.getTypeAccess().getRightParenthesisKeyword_3());
            		
            otherlv_4=(Token)match(input,15,FOLLOW_9); 

            			newLeafNode(otherlv_4, grammarAccess.getTypeAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalGraphQL.g:230:3: ( (lv_query_5_0= ruleQueryNode ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalGraphQL.g:231:4: (lv_query_5_0= ruleQueryNode )
            	    {
            	    // InternalGraphQL.g:231:4: (lv_query_5_0= ruleQueryNode )
            	    // InternalGraphQL.g:232:5: lv_query_5_0= ruleQueryNode
            	    {

            	    					newCompositeNode(grammarAccess.getTypeAccess().getQueryQueryNodeParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_10);
            	    lv_query_5_0=ruleQueryNode();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTypeRule());
            	    					}
            	    					add(
            	    						current,
            	    						"query",
            	    						lv_query_5_0,
            	    						"co7217.miniproject2.GraphQL.QueryNode");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);

            otherlv_6=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getTypeAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleQueryNode"
    // InternalGraphQL.g:257:1: entryRuleQueryNode returns [EObject current=null] : iv_ruleQueryNode= ruleQueryNode EOF ;
    public final EObject entryRuleQueryNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQueryNode = null;


        try {
            // InternalGraphQL.g:257:50: (iv_ruleQueryNode= ruleQueryNode EOF )
            // InternalGraphQL.g:258:2: iv_ruleQueryNode= ruleQueryNode EOF
            {
             newCompositeNode(grammarAccess.getQueryNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQueryNode=ruleQueryNode();

            state._fsp--;

             current =iv_ruleQueryNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQueryNode"


    // $ANTLR start "ruleQueryNode"
    // InternalGraphQL.g:264:1: ruleQueryNode returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? ) ) ;
    public final EObject ruleQueryNode() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_QueryNode_3_0 = null;

        AntlrDatatypeRuleToken lv_QueryNode_4_0 = null;

        EObject lv_nextNode_9_0 = null;



        	enterRule();

        try {
            // InternalGraphQL.g:270:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? ) ) )
            // InternalGraphQL.g:271:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? ) )
            {
            // InternalGraphQL.g:271:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? ) )
            // InternalGraphQL.g:272:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? )
            {
            // InternalGraphQL.g:272:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGraphQL.g:273:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGraphQL.g:273:4: (lv_name_0_0= RULE_ID )
            // InternalGraphQL.g:274:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_11); 

            					newLeafNode(lv_name_0_0, grammarAccess.getQueryNodeAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQueryNodeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,17,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getQueryNodeAccess().getColonKeyword_1());
            		
            // InternalGraphQL.g:294:3: ( (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )? )
            // InternalGraphQL.g:295:4: (otherlv_2= '[' )? ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? ) (otherlv_5= '!' )? (otherlv_6= ']' )? (otherlv_7= '!' )? (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )?
            {
            // InternalGraphQL.g:295:4: (otherlv_2= '[' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalGraphQL.g:296:5: otherlv_2= '['
                    {
                    otherlv_2=(Token)match(input,18,FOLLOW_13); 

                    					newLeafNode(otherlv_2, grammarAccess.getQueryNodeAccess().getLeftSquareBracketKeyword_2_0());
                    				

                    }
                    break;

            }

            // InternalGraphQL.g:301:4: ( ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )? )
            // InternalGraphQL.g:302:5: ( (lv_QueryNode_3_0= ruleDataID ) )? ( (lv_QueryNode_4_0= ruleTypeID ) )?
            {
            // InternalGraphQL.g:302:5: ( (lv_QueryNode_3_0= ruleDataID ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( ((LA6_0>=21 && LA6_0<=27)) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalGraphQL.g:303:6: (lv_QueryNode_3_0= ruleDataID )
                    {
                    // InternalGraphQL.g:303:6: (lv_QueryNode_3_0= ruleDataID )
                    // InternalGraphQL.g:304:7: lv_QueryNode_3_0= ruleDataID
                    {

                    							newCompositeNode(grammarAccess.getQueryNodeAccess().getQueryNodeDataIDParserRuleCall_2_1_0_0());
                    						
                    pushFollow(FOLLOW_14);
                    lv_QueryNode_3_0=ruleDataID();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getQueryNodeRule());
                    							}
                    							add(
                    								current,
                    								"QueryNode",
                    								lv_QueryNode_3_0,
                    								"co7217.miniproject2.GraphQL.DataID");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }
                    break;

            }

            // InternalGraphQL.g:321:5: ( (lv_QueryNode_4_0= ruleTypeID ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0>=28 && LA7_0<=33)) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalGraphQL.g:322:6: (lv_QueryNode_4_0= ruleTypeID )
                    {
                    // InternalGraphQL.g:322:6: (lv_QueryNode_4_0= ruleTypeID )
                    // InternalGraphQL.g:323:7: lv_QueryNode_4_0= ruleTypeID
                    {

                    							newCompositeNode(grammarAccess.getQueryNodeAccess().getQueryNodeTypeIDParserRuleCall_2_1_1_0());
                    						
                    pushFollow(FOLLOW_15);
                    lv_QueryNode_4_0=ruleTypeID();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getQueryNodeRule());
                    							}
                    							add(
                    								current,
                    								"QueryNode",
                    								lv_QueryNode_4_0,
                    								"co7217.miniproject2.GraphQL.TypeID");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }
                    break;

            }


            }

            // InternalGraphQL.g:341:4: (otherlv_5= '!' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==19) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalGraphQL.g:342:5: otherlv_5= '!'
                    {
                    otherlv_5=(Token)match(input,19,FOLLOW_15); 

                    					newLeafNode(otherlv_5, grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_2());
                    				

                    }
                    break;

            }

            // InternalGraphQL.g:347:4: (otherlv_6= ']' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==20) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalGraphQL.g:348:5: otherlv_6= ']'
                    {
                    otherlv_6=(Token)match(input,20,FOLLOW_16); 

                    					newLeafNode(otherlv_6, grammarAccess.getQueryNodeAccess().getRightSquareBracketKeyword_2_3());
                    				

                    }
                    break;

            }

            // InternalGraphQL.g:353:4: (otherlv_7= '!' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==19) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalGraphQL.g:354:5: otherlv_7= '!'
                    {
                    otherlv_7=(Token)match(input,19,FOLLOW_17); 

                    					newLeafNode(otherlv_7, grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_4());
                    				

                    }
                    break;

            }

            // InternalGraphQL.g:359:4: (otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==15) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalGraphQL.g:360:5: otherlv_8= '{' ( (lv_nextNode_9_0= ruleQueryNode ) ) otherlv_10= '}'
                    {
                    otherlv_8=(Token)match(input,15,FOLLOW_9); 

                    					newLeafNode(otherlv_8, grammarAccess.getQueryNodeAccess().getLeftCurlyBracketKeyword_2_5_0());
                    				
                    // InternalGraphQL.g:364:5: ( (lv_nextNode_9_0= ruleQueryNode ) )
                    // InternalGraphQL.g:365:6: (lv_nextNode_9_0= ruleQueryNode )
                    {
                    // InternalGraphQL.g:365:6: (lv_nextNode_9_0= ruleQueryNode )
                    // InternalGraphQL.g:366:7: lv_nextNode_9_0= ruleQueryNode
                    {

                    							newCompositeNode(grammarAccess.getQueryNodeAccess().getNextNodeQueryNodeParserRuleCall_2_5_1_0());
                    						
                    pushFollow(FOLLOW_18);
                    lv_nextNode_9_0=ruleQueryNode();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getQueryNodeRule());
                    							}
                    							add(
                    								current,
                    								"nextNode",
                    								lv_nextNode_9_0,
                    								"co7217.miniproject2.GraphQL.QueryNode");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    otherlv_10=(Token)match(input,16,FOLLOW_2); 

                    					newLeafNode(otherlv_10, grammarAccess.getQueryNodeAccess().getRightCurlyBracketKeyword_2_5_2());
                    				

                    }
                    break;

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQueryNode"


    // $ANTLR start "entryRuleDataID"
    // InternalGraphQL.g:393:1: entryRuleDataID returns [String current=null] : iv_ruleDataID= ruleDataID EOF ;
    public final String entryRuleDataID() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDataID = null;


        try {
            // InternalGraphQL.g:393:46: (iv_ruleDataID= ruleDataID EOF )
            // InternalGraphQL.g:394:2: iv_ruleDataID= ruleDataID EOF
            {
             newCompositeNode(grammarAccess.getDataIDRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataID=ruleDataID();

            state._fsp--;

             current =iv_ruleDataID.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataID"


    // $ANTLR start "ruleDataID"
    // InternalGraphQL.g:400:1: ruleDataID returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'String' | kw= 'Integer' | kw= 'Int' | kw= 'Float' | kw= 'True' | kw= 'False' | kw= 'Date' ) ;
    public final AntlrDatatypeRuleToken ruleDataID() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalGraphQL.g:406:2: ( (kw= 'String' | kw= 'Integer' | kw= 'Int' | kw= 'Float' | kw= 'True' | kw= 'False' | kw= 'Date' ) )
            // InternalGraphQL.g:407:2: (kw= 'String' | kw= 'Integer' | kw= 'Int' | kw= 'Float' | kw= 'True' | kw= 'False' | kw= 'Date' )
            {
            // InternalGraphQL.g:407:2: (kw= 'String' | kw= 'Integer' | kw= 'Int' | kw= 'Float' | kw= 'True' | kw= 'False' | kw= 'Date' )
            int alt12=7;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt12=1;
                }
                break;
            case 22:
                {
                alt12=2;
                }
                break;
            case 23:
                {
                alt12=3;
                }
                break;
            case 24:
                {
                alt12=4;
                }
                break;
            case 25:
                {
                alt12=5;
                }
                break;
            case 26:
                {
                alt12=6;
                }
                break;
            case 27:
                {
                alt12=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalGraphQL.g:408:3: kw= 'String'
                    {
                    kw=(Token)match(input,21,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDataIDAccess().getStringKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:414:3: kw= 'Integer'
                    {
                    kw=(Token)match(input,22,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDataIDAccess().getIntegerKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalGraphQL.g:420:3: kw= 'Int'
                    {
                    kw=(Token)match(input,23,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDataIDAccess().getIntKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalGraphQL.g:426:3: kw= 'Float'
                    {
                    kw=(Token)match(input,24,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDataIDAccess().getFloatKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalGraphQL.g:432:3: kw= 'True'
                    {
                    kw=(Token)match(input,25,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDataIDAccess().getTrueKeyword_4());
                    		

                    }
                    break;
                case 6 :
                    // InternalGraphQL.g:438:3: kw= 'False'
                    {
                    kw=(Token)match(input,26,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDataIDAccess().getFalseKeyword_5());
                    		

                    }
                    break;
                case 7 :
                    // InternalGraphQL.g:444:3: kw= 'Date'
                    {
                    kw=(Token)match(input,27,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDataIDAccess().getDateKeyword_6());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataID"


    // $ANTLR start "entryRuleTypeID"
    // InternalGraphQL.g:453:1: entryRuleTypeID returns [String current=null] : iv_ruleTypeID= ruleTypeID EOF ;
    public final String entryRuleTypeID() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTypeID = null;


        try {
            // InternalGraphQL.g:453:46: (iv_ruleTypeID= ruleTypeID EOF )
            // InternalGraphQL.g:454:2: iv_ruleTypeID= ruleTypeID EOF
            {
             newCompositeNode(grammarAccess.getTypeIDRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeID=ruleTypeID();

            state._fsp--;

             current =iv_ruleTypeID.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeID"


    // $ANTLR start "ruleTypeID"
    // InternalGraphQL.g:460:1: ruleTypeID returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'User' | kw= 'Entity' | kw= 'HashTag' | kw= 'UserMention' | kw= 'Url' | kw= 'Tweet' ) ;
    public final AntlrDatatypeRuleToken ruleTypeID() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalGraphQL.g:466:2: ( (kw= 'User' | kw= 'Entity' | kw= 'HashTag' | kw= 'UserMention' | kw= 'Url' | kw= 'Tweet' ) )
            // InternalGraphQL.g:467:2: (kw= 'User' | kw= 'Entity' | kw= 'HashTag' | kw= 'UserMention' | kw= 'Url' | kw= 'Tweet' )
            {
            // InternalGraphQL.g:467:2: (kw= 'User' | kw= 'Entity' | kw= 'HashTag' | kw= 'UserMention' | kw= 'Url' | kw= 'Tweet' )
            int alt13=6;
            switch ( input.LA(1) ) {
            case 28:
                {
                alt13=1;
                }
                break;
            case 29:
                {
                alt13=2;
                }
                break;
            case 30:
                {
                alt13=3;
                }
                break;
            case 31:
                {
                alt13=4;
                }
                break;
            case 32:
                {
                alt13=5;
                }
                break;
            case 33:
                {
                alt13=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalGraphQL.g:468:3: kw= 'User'
                    {
                    kw=(Token)match(input,28,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getUserKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:474:3: kw= 'Entity'
                    {
                    kw=(Token)match(input,29,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getEntityKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalGraphQL.g:480:3: kw= 'HashTag'
                    {
                    kw=(Token)match(input,30,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getHashTagKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalGraphQL.g:486:3: kw= 'UserMention'
                    {
                    kw=(Token)match(input,31,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getUserMentionKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalGraphQL.g:492:3: kw= 'Url'
                    {
                    kw=(Token)match(input,32,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getUrlKeyword_4());
                    		

                    }
                    break;
                case 6 :
                    // InternalGraphQL.g:498:3: kw= 'Tweet'
                    {
                    kw=(Token)match(input,33,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeIDAccess().getTweetKeyword_5());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeID"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x000000000FE00000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000003F0001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000003FFFC8002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x00000003FFF88002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00000003F0188002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000188002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000088002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000010000L});

}