package co7217.miniproject2.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import co7217.miniproject2.services.GraphQLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGraphQLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'String'", "'Integer'", "'Int'", "'Float'", "'True'", "'False'", "'Date'", "'User'", "'Entity'", "'HashTag'", "'UserMention'", "'Url'", "'Tweet'", "'scalar'", "'type'", "'('", "')'", "'{'", "'}'", "':'", "'['", "'!'", "']'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalGraphQLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGraphQLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGraphQLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGraphQL.g"; }


    	private GraphQLGrammarAccess grammarAccess;

    	public void setGrammarAccess(GraphQLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleGraphQL"
    // InternalGraphQL.g:53:1: entryRuleGraphQL : ruleGraphQL EOF ;
    public final void entryRuleGraphQL() throws RecognitionException {
        try {
            // InternalGraphQL.g:54:1: ( ruleGraphQL EOF )
            // InternalGraphQL.g:55:1: ruleGraphQL EOF
            {
             before(grammarAccess.getGraphQLRule()); 
            pushFollow(FOLLOW_1);
            ruleGraphQL();

            state._fsp--;

             after(grammarAccess.getGraphQLRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGraphQL"


    // $ANTLR start "ruleGraphQL"
    // InternalGraphQL.g:62:1: ruleGraphQL : ( ( rule__GraphQL__QueriesAssignment )* ) ;
    public final void ruleGraphQL() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:66:2: ( ( ( rule__GraphQL__QueriesAssignment )* ) )
            // InternalGraphQL.g:67:2: ( ( rule__GraphQL__QueriesAssignment )* )
            {
            // InternalGraphQL.g:67:2: ( ( rule__GraphQL__QueriesAssignment )* )
            // InternalGraphQL.g:68:3: ( rule__GraphQL__QueriesAssignment )*
            {
             before(grammarAccess.getGraphQLAccess().getQueriesAssignment()); 
            // InternalGraphQL.g:69:3: ( rule__GraphQL__QueriesAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=24 && LA1_0<=25)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalGraphQL.g:69:4: rule__GraphQL__QueriesAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__GraphQL__QueriesAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getGraphQLAccess().getQueriesAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGraphQL"


    // $ANTLR start "entryRuleQuery"
    // InternalGraphQL.g:78:1: entryRuleQuery : ruleQuery EOF ;
    public final void entryRuleQuery() throws RecognitionException {
        try {
            // InternalGraphQL.g:79:1: ( ruleQuery EOF )
            // InternalGraphQL.g:80:1: ruleQuery EOF
            {
             before(grammarAccess.getQueryRule()); 
            pushFollow(FOLLOW_1);
            ruleQuery();

            state._fsp--;

             after(grammarAccess.getQueryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuery"


    // $ANTLR start "ruleQuery"
    // InternalGraphQL.g:87:1: ruleQuery : ( ( rule__Query__Alternatives ) ) ;
    public final void ruleQuery() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:91:2: ( ( ( rule__Query__Alternatives ) ) )
            // InternalGraphQL.g:92:2: ( ( rule__Query__Alternatives ) )
            {
            // InternalGraphQL.g:92:2: ( ( rule__Query__Alternatives ) )
            // InternalGraphQL.g:93:3: ( rule__Query__Alternatives )
            {
             before(grammarAccess.getQueryAccess().getAlternatives()); 
            // InternalGraphQL.g:94:3: ( rule__Query__Alternatives )
            // InternalGraphQL.g:94:4: rule__Query__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Query__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getQueryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuery"


    // $ANTLR start "entryRuleDataType"
    // InternalGraphQL.g:103:1: entryRuleDataType : ruleDataType EOF ;
    public final void entryRuleDataType() throws RecognitionException {
        try {
            // InternalGraphQL.g:104:1: ( ruleDataType EOF )
            // InternalGraphQL.g:105:1: ruleDataType EOF
            {
             before(grammarAccess.getDataTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleDataType();

            state._fsp--;

             after(grammarAccess.getDataTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // InternalGraphQL.g:112:1: ruleDataType : ( ( rule__DataType__Group__0 ) ) ;
    public final void ruleDataType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:116:2: ( ( ( rule__DataType__Group__0 ) ) )
            // InternalGraphQL.g:117:2: ( ( rule__DataType__Group__0 ) )
            {
            // InternalGraphQL.g:117:2: ( ( rule__DataType__Group__0 ) )
            // InternalGraphQL.g:118:3: ( rule__DataType__Group__0 )
            {
             before(grammarAccess.getDataTypeAccess().getGroup()); 
            // InternalGraphQL.g:119:3: ( rule__DataType__Group__0 )
            // InternalGraphQL.g:119:4: rule__DataType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DataType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleType"
    // InternalGraphQL.g:128:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // InternalGraphQL.g:129:1: ( ruleType EOF )
            // InternalGraphQL.g:130:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalGraphQL.g:137:1: ruleType : ( ( rule__Type__Group__0 ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:141:2: ( ( ( rule__Type__Group__0 ) ) )
            // InternalGraphQL.g:142:2: ( ( rule__Type__Group__0 ) )
            {
            // InternalGraphQL.g:142:2: ( ( rule__Type__Group__0 ) )
            // InternalGraphQL.g:143:3: ( rule__Type__Group__0 )
            {
             before(grammarAccess.getTypeAccess().getGroup()); 
            // InternalGraphQL.g:144:3: ( rule__Type__Group__0 )
            // InternalGraphQL.g:144:4: rule__Type__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Type__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleQueryNode"
    // InternalGraphQL.g:153:1: entryRuleQueryNode : ruleQueryNode EOF ;
    public final void entryRuleQueryNode() throws RecognitionException {
        try {
            // InternalGraphQL.g:154:1: ( ruleQueryNode EOF )
            // InternalGraphQL.g:155:1: ruleQueryNode EOF
            {
             before(grammarAccess.getQueryNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleQueryNode();

            state._fsp--;

             after(grammarAccess.getQueryNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQueryNode"


    // $ANTLR start "ruleQueryNode"
    // InternalGraphQL.g:162:1: ruleQueryNode : ( ( rule__QueryNode__Group__0 ) ) ;
    public final void ruleQueryNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:166:2: ( ( ( rule__QueryNode__Group__0 ) ) )
            // InternalGraphQL.g:167:2: ( ( rule__QueryNode__Group__0 ) )
            {
            // InternalGraphQL.g:167:2: ( ( rule__QueryNode__Group__0 ) )
            // InternalGraphQL.g:168:3: ( rule__QueryNode__Group__0 )
            {
             before(grammarAccess.getQueryNodeAccess().getGroup()); 
            // InternalGraphQL.g:169:3: ( rule__QueryNode__Group__0 )
            // InternalGraphQL.g:169:4: rule__QueryNode__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryNodeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQueryNode"


    // $ANTLR start "entryRuleDataID"
    // InternalGraphQL.g:178:1: entryRuleDataID : ruleDataID EOF ;
    public final void entryRuleDataID() throws RecognitionException {
        try {
            // InternalGraphQL.g:179:1: ( ruleDataID EOF )
            // InternalGraphQL.g:180:1: ruleDataID EOF
            {
             before(grammarAccess.getDataIDRule()); 
            pushFollow(FOLLOW_1);
            ruleDataID();

            state._fsp--;

             after(grammarAccess.getDataIDRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataID"


    // $ANTLR start "ruleDataID"
    // InternalGraphQL.g:187:1: ruleDataID : ( ( rule__DataID__Alternatives ) ) ;
    public final void ruleDataID() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:191:2: ( ( ( rule__DataID__Alternatives ) ) )
            // InternalGraphQL.g:192:2: ( ( rule__DataID__Alternatives ) )
            {
            // InternalGraphQL.g:192:2: ( ( rule__DataID__Alternatives ) )
            // InternalGraphQL.g:193:3: ( rule__DataID__Alternatives )
            {
             before(grammarAccess.getDataIDAccess().getAlternatives()); 
            // InternalGraphQL.g:194:3: ( rule__DataID__Alternatives )
            // InternalGraphQL.g:194:4: rule__DataID__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DataID__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDataIDAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataID"


    // $ANTLR start "entryRuleTypeID"
    // InternalGraphQL.g:203:1: entryRuleTypeID : ruleTypeID EOF ;
    public final void entryRuleTypeID() throws RecognitionException {
        try {
            // InternalGraphQL.g:204:1: ( ruleTypeID EOF )
            // InternalGraphQL.g:205:1: ruleTypeID EOF
            {
             before(grammarAccess.getTypeIDRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeID();

            state._fsp--;

             after(grammarAccess.getTypeIDRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeID"


    // $ANTLR start "ruleTypeID"
    // InternalGraphQL.g:212:1: ruleTypeID : ( ( rule__TypeID__Alternatives ) ) ;
    public final void ruleTypeID() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:216:2: ( ( ( rule__TypeID__Alternatives ) ) )
            // InternalGraphQL.g:217:2: ( ( rule__TypeID__Alternatives ) )
            {
            // InternalGraphQL.g:217:2: ( ( rule__TypeID__Alternatives ) )
            // InternalGraphQL.g:218:3: ( rule__TypeID__Alternatives )
            {
             before(grammarAccess.getTypeIDAccess().getAlternatives()); 
            // InternalGraphQL.g:219:3: ( rule__TypeID__Alternatives )
            // InternalGraphQL.g:219:4: rule__TypeID__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TypeID__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeIDAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeID"


    // $ANTLR start "rule__Query__Alternatives"
    // InternalGraphQL.g:227:1: rule__Query__Alternatives : ( ( ruleDataType ) | ( ruleType ) );
    public final void rule__Query__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:231:1: ( ( ruleDataType ) | ( ruleType ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==24) ) {
                alt2=1;
            }
            else if ( (LA2_0==25) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalGraphQL.g:232:2: ( ruleDataType )
                    {
                    // InternalGraphQL.g:232:2: ( ruleDataType )
                    // InternalGraphQL.g:233:3: ruleDataType
                    {
                     before(grammarAccess.getQueryAccess().getDataTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleDataType();

                    state._fsp--;

                     after(grammarAccess.getQueryAccess().getDataTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:238:2: ( ruleType )
                    {
                    // InternalGraphQL.g:238:2: ( ruleType )
                    // InternalGraphQL.g:239:3: ruleType
                    {
                     before(grammarAccess.getQueryAccess().getTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleType();

                    state._fsp--;

                     after(grammarAccess.getQueryAccess().getTypeParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__Alternatives"


    // $ANTLR start "rule__DataID__Alternatives"
    // InternalGraphQL.g:248:1: rule__DataID__Alternatives : ( ( 'String' ) | ( 'Integer' ) | ( 'Int' ) | ( 'Float' ) | ( 'True' ) | ( 'False' ) | ( 'Date' ) );
    public final void rule__DataID__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:252:1: ( ( 'String' ) | ( 'Integer' ) | ( 'Int' ) | ( 'Float' ) | ( 'True' ) | ( 'False' ) | ( 'Date' ) )
            int alt3=7;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt3=1;
                }
                break;
            case 12:
                {
                alt3=2;
                }
                break;
            case 13:
                {
                alt3=3;
                }
                break;
            case 14:
                {
                alt3=4;
                }
                break;
            case 15:
                {
                alt3=5;
                }
                break;
            case 16:
                {
                alt3=6;
                }
                break;
            case 17:
                {
                alt3=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalGraphQL.g:253:2: ( 'String' )
                    {
                    // InternalGraphQL.g:253:2: ( 'String' )
                    // InternalGraphQL.g:254:3: 'String'
                    {
                     before(grammarAccess.getDataIDAccess().getStringKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getDataIDAccess().getStringKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:259:2: ( 'Integer' )
                    {
                    // InternalGraphQL.g:259:2: ( 'Integer' )
                    // InternalGraphQL.g:260:3: 'Integer'
                    {
                     before(grammarAccess.getDataIDAccess().getIntegerKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getDataIDAccess().getIntegerKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGraphQL.g:265:2: ( 'Int' )
                    {
                    // InternalGraphQL.g:265:2: ( 'Int' )
                    // InternalGraphQL.g:266:3: 'Int'
                    {
                     before(grammarAccess.getDataIDAccess().getIntKeyword_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getDataIDAccess().getIntKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGraphQL.g:271:2: ( 'Float' )
                    {
                    // InternalGraphQL.g:271:2: ( 'Float' )
                    // InternalGraphQL.g:272:3: 'Float'
                    {
                     before(grammarAccess.getDataIDAccess().getFloatKeyword_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getDataIDAccess().getFloatKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalGraphQL.g:277:2: ( 'True' )
                    {
                    // InternalGraphQL.g:277:2: ( 'True' )
                    // InternalGraphQL.g:278:3: 'True'
                    {
                     before(grammarAccess.getDataIDAccess().getTrueKeyword_4()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getDataIDAccess().getTrueKeyword_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalGraphQL.g:283:2: ( 'False' )
                    {
                    // InternalGraphQL.g:283:2: ( 'False' )
                    // InternalGraphQL.g:284:3: 'False'
                    {
                     before(grammarAccess.getDataIDAccess().getFalseKeyword_5()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getDataIDAccess().getFalseKeyword_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalGraphQL.g:289:2: ( 'Date' )
                    {
                    // InternalGraphQL.g:289:2: ( 'Date' )
                    // InternalGraphQL.g:290:3: 'Date'
                    {
                     before(grammarAccess.getDataIDAccess().getDateKeyword_6()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getDataIDAccess().getDateKeyword_6()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataID__Alternatives"


    // $ANTLR start "rule__TypeID__Alternatives"
    // InternalGraphQL.g:299:1: rule__TypeID__Alternatives : ( ( 'User' ) | ( 'Entity' ) | ( 'HashTag' ) | ( 'UserMention' ) | ( 'Url' ) | ( 'Tweet' ) );
    public final void rule__TypeID__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:303:1: ( ( 'User' ) | ( 'Entity' ) | ( 'HashTag' ) | ( 'UserMention' ) | ( 'Url' ) | ( 'Tweet' ) )
            int alt4=6;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt4=1;
                }
                break;
            case 19:
                {
                alt4=2;
                }
                break;
            case 20:
                {
                alt4=3;
                }
                break;
            case 21:
                {
                alt4=4;
                }
                break;
            case 22:
                {
                alt4=5;
                }
                break;
            case 23:
                {
                alt4=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalGraphQL.g:304:2: ( 'User' )
                    {
                    // InternalGraphQL.g:304:2: ( 'User' )
                    // InternalGraphQL.g:305:3: 'User'
                    {
                     before(grammarAccess.getTypeIDAccess().getUserKeyword_0()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getUserKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGraphQL.g:310:2: ( 'Entity' )
                    {
                    // InternalGraphQL.g:310:2: ( 'Entity' )
                    // InternalGraphQL.g:311:3: 'Entity'
                    {
                     before(grammarAccess.getTypeIDAccess().getEntityKeyword_1()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getEntityKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGraphQL.g:316:2: ( 'HashTag' )
                    {
                    // InternalGraphQL.g:316:2: ( 'HashTag' )
                    // InternalGraphQL.g:317:3: 'HashTag'
                    {
                     before(grammarAccess.getTypeIDAccess().getHashTagKeyword_2()); 
                    match(input,20,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getHashTagKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGraphQL.g:322:2: ( 'UserMention' )
                    {
                    // InternalGraphQL.g:322:2: ( 'UserMention' )
                    // InternalGraphQL.g:323:3: 'UserMention'
                    {
                     before(grammarAccess.getTypeIDAccess().getUserMentionKeyword_3()); 
                    match(input,21,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getUserMentionKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalGraphQL.g:328:2: ( 'Url' )
                    {
                    // InternalGraphQL.g:328:2: ( 'Url' )
                    // InternalGraphQL.g:329:3: 'Url'
                    {
                     before(grammarAccess.getTypeIDAccess().getUrlKeyword_4()); 
                    match(input,22,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getUrlKeyword_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalGraphQL.g:334:2: ( 'Tweet' )
                    {
                    // InternalGraphQL.g:334:2: ( 'Tweet' )
                    // InternalGraphQL.g:335:3: 'Tweet'
                    {
                     before(grammarAccess.getTypeIDAccess().getTweetKeyword_5()); 
                    match(input,23,FOLLOW_2); 
                     after(grammarAccess.getTypeIDAccess().getTweetKeyword_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeID__Alternatives"


    // $ANTLR start "rule__DataType__Group__0"
    // InternalGraphQL.g:344:1: rule__DataType__Group__0 : rule__DataType__Group__0__Impl rule__DataType__Group__1 ;
    public final void rule__DataType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:348:1: ( rule__DataType__Group__0__Impl rule__DataType__Group__1 )
            // InternalGraphQL.g:349:2: rule__DataType__Group__0__Impl rule__DataType__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__DataType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__0"


    // $ANTLR start "rule__DataType__Group__0__Impl"
    // InternalGraphQL.g:356:1: rule__DataType__Group__0__Impl : ( 'scalar' ) ;
    public final void rule__DataType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:360:1: ( ( 'scalar' ) )
            // InternalGraphQL.g:361:1: ( 'scalar' )
            {
            // InternalGraphQL.g:361:1: ( 'scalar' )
            // InternalGraphQL.g:362:2: 'scalar'
            {
             before(grammarAccess.getDataTypeAccess().getScalarKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getDataTypeAccess().getScalarKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__0__Impl"


    // $ANTLR start "rule__DataType__Group__1"
    // InternalGraphQL.g:371:1: rule__DataType__Group__1 : rule__DataType__Group__1__Impl ;
    public final void rule__DataType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:375:1: ( rule__DataType__Group__1__Impl )
            // InternalGraphQL.g:376:2: rule__DataType__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__1"


    // $ANTLR start "rule__DataType__Group__1__Impl"
    // InternalGraphQL.g:382:1: rule__DataType__Group__1__Impl : ( ( rule__DataType__NameAssignment_1 ) ) ;
    public final void rule__DataType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:386:1: ( ( ( rule__DataType__NameAssignment_1 ) ) )
            // InternalGraphQL.g:387:1: ( ( rule__DataType__NameAssignment_1 ) )
            {
            // InternalGraphQL.g:387:1: ( ( rule__DataType__NameAssignment_1 ) )
            // InternalGraphQL.g:388:2: ( rule__DataType__NameAssignment_1 )
            {
             before(grammarAccess.getDataTypeAccess().getNameAssignment_1()); 
            // InternalGraphQL.g:389:2: ( rule__DataType__NameAssignment_1 )
            // InternalGraphQL.g:389:3: rule__DataType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DataType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDataTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__1__Impl"


    // $ANTLR start "rule__Type__Group__0"
    // InternalGraphQL.g:398:1: rule__Type__Group__0 : rule__Type__Group__0__Impl rule__Type__Group__1 ;
    public final void rule__Type__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:402:1: ( rule__Type__Group__0__Impl rule__Type__Group__1 )
            // InternalGraphQL.g:403:2: rule__Type__Group__0__Impl rule__Type__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Type__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__0"


    // $ANTLR start "rule__Type__Group__0__Impl"
    // InternalGraphQL.g:410:1: rule__Type__Group__0__Impl : ( ( ( 'type' ) ) ( ( 'type' )* ) ) ;
    public final void rule__Type__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:414:1: ( ( ( ( 'type' ) ) ( ( 'type' )* ) ) )
            // InternalGraphQL.g:415:1: ( ( ( 'type' ) ) ( ( 'type' )* ) )
            {
            // InternalGraphQL.g:415:1: ( ( ( 'type' ) ) ( ( 'type' )* ) )
            // InternalGraphQL.g:416:2: ( ( 'type' ) ) ( ( 'type' )* )
            {
            // InternalGraphQL.g:416:2: ( ( 'type' ) )
            // InternalGraphQL.g:417:3: ( 'type' )
            {
             before(grammarAccess.getTypeAccess().getTypeKeyword_0()); 
            // InternalGraphQL.g:418:3: ( 'type' )
            // InternalGraphQL.g:418:4: 'type'
            {
            match(input,25,FOLLOW_6); 

            }

             after(grammarAccess.getTypeAccess().getTypeKeyword_0()); 

            }

            // InternalGraphQL.g:421:2: ( ( 'type' )* )
            // InternalGraphQL.g:422:3: ( 'type' )*
            {
             before(grammarAccess.getTypeAccess().getTypeKeyword_0()); 
            // InternalGraphQL.g:423:3: ( 'type' )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==25) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalGraphQL.g:423:4: 'type'
            	    {
            	    match(input,25,FOLLOW_6); 

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getTypeAccess().getTypeKeyword_0()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__0__Impl"


    // $ANTLR start "rule__Type__Group__1"
    // InternalGraphQL.g:432:1: rule__Type__Group__1 : rule__Type__Group__1__Impl rule__Type__Group__2 ;
    public final void rule__Type__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:436:1: ( rule__Type__Group__1__Impl rule__Type__Group__2 )
            // InternalGraphQL.g:437:2: rule__Type__Group__1__Impl rule__Type__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Type__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__1"


    // $ANTLR start "rule__Type__Group__1__Impl"
    // InternalGraphQL.g:444:1: rule__Type__Group__1__Impl : ( ( rule__Type__NameAssignment_1 ) ) ;
    public final void rule__Type__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:448:1: ( ( ( rule__Type__NameAssignment_1 ) ) )
            // InternalGraphQL.g:449:1: ( ( rule__Type__NameAssignment_1 ) )
            {
            // InternalGraphQL.g:449:1: ( ( rule__Type__NameAssignment_1 ) )
            // InternalGraphQL.g:450:2: ( rule__Type__NameAssignment_1 )
            {
             before(grammarAccess.getTypeAccess().getNameAssignment_1()); 
            // InternalGraphQL.g:451:2: ( rule__Type__NameAssignment_1 )
            // InternalGraphQL.g:451:3: rule__Type__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Type__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__1__Impl"


    // $ANTLR start "rule__Type__Group__2"
    // InternalGraphQL.g:459:1: rule__Type__Group__2 : rule__Type__Group__2__Impl rule__Type__Group__3 ;
    public final void rule__Type__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:463:1: ( rule__Type__Group__2__Impl rule__Type__Group__3 )
            // InternalGraphQL.g:464:2: rule__Type__Group__2__Impl rule__Type__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Type__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__2"


    // $ANTLR start "rule__Type__Group__2__Impl"
    // InternalGraphQL.g:471:1: rule__Type__Group__2__Impl : ( '(' ) ;
    public final void rule__Type__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:475:1: ( ( '(' ) )
            // InternalGraphQL.g:476:1: ( '(' )
            {
            // InternalGraphQL.g:476:1: ( '(' )
            // InternalGraphQL.g:477:2: '('
            {
             before(grammarAccess.getTypeAccess().getLeftParenthesisKeyword_2()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getTypeAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__2__Impl"


    // $ANTLR start "rule__Type__Group__3"
    // InternalGraphQL.g:486:1: rule__Type__Group__3 : rule__Type__Group__3__Impl rule__Type__Group__4 ;
    public final void rule__Type__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:490:1: ( rule__Type__Group__3__Impl rule__Type__Group__4 )
            // InternalGraphQL.g:491:2: rule__Type__Group__3__Impl rule__Type__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__Type__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__3"


    // $ANTLR start "rule__Type__Group__3__Impl"
    // InternalGraphQL.g:498:1: rule__Type__Group__3__Impl : ( ')' ) ;
    public final void rule__Type__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:502:1: ( ( ')' ) )
            // InternalGraphQL.g:503:1: ( ')' )
            {
            // InternalGraphQL.g:503:1: ( ')' )
            // InternalGraphQL.g:504:2: ')'
            {
             before(grammarAccess.getTypeAccess().getRightParenthesisKeyword_3()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getTypeAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__3__Impl"


    // $ANTLR start "rule__Type__Group__4"
    // InternalGraphQL.g:513:1: rule__Type__Group__4 : rule__Type__Group__4__Impl rule__Type__Group__5 ;
    public final void rule__Type__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:517:1: ( rule__Type__Group__4__Impl rule__Type__Group__5 )
            // InternalGraphQL.g:518:2: rule__Type__Group__4__Impl rule__Type__Group__5
            {
            pushFollow(FOLLOW_10);
            rule__Type__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__4"


    // $ANTLR start "rule__Type__Group__4__Impl"
    // InternalGraphQL.g:525:1: rule__Type__Group__4__Impl : ( '{' ) ;
    public final void rule__Type__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:529:1: ( ( '{' ) )
            // InternalGraphQL.g:530:1: ( '{' )
            {
            // InternalGraphQL.g:530:1: ( '{' )
            // InternalGraphQL.g:531:2: '{'
            {
             before(grammarAccess.getTypeAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getTypeAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__4__Impl"


    // $ANTLR start "rule__Type__Group__5"
    // InternalGraphQL.g:540:1: rule__Type__Group__5 : rule__Type__Group__5__Impl rule__Type__Group__6 ;
    public final void rule__Type__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:544:1: ( rule__Type__Group__5__Impl rule__Type__Group__6 )
            // InternalGraphQL.g:545:2: rule__Type__Group__5__Impl rule__Type__Group__6
            {
            pushFollow(FOLLOW_11);
            rule__Type__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__5"


    // $ANTLR start "rule__Type__Group__5__Impl"
    // InternalGraphQL.g:552:1: rule__Type__Group__5__Impl : ( ( ( rule__Type__QueryAssignment_5 ) ) ( ( rule__Type__QueryAssignment_5 )* ) ) ;
    public final void rule__Type__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:556:1: ( ( ( ( rule__Type__QueryAssignment_5 ) ) ( ( rule__Type__QueryAssignment_5 )* ) ) )
            // InternalGraphQL.g:557:1: ( ( ( rule__Type__QueryAssignment_5 ) ) ( ( rule__Type__QueryAssignment_5 )* ) )
            {
            // InternalGraphQL.g:557:1: ( ( ( rule__Type__QueryAssignment_5 ) ) ( ( rule__Type__QueryAssignment_5 )* ) )
            // InternalGraphQL.g:558:2: ( ( rule__Type__QueryAssignment_5 ) ) ( ( rule__Type__QueryAssignment_5 )* )
            {
            // InternalGraphQL.g:558:2: ( ( rule__Type__QueryAssignment_5 ) )
            // InternalGraphQL.g:559:3: ( rule__Type__QueryAssignment_5 )
            {
             before(grammarAccess.getTypeAccess().getQueryAssignment_5()); 
            // InternalGraphQL.g:560:3: ( rule__Type__QueryAssignment_5 )
            // InternalGraphQL.g:560:4: rule__Type__QueryAssignment_5
            {
            pushFollow(FOLLOW_12);
            rule__Type__QueryAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getQueryAssignment_5()); 

            }

            // InternalGraphQL.g:563:2: ( ( rule__Type__QueryAssignment_5 )* )
            // InternalGraphQL.g:564:3: ( rule__Type__QueryAssignment_5 )*
            {
             before(grammarAccess.getTypeAccess().getQueryAssignment_5()); 
            // InternalGraphQL.g:565:3: ( rule__Type__QueryAssignment_5 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_ID) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalGraphQL.g:565:4: rule__Type__QueryAssignment_5
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__Type__QueryAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getTypeAccess().getQueryAssignment_5()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__5__Impl"


    // $ANTLR start "rule__Type__Group__6"
    // InternalGraphQL.g:574:1: rule__Type__Group__6 : rule__Type__Group__6__Impl ;
    public final void rule__Type__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:578:1: ( rule__Type__Group__6__Impl )
            // InternalGraphQL.g:579:2: rule__Type__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Type__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__6"


    // $ANTLR start "rule__Type__Group__6__Impl"
    // InternalGraphQL.g:585:1: rule__Type__Group__6__Impl : ( '}' ) ;
    public final void rule__Type__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:589:1: ( ( '}' ) )
            // InternalGraphQL.g:590:1: ( '}' )
            {
            // InternalGraphQL.g:590:1: ( '}' )
            // InternalGraphQL.g:591:2: '}'
            {
             before(grammarAccess.getTypeAccess().getRightCurlyBracketKeyword_6()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getTypeAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__6__Impl"


    // $ANTLR start "rule__QueryNode__Group__0"
    // InternalGraphQL.g:601:1: rule__QueryNode__Group__0 : rule__QueryNode__Group__0__Impl rule__QueryNode__Group__1 ;
    public final void rule__QueryNode__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:605:1: ( rule__QueryNode__Group__0__Impl rule__QueryNode__Group__1 )
            // InternalGraphQL.g:606:2: rule__QueryNode__Group__0__Impl rule__QueryNode__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__QueryNode__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__0"


    // $ANTLR start "rule__QueryNode__Group__0__Impl"
    // InternalGraphQL.g:613:1: rule__QueryNode__Group__0__Impl : ( ( rule__QueryNode__NameAssignment_0 ) ) ;
    public final void rule__QueryNode__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:617:1: ( ( ( rule__QueryNode__NameAssignment_0 ) ) )
            // InternalGraphQL.g:618:1: ( ( rule__QueryNode__NameAssignment_0 ) )
            {
            // InternalGraphQL.g:618:1: ( ( rule__QueryNode__NameAssignment_0 ) )
            // InternalGraphQL.g:619:2: ( rule__QueryNode__NameAssignment_0 )
            {
             before(grammarAccess.getQueryNodeAccess().getNameAssignment_0()); 
            // InternalGraphQL.g:620:2: ( rule__QueryNode__NameAssignment_0 )
            // InternalGraphQL.g:620:3: rule__QueryNode__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getQueryNodeAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__0__Impl"


    // $ANTLR start "rule__QueryNode__Group__1"
    // InternalGraphQL.g:628:1: rule__QueryNode__Group__1 : rule__QueryNode__Group__1__Impl rule__QueryNode__Group__2 ;
    public final void rule__QueryNode__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:632:1: ( rule__QueryNode__Group__1__Impl rule__QueryNode__Group__2 )
            // InternalGraphQL.g:633:2: rule__QueryNode__Group__1__Impl rule__QueryNode__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__QueryNode__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__1"


    // $ANTLR start "rule__QueryNode__Group__1__Impl"
    // InternalGraphQL.g:640:1: rule__QueryNode__Group__1__Impl : ( ':' ) ;
    public final void rule__QueryNode__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:644:1: ( ( ':' ) )
            // InternalGraphQL.g:645:1: ( ':' )
            {
            // InternalGraphQL.g:645:1: ( ':' )
            // InternalGraphQL.g:646:2: ':'
            {
             before(grammarAccess.getQueryNodeAccess().getColonKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getQueryNodeAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__1__Impl"


    // $ANTLR start "rule__QueryNode__Group__2"
    // InternalGraphQL.g:655:1: rule__QueryNode__Group__2 : rule__QueryNode__Group__2__Impl ;
    public final void rule__QueryNode__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:659:1: ( rule__QueryNode__Group__2__Impl )
            // InternalGraphQL.g:660:2: rule__QueryNode__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__2"


    // $ANTLR start "rule__QueryNode__Group__2__Impl"
    // InternalGraphQL.g:666:1: rule__QueryNode__Group__2__Impl : ( ( rule__QueryNode__Group_2__0 ) ) ;
    public final void rule__QueryNode__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:670:1: ( ( ( rule__QueryNode__Group_2__0 ) ) )
            // InternalGraphQL.g:671:1: ( ( rule__QueryNode__Group_2__0 ) )
            {
            // InternalGraphQL.g:671:1: ( ( rule__QueryNode__Group_2__0 ) )
            // InternalGraphQL.g:672:2: ( rule__QueryNode__Group_2__0 )
            {
             before(grammarAccess.getQueryNodeAccess().getGroup_2()); 
            // InternalGraphQL.g:673:2: ( rule__QueryNode__Group_2__0 )
            // InternalGraphQL.g:673:3: rule__QueryNode__Group_2__0
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryNodeAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group__2__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__0"
    // InternalGraphQL.g:682:1: rule__QueryNode__Group_2__0 : rule__QueryNode__Group_2__0__Impl rule__QueryNode__Group_2__1 ;
    public final void rule__QueryNode__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:686:1: ( rule__QueryNode__Group_2__0__Impl rule__QueryNode__Group_2__1 )
            // InternalGraphQL.g:687:2: rule__QueryNode__Group_2__0__Impl rule__QueryNode__Group_2__1
            {
            pushFollow(FOLLOW_14);
            rule__QueryNode__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__0"


    // $ANTLR start "rule__QueryNode__Group_2__0__Impl"
    // InternalGraphQL.g:694:1: rule__QueryNode__Group_2__0__Impl : ( ( '[' )? ) ;
    public final void rule__QueryNode__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:698:1: ( ( ( '[' )? ) )
            // InternalGraphQL.g:699:1: ( ( '[' )? )
            {
            // InternalGraphQL.g:699:1: ( ( '[' )? )
            // InternalGraphQL.g:700:2: ( '[' )?
            {
             before(grammarAccess.getQueryNodeAccess().getLeftSquareBracketKeyword_2_0()); 
            // InternalGraphQL.g:701:2: ( '[' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==31) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalGraphQL.g:701:3: '['
                    {
                    match(input,31,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getLeftSquareBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__0__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__1"
    // InternalGraphQL.g:709:1: rule__QueryNode__Group_2__1 : rule__QueryNode__Group_2__1__Impl rule__QueryNode__Group_2__2 ;
    public final void rule__QueryNode__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:713:1: ( rule__QueryNode__Group_2__1__Impl rule__QueryNode__Group_2__2 )
            // InternalGraphQL.g:714:2: rule__QueryNode__Group_2__1__Impl rule__QueryNode__Group_2__2
            {
            pushFollow(FOLLOW_15);
            rule__QueryNode__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__1"


    // $ANTLR start "rule__QueryNode__Group_2__1__Impl"
    // InternalGraphQL.g:721:1: rule__QueryNode__Group_2__1__Impl : ( ( rule__QueryNode__Group_2_1__0 ) ) ;
    public final void rule__QueryNode__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:725:1: ( ( ( rule__QueryNode__Group_2_1__0 ) ) )
            // InternalGraphQL.g:726:1: ( ( rule__QueryNode__Group_2_1__0 ) )
            {
            // InternalGraphQL.g:726:1: ( ( rule__QueryNode__Group_2_1__0 ) )
            // InternalGraphQL.g:727:2: ( rule__QueryNode__Group_2_1__0 )
            {
             before(grammarAccess.getQueryNodeAccess().getGroup_2_1()); 
            // InternalGraphQL.g:728:2: ( rule__QueryNode__Group_2_1__0 )
            // InternalGraphQL.g:728:3: rule__QueryNode__Group_2_1__0
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_1__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryNodeAccess().getGroup_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__1__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__2"
    // InternalGraphQL.g:736:1: rule__QueryNode__Group_2__2 : rule__QueryNode__Group_2__2__Impl rule__QueryNode__Group_2__3 ;
    public final void rule__QueryNode__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:740:1: ( rule__QueryNode__Group_2__2__Impl rule__QueryNode__Group_2__3 )
            // InternalGraphQL.g:741:2: rule__QueryNode__Group_2__2__Impl rule__QueryNode__Group_2__3
            {
            pushFollow(FOLLOW_15);
            rule__QueryNode__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__2"


    // $ANTLR start "rule__QueryNode__Group_2__2__Impl"
    // InternalGraphQL.g:748:1: rule__QueryNode__Group_2__2__Impl : ( ( '!' )? ) ;
    public final void rule__QueryNode__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:752:1: ( ( ( '!' )? ) )
            // InternalGraphQL.g:753:1: ( ( '!' )? )
            {
            // InternalGraphQL.g:753:1: ( ( '!' )? )
            // InternalGraphQL.g:754:2: ( '!' )?
            {
             before(grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_2()); 
            // InternalGraphQL.g:755:2: ( '!' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==32) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalGraphQL.g:755:3: '!'
                    {
                    match(input,32,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__2__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__3"
    // InternalGraphQL.g:763:1: rule__QueryNode__Group_2__3 : rule__QueryNode__Group_2__3__Impl rule__QueryNode__Group_2__4 ;
    public final void rule__QueryNode__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:767:1: ( rule__QueryNode__Group_2__3__Impl rule__QueryNode__Group_2__4 )
            // InternalGraphQL.g:768:2: rule__QueryNode__Group_2__3__Impl rule__QueryNode__Group_2__4
            {
            pushFollow(FOLLOW_15);
            rule__QueryNode__Group_2__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__3"


    // $ANTLR start "rule__QueryNode__Group_2__3__Impl"
    // InternalGraphQL.g:775:1: rule__QueryNode__Group_2__3__Impl : ( ( ']' )? ) ;
    public final void rule__QueryNode__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:779:1: ( ( ( ']' )? ) )
            // InternalGraphQL.g:780:1: ( ( ']' )? )
            {
            // InternalGraphQL.g:780:1: ( ( ']' )? )
            // InternalGraphQL.g:781:2: ( ']' )?
            {
             before(grammarAccess.getQueryNodeAccess().getRightSquareBracketKeyword_2_3()); 
            // InternalGraphQL.g:782:2: ( ']' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==33) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalGraphQL.g:782:3: ']'
                    {
                    match(input,33,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getRightSquareBracketKeyword_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__3__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__4"
    // InternalGraphQL.g:790:1: rule__QueryNode__Group_2__4 : rule__QueryNode__Group_2__4__Impl rule__QueryNode__Group_2__5 ;
    public final void rule__QueryNode__Group_2__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:794:1: ( rule__QueryNode__Group_2__4__Impl rule__QueryNode__Group_2__5 )
            // InternalGraphQL.g:795:2: rule__QueryNode__Group_2__4__Impl rule__QueryNode__Group_2__5
            {
            pushFollow(FOLLOW_15);
            rule__QueryNode__Group_2__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__4"


    // $ANTLR start "rule__QueryNode__Group_2__4__Impl"
    // InternalGraphQL.g:802:1: rule__QueryNode__Group_2__4__Impl : ( ( '!' )? ) ;
    public final void rule__QueryNode__Group_2__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:806:1: ( ( ( '!' )? ) )
            // InternalGraphQL.g:807:1: ( ( '!' )? )
            {
            // InternalGraphQL.g:807:1: ( ( '!' )? )
            // InternalGraphQL.g:808:2: ( '!' )?
            {
             before(grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_4()); 
            // InternalGraphQL.g:809:2: ( '!' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==32) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalGraphQL.g:809:3: '!'
                    {
                    match(input,32,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getExclamationMarkKeyword_2_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__4__Impl"


    // $ANTLR start "rule__QueryNode__Group_2__5"
    // InternalGraphQL.g:817:1: rule__QueryNode__Group_2__5 : rule__QueryNode__Group_2__5__Impl ;
    public final void rule__QueryNode__Group_2__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:821:1: ( rule__QueryNode__Group_2__5__Impl )
            // InternalGraphQL.g:822:2: rule__QueryNode__Group_2__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__5"


    // $ANTLR start "rule__QueryNode__Group_2__5__Impl"
    // InternalGraphQL.g:828:1: rule__QueryNode__Group_2__5__Impl : ( ( rule__QueryNode__Group_2_5__0 )? ) ;
    public final void rule__QueryNode__Group_2__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:832:1: ( ( ( rule__QueryNode__Group_2_5__0 )? ) )
            // InternalGraphQL.g:833:1: ( ( rule__QueryNode__Group_2_5__0 )? )
            {
            // InternalGraphQL.g:833:1: ( ( rule__QueryNode__Group_2_5__0 )? )
            // InternalGraphQL.g:834:2: ( rule__QueryNode__Group_2_5__0 )?
            {
             before(grammarAccess.getQueryNodeAccess().getGroup_2_5()); 
            // InternalGraphQL.g:835:2: ( rule__QueryNode__Group_2_5__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==28) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalGraphQL.g:835:3: rule__QueryNode__Group_2_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__QueryNode__Group_2_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getGroup_2_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2__5__Impl"


    // $ANTLR start "rule__QueryNode__Group_2_1__0"
    // InternalGraphQL.g:844:1: rule__QueryNode__Group_2_1__0 : rule__QueryNode__Group_2_1__0__Impl rule__QueryNode__Group_2_1__1 ;
    public final void rule__QueryNode__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:848:1: ( rule__QueryNode__Group_2_1__0__Impl rule__QueryNode__Group_2_1__1 )
            // InternalGraphQL.g:849:2: rule__QueryNode__Group_2_1__0__Impl rule__QueryNode__Group_2_1__1
            {
            pushFollow(FOLLOW_14);
            rule__QueryNode__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_1__0"


    // $ANTLR start "rule__QueryNode__Group_2_1__0__Impl"
    // InternalGraphQL.g:856:1: rule__QueryNode__Group_2_1__0__Impl : ( ( rule__QueryNode__QueryNodeAssignment_2_1_0 )? ) ;
    public final void rule__QueryNode__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:860:1: ( ( ( rule__QueryNode__QueryNodeAssignment_2_1_0 )? ) )
            // InternalGraphQL.g:861:1: ( ( rule__QueryNode__QueryNodeAssignment_2_1_0 )? )
            {
            // InternalGraphQL.g:861:1: ( ( rule__QueryNode__QueryNodeAssignment_2_1_0 )? )
            // InternalGraphQL.g:862:2: ( rule__QueryNode__QueryNodeAssignment_2_1_0 )?
            {
             before(grammarAccess.getQueryNodeAccess().getQueryNodeAssignment_2_1_0()); 
            // InternalGraphQL.g:863:2: ( rule__QueryNode__QueryNodeAssignment_2_1_0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=11 && LA12_0<=17)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalGraphQL.g:863:3: rule__QueryNode__QueryNodeAssignment_2_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__QueryNode__QueryNodeAssignment_2_1_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getQueryNodeAssignment_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_1__0__Impl"


    // $ANTLR start "rule__QueryNode__Group_2_1__1"
    // InternalGraphQL.g:871:1: rule__QueryNode__Group_2_1__1 : rule__QueryNode__Group_2_1__1__Impl ;
    public final void rule__QueryNode__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:875:1: ( rule__QueryNode__Group_2_1__1__Impl )
            // InternalGraphQL.g:876:2: rule__QueryNode__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_1__1"


    // $ANTLR start "rule__QueryNode__Group_2_1__1__Impl"
    // InternalGraphQL.g:882:1: rule__QueryNode__Group_2_1__1__Impl : ( ( rule__QueryNode__QueryNodeAssignment_2_1_1 )? ) ;
    public final void rule__QueryNode__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:886:1: ( ( ( rule__QueryNode__QueryNodeAssignment_2_1_1 )? ) )
            // InternalGraphQL.g:887:1: ( ( rule__QueryNode__QueryNodeAssignment_2_1_1 )? )
            {
            // InternalGraphQL.g:887:1: ( ( rule__QueryNode__QueryNodeAssignment_2_1_1 )? )
            // InternalGraphQL.g:888:2: ( rule__QueryNode__QueryNodeAssignment_2_1_1 )?
            {
             before(grammarAccess.getQueryNodeAccess().getQueryNodeAssignment_2_1_1()); 
            // InternalGraphQL.g:889:2: ( rule__QueryNode__QueryNodeAssignment_2_1_1 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( ((LA13_0>=18 && LA13_0<=23)) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalGraphQL.g:889:3: rule__QueryNode__QueryNodeAssignment_2_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__QueryNode__QueryNodeAssignment_2_1_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQueryNodeAccess().getQueryNodeAssignment_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_1__1__Impl"


    // $ANTLR start "rule__QueryNode__Group_2_5__0"
    // InternalGraphQL.g:898:1: rule__QueryNode__Group_2_5__0 : rule__QueryNode__Group_2_5__0__Impl rule__QueryNode__Group_2_5__1 ;
    public final void rule__QueryNode__Group_2_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:902:1: ( rule__QueryNode__Group_2_5__0__Impl rule__QueryNode__Group_2_5__1 )
            // InternalGraphQL.g:903:2: rule__QueryNode__Group_2_5__0__Impl rule__QueryNode__Group_2_5__1
            {
            pushFollow(FOLLOW_10);
            rule__QueryNode__Group_2_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__0"


    // $ANTLR start "rule__QueryNode__Group_2_5__0__Impl"
    // InternalGraphQL.g:910:1: rule__QueryNode__Group_2_5__0__Impl : ( '{' ) ;
    public final void rule__QueryNode__Group_2_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:914:1: ( ( '{' ) )
            // InternalGraphQL.g:915:1: ( '{' )
            {
            // InternalGraphQL.g:915:1: ( '{' )
            // InternalGraphQL.g:916:2: '{'
            {
             before(grammarAccess.getQueryNodeAccess().getLeftCurlyBracketKeyword_2_5_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getQueryNodeAccess().getLeftCurlyBracketKeyword_2_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__0__Impl"


    // $ANTLR start "rule__QueryNode__Group_2_5__1"
    // InternalGraphQL.g:925:1: rule__QueryNode__Group_2_5__1 : rule__QueryNode__Group_2_5__1__Impl rule__QueryNode__Group_2_5__2 ;
    public final void rule__QueryNode__Group_2_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:929:1: ( rule__QueryNode__Group_2_5__1__Impl rule__QueryNode__Group_2_5__2 )
            // InternalGraphQL.g:930:2: rule__QueryNode__Group_2_5__1__Impl rule__QueryNode__Group_2_5__2
            {
            pushFollow(FOLLOW_11);
            rule__QueryNode__Group_2_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__1"


    // $ANTLR start "rule__QueryNode__Group_2_5__1__Impl"
    // InternalGraphQL.g:937:1: rule__QueryNode__Group_2_5__1__Impl : ( ( rule__QueryNode__NextNodeAssignment_2_5_1 ) ) ;
    public final void rule__QueryNode__Group_2_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:941:1: ( ( ( rule__QueryNode__NextNodeAssignment_2_5_1 ) ) )
            // InternalGraphQL.g:942:1: ( ( rule__QueryNode__NextNodeAssignment_2_5_1 ) )
            {
            // InternalGraphQL.g:942:1: ( ( rule__QueryNode__NextNodeAssignment_2_5_1 ) )
            // InternalGraphQL.g:943:2: ( rule__QueryNode__NextNodeAssignment_2_5_1 )
            {
             before(grammarAccess.getQueryNodeAccess().getNextNodeAssignment_2_5_1()); 
            // InternalGraphQL.g:944:2: ( rule__QueryNode__NextNodeAssignment_2_5_1 )
            // InternalGraphQL.g:944:3: rule__QueryNode__NextNodeAssignment_2_5_1
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__NextNodeAssignment_2_5_1();

            state._fsp--;


            }

             after(grammarAccess.getQueryNodeAccess().getNextNodeAssignment_2_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__1__Impl"


    // $ANTLR start "rule__QueryNode__Group_2_5__2"
    // InternalGraphQL.g:952:1: rule__QueryNode__Group_2_5__2 : rule__QueryNode__Group_2_5__2__Impl ;
    public final void rule__QueryNode__Group_2_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:956:1: ( rule__QueryNode__Group_2_5__2__Impl )
            // InternalGraphQL.g:957:2: rule__QueryNode__Group_2_5__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QueryNode__Group_2_5__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__2"


    // $ANTLR start "rule__QueryNode__Group_2_5__2__Impl"
    // InternalGraphQL.g:963:1: rule__QueryNode__Group_2_5__2__Impl : ( '}' ) ;
    public final void rule__QueryNode__Group_2_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:967:1: ( ( '}' ) )
            // InternalGraphQL.g:968:1: ( '}' )
            {
            // InternalGraphQL.g:968:1: ( '}' )
            // InternalGraphQL.g:969:2: '}'
            {
             before(grammarAccess.getQueryNodeAccess().getRightCurlyBracketKeyword_2_5_2()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getQueryNodeAccess().getRightCurlyBracketKeyword_2_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__Group_2_5__2__Impl"


    // $ANTLR start "rule__GraphQL__QueriesAssignment"
    // InternalGraphQL.g:979:1: rule__GraphQL__QueriesAssignment : ( ruleQuery ) ;
    public final void rule__GraphQL__QueriesAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:983:1: ( ( ruleQuery ) )
            // InternalGraphQL.g:984:2: ( ruleQuery )
            {
            // InternalGraphQL.g:984:2: ( ruleQuery )
            // InternalGraphQL.g:985:3: ruleQuery
            {
             before(grammarAccess.getGraphQLAccess().getQueriesQueryParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleQuery();

            state._fsp--;

             after(grammarAccess.getGraphQLAccess().getQueriesQueryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GraphQL__QueriesAssignment"


    // $ANTLR start "rule__DataType__NameAssignment_1"
    // InternalGraphQL.g:994:1: rule__DataType__NameAssignment_1 : ( ruleDataID ) ;
    public final void rule__DataType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:998:1: ( ( ruleDataID ) )
            // InternalGraphQL.g:999:2: ( ruleDataID )
            {
            // InternalGraphQL.g:999:2: ( ruleDataID )
            // InternalGraphQL.g:1000:3: ruleDataID
            {
             before(grammarAccess.getDataTypeAccess().getNameDataIDParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDataID();

            state._fsp--;

             after(grammarAccess.getDataTypeAccess().getNameDataIDParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__NameAssignment_1"


    // $ANTLR start "rule__Type__NameAssignment_1"
    // InternalGraphQL.g:1009:1: rule__Type__NameAssignment_1 : ( ruleTypeID ) ;
    public final void rule__Type__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:1013:1: ( ( ruleTypeID ) )
            // InternalGraphQL.g:1014:2: ( ruleTypeID )
            {
            // InternalGraphQL.g:1014:2: ( ruleTypeID )
            // InternalGraphQL.g:1015:3: ruleTypeID
            {
             before(grammarAccess.getTypeAccess().getNameTypeIDParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeID();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getNameTypeIDParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__NameAssignment_1"


    // $ANTLR start "rule__Type__QueryAssignment_5"
    // InternalGraphQL.g:1024:1: rule__Type__QueryAssignment_5 : ( ruleQueryNode ) ;
    public final void rule__Type__QueryAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:1028:1: ( ( ruleQueryNode ) )
            // InternalGraphQL.g:1029:2: ( ruleQueryNode )
            {
            // InternalGraphQL.g:1029:2: ( ruleQueryNode )
            // InternalGraphQL.g:1030:3: ruleQueryNode
            {
             before(grammarAccess.getTypeAccess().getQueryQueryNodeParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleQueryNode();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getQueryQueryNodeParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__QueryAssignment_5"


    // $ANTLR start "rule__QueryNode__NameAssignment_0"
    // InternalGraphQL.g:1039:1: rule__QueryNode__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__QueryNode__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:1043:1: ( ( RULE_ID ) )
            // InternalGraphQL.g:1044:2: ( RULE_ID )
            {
            // InternalGraphQL.g:1044:2: ( RULE_ID )
            // InternalGraphQL.g:1045:3: RULE_ID
            {
             before(grammarAccess.getQueryNodeAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQueryNodeAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__NameAssignment_0"


    // $ANTLR start "rule__QueryNode__QueryNodeAssignment_2_1_0"
    // InternalGraphQL.g:1054:1: rule__QueryNode__QueryNodeAssignment_2_1_0 : ( ruleDataID ) ;
    public final void rule__QueryNode__QueryNodeAssignment_2_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:1058:1: ( ( ruleDataID ) )
            // InternalGraphQL.g:1059:2: ( ruleDataID )
            {
            // InternalGraphQL.g:1059:2: ( ruleDataID )
            // InternalGraphQL.g:1060:3: ruleDataID
            {
             before(grammarAccess.getQueryNodeAccess().getQueryNodeDataIDParserRuleCall_2_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDataID();

            state._fsp--;

             after(grammarAccess.getQueryNodeAccess().getQueryNodeDataIDParserRuleCall_2_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__QueryNodeAssignment_2_1_0"


    // $ANTLR start "rule__QueryNode__QueryNodeAssignment_2_1_1"
    // InternalGraphQL.g:1069:1: rule__QueryNode__QueryNodeAssignment_2_1_1 : ( ruleTypeID ) ;
    public final void rule__QueryNode__QueryNodeAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:1073:1: ( ( ruleTypeID ) )
            // InternalGraphQL.g:1074:2: ( ruleTypeID )
            {
            // InternalGraphQL.g:1074:2: ( ruleTypeID )
            // InternalGraphQL.g:1075:3: ruleTypeID
            {
             before(grammarAccess.getQueryNodeAccess().getQueryNodeTypeIDParserRuleCall_2_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeID();

            state._fsp--;

             after(grammarAccess.getQueryNodeAccess().getQueryNodeTypeIDParserRuleCall_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__QueryNodeAssignment_2_1_1"


    // $ANTLR start "rule__QueryNode__NextNodeAssignment_2_5_1"
    // InternalGraphQL.g:1084:1: rule__QueryNode__NextNodeAssignment_2_5_1 : ( ruleQueryNode ) ;
    public final void rule__QueryNode__NextNodeAssignment_2_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGraphQL.g:1088:1: ( ( ruleQueryNode ) )
            // InternalGraphQL.g:1089:2: ( ruleQueryNode )
            {
            // InternalGraphQL.g:1089:2: ( ruleQueryNode )
            // InternalGraphQL.g:1090:3: ruleQueryNode
            {
             before(grammarAccess.getQueryNodeAccess().getNextNodeQueryNodeParserRuleCall_2_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQueryNode();

            state._fsp--;

             after(grammarAccess.getQueryNodeAccess().getNextNodeQueryNodeParserRuleCall_2_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryNode__NextNodeAssignment_2_5_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000003000002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x000000000003F800L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000FC0000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000080FFF800L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000310000000L});

}